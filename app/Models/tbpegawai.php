<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tbpegawai extends Model
{
    //
    protected $connection   = "db2019";
    protected $table        = "tbpegawai";
    protected $guarded      = [];
    public $timestamps      = false;

    public function __construct(){
        parent::__construct();
        $this->connection = "db".session("th_ajaran");
    }

}
