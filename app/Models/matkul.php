<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class matkul extends Model
{
    //
    protected $connection   = "db2019";
    protected $table        = "tbmatkul";
    protected $guarded      = [];

    public function __construct(){
        parent::__construct();
        $this->connection = "db".session("th_ajaran");
    }
}
