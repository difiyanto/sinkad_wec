<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class matkulojt extends Model
{
    //
    protected $connection   = "db2019";
    protected $table        = "o_matkulojt";
    protected $guarded      = [];
    public $timestamps      = false;

    public function __construct(){
        parent::__construct();
        $this->connection = "db".session("th_ajaran");
    }

    public function getmatkul(){
        return $this->belongsTo("App\\Models\\matkul", "kd_matkul", "kd_matkul");
    }
}
