<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class pengguna extends Authenticatable
{
    //
    protected $connection   = "db2019";
    protected $table        = "tbpengguna";
    protected $fillable      = ["username", "password", "nama_pengguna", "otoritas"];

    public function __construct(){
        parent::__construct();
        $this->connection = "db".session("th_ajaran");
    }
}
