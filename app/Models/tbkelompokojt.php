<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tbkelompokojt extends Model
{
    //
    protected $connection   = "db2019";
    protected $table        = "tbkelompokojt";
    protected $fillable      = ["kode_kelompok","kode_perusahaan", "NIP", "bulan", "no_prop1", "kode_perusahaan1", "tgl1" , "cekstatus", "sts1"];
    public $timestamps       = false;

    public function __construct(){
        parent::__construct();
        $this->connection = "db".session("th_ajaran");
    }

    public function getdu(){
        return $this->hasMany("App\\Models\\du", "KDOJT","kode_kelompok");
    }

    public function getpembimbing1(){
        return $this->belongsTo("App\\Models\\tbpegawai", "NIP", "NIP");
    }

    public function getperusahaan(){
        return $this->belongsTo("App\\Models\\perusahaan", "kode_perusahaan", "kode_perusahaan");
    }
}
