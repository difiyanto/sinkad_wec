<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mhsdaft extends Model
{
    //
    protected $connection   = "db2019";
    protected $table        = "mhsdaft";
    protected $guarded      = [];
    public $timestamps      = false;

    public function __construct(){
        parent::__construct();
        $this->connection = "db".session("th_ajaran");
    }

    public function getjurusan(){
        return $this->belongsTo("App\\Models\\tb_jurusan", "KD_JURUSAN", "kd_jurusan");
    }
}
