<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletingTrait;

class detkelompokojt extends Model
{
    //
    // use SoftDeletingTrait;
    protected $connection   = "db2019";
    protected $table        = "o_detkelompokojt";
    protected $guarded      = [];
    // protected $dates = ['deleted_at'];

    public function __construct(){
        parent::__construct();
        $this->connection = "db".session("th_ajaran");
    }

    public function getdu(){
        return $this->belongsTo("App\\Models\\du", "nim", "NIM");
    }

    public function getkelompokojt(){
        return $this->belongsTo("App\\Models\\kelompokojt", "no_kelompok", "no_kelompok");
    }
}
