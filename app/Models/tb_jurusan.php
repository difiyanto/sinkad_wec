<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tb_jurusan extends Model
{
    //
    protected $connection   = "db2019";
    protected $table        = "tb_jurusan";
    protected $guarded      = [];

    public function __construct(){
        parent::__construct();
        $this->connection = "db".session("th_ajaran");
    }

    public function getmatkul(){
        return $this->hasMany("App\\Models\\tb_matkul", "kd_jurusan", "KD_JURUSAN");
    }

}
