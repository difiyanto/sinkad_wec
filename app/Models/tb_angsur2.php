<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tb_angsur2 extends Model
{
    //
    protected $connection   = "db2020";
    protected $table        = "tb_angsur";
    protected $guarded      = [];

    public function getjurusan(){
        return $this->hasMany("App\\Models\\tb_jurusan", "KD_JURUSAN", "KD_JURUSAN");
    }

}
