<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        dd(Auth::guard($guard)->check());
        if (Auth::guard($guard)->check()) {
            return $next($request);
        }else{
            return redirect("/admin/login.html");
        }
        // dd(Auth::guard("fo"));
        // if(Auth::guard($guard)->check()){
        //     dd(Auth::guard("fo")->check())." sdfdsf";
        // }else{
        //     dd(Auth::guard("fo")->check())." sdfs";
        // }

    }
}
