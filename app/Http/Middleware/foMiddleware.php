<?php

namespace App\Http\Middleware;

use Closure;

class foMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(session('login_fo') <> 1){
            return redirect()->to('/admin/login.html');
        }
        return $next($request);
    }
}
