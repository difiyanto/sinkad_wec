<?php

namespace App\Http\Middleware;

use Closure;

class eccMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(session('login_ecc') <> 1){
            return redirect()->to('admin/login.html');
        }
        return $next($request);
    }
}
