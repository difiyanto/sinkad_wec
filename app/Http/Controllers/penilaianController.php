<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Exception;

use App\Models\kelompokojt;
use App\Models\tbkelompokojt;
use App\Models\tbnilai;
use App\Models\du;

class penilaianController extends Controller
{
    //

    public function inputNilai(){
        $kelompok = kelompokojt::where("bolehujian", "1")->get();
        return view("ojt_page.nilai.input", compact("kelompok"));
    }

    public function lihatNilai(){
        $kelompokojt = kelompokojt::with(["getdetail","gettbkelompok"])->has("gettbkelompok")->get();
        return view("ojt_page.nilai.lihat", compact("kelompokojt"));
    }

    public function cari($nokelompok){
        $kelompok = kelompokojt::with("gettbkelompok","getdetail", "getperusahaan", "getpembimbing1", "getpembimbing2")->where("no_kelompok", $nokelompok)->first();
        $jenis = $kelompok->sts_pencarian <> 2 ? "OJT" : "TA";
        $bulan = Date("F", strtotime("2020/".$kelompok->bulan."/02"));
        $data = '
        <div class="form-horizontal">
            <div class="form-group row">
                <label class="col-sm-3 form-control-label"><b>Nomor Kelompok</b></label>
                <div class="col-sm-9">
                    <input id="no_kelompok" type="text"  class="form-control form-control-success" value="'.$kelompok->no_kelompok.'" readonly="true" name="no_kelompok">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 form-control-label"><b>Nama Perusahaan</b></label>
                <div class="col-sm-9">
                    <input id="nmperusahaan" type="text" class="form-control form-control-success" value="'.$kelompok->getperusahaan->nama_perusahaan.'" readonly="true" name="nmperusahaan">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 form-control-label"><b>Jenis Pengajuan</b></label>
                <div class="col-sm-9">
                    <input id="jenispengajuan" type="text" class="form-control form-control-success" value="'.$jenis.'" readonly="true" name="jenispengajuan">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 form-control-label"><b>Bulan</b></label>
                <div class="col-sm-9">
                    <input id="bulan" type="text" class="form-control form-control-success" value="'.$bulan.'" readonly="true" name="bulan">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 form-control-label"><b>Lama OJT/TA</b></label>
                <div class="col-sm-9">
                    <input id="lama" type="text" class="form-control form-control-success" value="'.$kelompok->lama.'" readonly="true" name="lama">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 form-control-label"><b>Pembimbing 1</b></label>
                <div class="col-sm-9">
                    <input id="pembinbing1" type="text" class="form-control form-control-success" value="'.$kelompok->getpembimbing1->nama.'" readonly="true" name="pembinbing1">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 form-control-label"><b>Pembimbing 2</b></label>
                <div class="col-sm-9">
                    <input id="pembinbing2" type="text" class="form-control form-control-success" value="'.$kelompok['getpembimbing2']['nama'].'" readonly="true" name="pembinbing2">
                </div>
            </div>
            <div class="form-group row">
            <label class="col-sm-3 form-control-label"><b>Judul Laporan</b></label>
            <div class="col-sm-9">
                <input id="judul" type="text" class="form-control form-control-success" value="'.$kelompok->gettbkelompok["judul_lap"].'" name="judul" style="margin-bottom:10px">

                <input id="xjudul" type="hidden" value="'.$kelompok->gettbkelompok["judul_lap"].'">
                <button class="btn btn-primary btn-sm" id="btnJudul">Simpan Judul Laporan</button>
            </div>
        </div>
        </div>
        ';
        $data .= '
        <table class="table table-responsive table-bordered table-striped">
            <thead>
                <tr>
                    <th>NIM</th>
                    <th>Nama Mahasiswa</th>
                    <th>Kelas</th>
                    <th>Nilai</th>
                </tr>
            </thead>
            <tbody>';
        $index = 1;
        foreach ($kelompok->getdetail as $detail) {
        if(substr(session("th_ajaran"), -3) == "roi"){
            $kd_matkul = "";
        }else{
            $kd_matkul = $detail->getdu->getmhsdaft->getjurusan->getmatkul->where("matakuliah", "UJIAN KOMPREHENSIF")->first()->kd_matkul;
        }
        
        $kd_matkul2 = $detail->getdu->getmhsdaft->getjurusan->getmatkul->where("matakuliah", "NILAI OJT")->first()->kd_matkul;
        
        if(substr(session("th_ajaran"), -3) == "roi"){
            $nilai = $detail->getdu->getnilai->where("kd_matkul", $kd_matkul2);
        }else{
            $nilai = $detail->getdu->getnilai->where("kd_matkul", $kd_matkul);
        }
        $currentNilai = count($nilai) > 0 ? $nilai->first()->nilaiakhir : "";
        
        $data .='<tr>
                    <td style="width:10%">'.$detail->nim.'</td>
                    <td style="width:40%">'.$detail->getdu->getmhsdaft->NAMA.'</td>
                    <td style="width:10%">'.$detail->getdu->KELAS.'</td>
                    <td>
                    <span class="" style="float:left;padding-top:5px;padding-right:5px;font-weight:bold" id="currentNilai-'.$index.'">'.$currentNilai.'</span>
                    <input type="hidden" name="" class="" value="'.$currentNilai.'" id="xcurrentNilai-'.$index.'">
                    <input type="text" id="'.$index.'" data-nim="'.$detail->nim.'" data-matkul="'.$kd_matkul.'" data-matkul2="'.$kd_matkul2.'" name="" class="form-control newNilai" style="width:30%;float:left" placeholder="input nilai..."> 
                    <span class="text-danger text-sm col-sm-4"> <strong>* tekan enter untuk simpan</strong></span>
                    </td>
                </tr>';
        $index++;
        }
        $data .='</tbody>
        </table>
        
        <script>
        $(function(){
            $(".newNilai").change(function(){
                var nilai = $(this).val()
                var nim = $(this).attr("data-nim")
                var id = $(this).attr("id")
                var next = parseInt(id) + 1
                var kdmatkul = $(this).attr("data-matkul")
                var kdmatkul2 = $(this).attr("data-matkul2")
                var xurl = $("#xcurrentNilai-"+id).val() == "" ? "'.url('/ojt/input-nilai/new/').'" : "'.url('/ojt/input-nilai/update/').'"
                $.ajax({
                    url:xurl,
                    type:"GET",
                    data:{nim:nim, kdmatkul:kdmatkul, kdmatkul2:kdmatkul2, nilai:nilai},
                    success:function(r){
                        console.log(r)
                        var res = JSON.parse(r)
                        var view = "<h4 class=text-danger>Tidak bisa memasukkan nilai</h4>"
                        view += "<br><ul>"
                        if(res["nilai"] == 0){
                            view += "<li>Terdapat nilai lain yang belum lengkap</li>"
                        }
                        if(res["administrasi"] == 0){
                            view += "<li>Terdapat administrasi yang belum terselesaikan</li>"
                        }
                        
                        view += "</ul>"
                        $("#currentNilai-"+id).html(nilai)
                        $("#xcurrentNilai-"+id).val(nilai)
                        $("#"+id).val("")
                        $("#"+next).focus()

                        if(res["nilai"] == 0 || res["administrasi"] == 0){
                            $("#data_mhs").html(view)
                            $("#modal_ceknim").modal()
                        }
                    },
                    error:function(e){
                        console.log(e.responseText)
                        alert("Terjadi Kesalahan !")
                    }
                })
            })

            $("#btnJudul").click(function(e){
                e.preventDefault()
                var judul = $("#judul").val()
                var kd_kelompok = $("#no_kelompok").val()
                var yurl = $("#xjudul").val() == "" ?  "'.url('/ojt/input-nilai/saveJudul/').'" : "'.url('/ojt/input-nilai/updateJudul/').'"
                $.ajax({
                    url:yurl,
                    type:"GET",
                    data:{judul:judul, no_kelompok:kd_kelompok},
                    success:function(r){
                        console.log(r)
                        Swal.fire(
                            "Judul Laporan Berhasil Disimpan",
                            "",
                            "success"
                        )
                        $("#xjudul").val(judul)
                    },
                    error:function(e){
                        console.log(e.responseText)
                        alert("Terjadi Kesalahan !")
                    }
                })
            })
        })
        </script>
        ';
        return $data;
    }

    public function baru(Request $req){
        $sts_nilai = 1;
        $sts_adminis = 1;
        //nilai
        $nilai = $this->cek_nilai($req->nim);
        if($nilai == 1){
            $sts_nilai = 0;
        }
        $kebijakan_nilai = $this->kebijakan_nilai($req->nim);
        if($kebijakan_nilai == 1){
            $sts_nilai = 1;
        }

        //adminis
        $sts_adminis = $this->cek_administrasi($req->nim);
        // dd($sts_adminis);
        $kebijakan_adminis = $this->kebijakan_admin($req->nim);
        if($kebijakan_adminis == 1){
            $sts_adminis = 1;
        }

        if($sts_nilai == 1 || $sts_adminis == 1){
            $nilai = $req->nilai > 90 ? 90 : $req->nilai;
            try {
                if($req->kdmatkul != ''){
                    $save = tbnilai::insert([
                        "NIM"           => $req->nim,
                        "kd_matkul"     => $req->kdmatkul,
                        "nilaiutama"    => $nilai,
                        "nilaiakhir"    => $nilai
                    ]);
                    
                }
                
    
                $save2 = tbnilai::insert([
                    "NIM"           => $req->nim,
                    "kd_matkul"     => $req->kdmatkul2,
                    "nilaiutama"    => $nilai,
                    "nilaiakhir"    => $nilai
                ]);
                $sts = 200;
                $msg = "success";
            } catch (Exception $e) {
                $sts = 500;
                $msg = "error . ".$e;
            }

        }
            // return "sdf";
        return json_encode([
            "status"    =>$sts, 
            "message"   =>$msg,
            "nilai"     => $sts_nilai,
            "administrasi"  => $sts_adminis
        ]);
    }

    public function update(Request $req){
        $sts_nilai = 1;
        //nilai
        $nilai = $this->cek_nilai($req->nim);
        if($nilai == 1){
            $sts_nilai = 0;
        }
        $kebijakan_nilai = $this->kebijakan_nilai($req->nim);
        if($kebijakan_nilai == 1){
            $sts_nilai = 1;
        }

        //adminis
        $sts_adminis = $this->cek_administrasi($req->nim);
        // dd($sts_adminis);
        $kebijakan_adminis = $this->kebijakan_admin($req->nim);
        if($kebijakan_adminis == 1){
            $sts_adminis = 1;
        }
        
        if($sts_nilai == 1 || $sts_adminis == 1){
            $nilai = $req->nilai > 90 ? 90 : $req->nilai;
            $update = tbnilai::where("NIM", $req->nim)->where("kd_matkul", $req->kdmatkul)->update([
                "nilaiutama"    => $nilai,
                "nilaiakhir"    => $nilai
            ]);
            $update = tbnilai::where("NIM", $req->nim)->where("kd_matkul", $req->kdmatkul2)->update([
                "nilaiutama"    => $nilai,
                "nilaiakhir"    => $nilai
            ]);
        }
        $sts = 200;
        $msg = "success";

        return json_encode([
            "status"    => $sts, 
            "message"   => $msg,
            "nilai"     => $sts_nilai,
            "administrasi"  => $sts_adminis
        ]);
    }

    public function saveJudul(Request $req){
        $kelompokojt = kelompokojt::with("getdetail")->where("no_kelompok", $req->no_kelompok)->first(); 
        try {
            
            $src = tbkelompokojt::where("kode_kelompok", $req->no_kelompok)->count();

            if($src > 0){
                $save = tbkelompokojt::where("kode_kelompok", $req->no_kelompok)->update([
                    "judul_lap"	        => strtoupper($req->judul),
                ]);
            }else{
                $save = tbkelompokojt::insert([
                    "kode_kelompok"	        => $req->no_kelompok,
                    "kode_perusahaan"	    => $kelompokojt->sts_pencarian == 2 || $kelompokojt->kd_perusahaan == 1 ? "2694" : $kelompokojt->kd_perusahaan,
                    "NIP"		            => $kelompokojt->nip,
                    "bulan"                 => $kelompokojt->bulan,
                    "no_prop1"	            => $req->no_kelompok,
                    "kode_perusahaan1"	    => $kelompokojt->sts_pencarian == 2 || $kelompokojt->kd_perusahaan == 1 ? "2694" : $kelompokojt->kd_perusahaan,
                    "tgl1"		            => $kelompokojt->created_at,
                    "judul_lap"	            => strtoupper($req->judul),
                    "cekstatus"	            => $kelompokojt->sts_pencarian == 2 || $kelompokojt->kd_perusahaan == 1 ? 2 : $kelompokojt->sts_pencarian,
                    "sts1"		            => "1"
                ]);
            }

            foreach ($kelompokojt->getdetail as $k) {
                $update = du::where("NIM", $k->nim)->update([
                    "KDOJT" => $req->no_kelompok
                ]);
            }
            $sts = 200;
            $msg = "success";
        } catch (Exception $e) {
            $sts = 500;
            $msg = "error . ".$e;
        }

        return json_encode(["status"=>$sts, "message"=>$msg]);
    }

    public function updateJudul(Request $req){
        $kelompokojt = kelompokojt::with("getdetail")->where("no_kelompok", $req->no_kelompok)->first(); 
        try {
            $save = tbkelompokojt::where("kode_kelompok", $req->no_kelompok)->update([
                "judul_lap"	        => strtoupper($req->judul),
            ]);
            foreach ($kelompokojt->getdetail as $k) {
                $update = du::where("NIM", $k->nim)->update([
                    "KDOJT" => $req->no_kelompok
                ]);
            }
            $sts = 200;
            $msg = "success";
        } catch (Exception $e) {
            $sts = 500;
            $msg = "error . ".$e;
        }

        return json_encode(["status"=>$sts, "message"=>$msg]);
    }
}
