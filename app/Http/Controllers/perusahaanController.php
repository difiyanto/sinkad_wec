<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
//model
use App\Models\perusahaan;

//additional


class perusahaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */



    public function index()
    {
        //
        $result = [
            "perusahaan"    => perusahaan::all()
        ];
        return view("ojt_page.perusahaan.index", $result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view("ojt_page.perusahaan.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        //
        $this->validate($req, [
            "nm_perusahaan"         => "required",
            "alamat_perusahaan"     => "required",
            "kota"                  => "required",
            "telp"                  => "required",
            "telp_penghubung"       => "required",
        ],[
            "required"              => "Tidak boleh kosong"
        ]);

        $dperusahaan = perusahaan::orderBy("kode_perusahaan", "DESC")->first();

        $insert = perusahaan::insert([
            "kode_perusahaan"       => $dperusahaan->kode_perusahaan + 1,
            "nama_perusahaan"       => $req->nm_perusahaan,
            "alamat"                => $req->alamat_perusahaan,
            "kota"                  => $req->kota,
            "telp"                  => $req->telp,
            "nama_Penghubung"       => $req->nm_penghubung,
            "telp_penghubung"       => $req->telp_penghubung,
            "createdby"             => 1
        ]);

        return redirect()->to("ojt/perusahaan/data.html")->with(["stsPerusahaan" => 1]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $perusahaan = perusahaan::find($id);
        $result = [
            "perusahaan"    => $perusahaan,
        ];
        return view("ojt_page.perusahaan.edit", $result);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        //
        $this->validate($req, [
            "nm_perusahaan"         => "required",
            "alamat_perusahaan"     => "required",
            "kota"                  => "required",
            "telp"                  => "required",
            "nm_penghubung"         => "required",
            "telp_penghubung"       => "required",
        ],[
            "required"  => "Tidak boleh kosong"
        ]);
        $tb = perusahaan::find($id);
        $tb->nama_perusahaan    = $req->nm_perusahaan;
        $tb->alamat             = $req->alamat_perusahaan;
        $tb->kota               = $req->kota;
        $tb->telp               = $req->telp;
        $tb->nama_Penghubung    = $req->nm_penghubung;
        $tb->telp_penghubung    = $req->telp_penghubung;
        $tb->save();
        return redirect()->to("ojt/perusahaan/data.html")->with(["stsPerusahaan" => 1]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            perusahaan::find($id)->delete();
            $msg = "berhasil";
            $sts = 1;
        } catch (Exception $e) {
            //throw $th;
            $msg = $e;
            $sts = 0;
        }
        return json_encode(["sts"=>$sts, "msg"=>$msg]);
    }

    public function softdestroy($id)
    {
        //
        try {
            perusahaan::find($id)->update(["softdelete"=>1]);
            $msg = "berhasil";
            $sts = 1;
        } catch (Exception $e) {
            //throw $th;
            $msg = $e;
            $sts = 0;
        }
        return json_encode(["sts"=>$sts, "msg"=>$msg]);
    }

    public function getjson(Request $req){
        $columns = array(
            0   => "nama_perusahaan",
            1   => "alamat",
            2   => "kota",
            3   => "nama_Penghubung"
        );

        $totalData = perusahaan::count();

        $totalFiltered = $totalData;
        $limit = $req->input("length");
        $start = $req->input("start");
        $order = $columns[$req->input("order.0.column")];
        $dir = $req->input("order.0.dir");

        if(empty($req->input('search.value')))
        {            
            $perusahaans = perusahaan::offset($start)
                         ->limit($limit)
                         ->orderBy($order,$dir)
                         ->get();
        }else {
            $search = $req->input('search.value'); 

            $perusahaans =  perusahaan::where('kode_perusahaan','LIKE',"%{$search}%")
                            ->orWhere('nama_perusahaan', 'LIKE',"%{$search}%")
                            ->orWhere('alamat', 'LIKE',"%{$search}%")
                            ->orWhere('kota', 'LIKE',"%{$search}%")
                            ->orWhere('nama_Penghubung', 'LIKE',"%{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            $totalFiltered = perusahaan::where('kode_perusahaan','LIKE',"%{$search}%")
                            ->orWhere('nama_perusahaan', 'LIKE',"%{$search}%")
                            ->orWhere('alamat', 'LIKE',"%{$search}%")
                            ->orWhere('kota', 'LIKE',"%{$search}%")
                            ->orWhere('nama_Penghubung', 'LIKE',"%{$search}%")
                             ->count();
        }

        $data = array();
        if(!empty($perusahaans))
        {
            $no = 1;
            foreach ($perusahaans as $peru)
            {
                $edit = url('/ojt/perusahaan/edit-'.$peru->kode_perusahaan.".html");
                $nestedData['no']               = $no;
                $nestedData['nama_perusahaan']  = $peru->nama_perusahaan;
                $nestedData['alamat']           = $peru->alamat;
                $nestedData['kota']             = $peru->kota;
                $nestedData['nama_Penghubung']  = $peru->nama_Penghubung;
                $nestedData['aksi']             = "
                <a href='{$edit}' class='btn btn-info btn-xs' data-toggle='tooltip' data-placement='bottom' title='update'> <i class='fa fa-pencil'></i> </a>

                <button class='btn btn-danger btn-xs' onclick='deletex({$peru->kode_perusahaan})' data-toggle='tooltip' data-placement='bottom' title='delete'> <i class='fa fa-trash'></i> </button>
                ";
                $no++;
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($req->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
            );

        echo json_encode($json_data); 
    }
}
