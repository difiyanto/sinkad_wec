<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//model
use App\Models\kelompokojt;

//additional
use PDF;
use DB;
use Exception;
use App\Http\Requests;


class pengajarController extends Controller
{
    //

    public function index(){
        return view("pengajar_page.index");
    }

    public function accpemberangkatan(){
        // dd(session("divisi"));
        if(session("divisi") == "DOSEN"){
            $kelompok = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                        ->join("tbperusahaan", "tbperusahaan.kode_perusahaan","=","o_kelompokojt.kd_perusahaan")
                        ->where("o_kelompokojt.apr_pemberangkatan21",null)
                        ->where("o_kelompokojt.nip", session("nip"))
                        ->get();
                        
            $kelompok2 = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                        ->join("tbperusahaan", "tbperusahaan.kode_perusahaan","=","o_kelompokojt.kd_perusahaan")
                        ->where("o_kelompokojt.apr_pemberangkatan21",1)
                        ->where("o_kelompokojt.nip", session("nip"))
                        ->where("o_kelompokojt.bolehujian", "<>",1)
                        ->get();

            $kelompok3 = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                        ->join("tbperusahaan", "tbperusahaan.kode_perusahaan","=","o_kelompokojt.kd_perusahaan")
                        ->where("o_kelompokojt.apr_pemberangkatan21",1)
                        ->where("o_kelompokojt.nip", session("nip"))
                        ->where("o_kelompokojt.bolehujian", 1)
                        ->get();
        }else{
            $kelompok = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                        ->join("tbperusahaan", "tbperusahaan.kode_perusahaan","=","o_kelompokojt.kd_perusahaan")
                        ->where("o_kelompokojt.apr_pemberangkatan22",null)
                        ->where("o_kelompokojt.nip2", session("nip"))
                        ->get();
          
            
            $kelompok2 = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                        ->join("tbperusahaan", "tbperusahaan.kode_perusahaan","=","o_kelompokojt.kd_perusahaan")
                        ->where("o_kelompokojt.apr_pemberangkatan22",1)
                        ->where("o_kelompokojt.nip2", session("nip"))
                        ->where("o_kelompokojt.bolehujian", "<>" ,1)
                        ->get();

            

            $kelompok3 = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                        ->join("tbperusahaan", "tbperusahaan.kode_perusahaan","=","o_kelompokojt.kd_perusahaan")
                        ->where("o_kelompokojt.apr_pemberangkatan22",1)
                        ->where("o_kelompokojt.nip2", session("nip"))
                        ->where("o_kelompokojt.bolehujian", 1)
                        ->get();
            
        }
        return view("pengajar_page.pemberangkatan", ["kelompok"     => $kelompok, 
                                                     "kelompok2"    => $kelompok2,
                                                     "kelompok3"    => $kelompok3]);
    }

    public function detailpemberangkatan($nokelompok){
        $kelompok = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                    ->join("tbperusahaan","tbperusahaan.kode_perusahaan","=","o_kelompokojt.kd_perusahaan")
                    ->join("o_detkelompokojt","o_detkelompokojt.no_kelompok","=","o_kelompokojt.no_kelompok")
                    ->join("du","du.NIM","=","o_detkelompokojt.nim")
                    ->join("mhsdaft","mhsdaft.NPM","=","du.NPM")
                    ->where("o_kelompokojt.no_kelompok",$nokelompok)
                    ->get();
        $kelompok = kelompokojt::with(["getperusahaan", "getpembimbing1", "getpembimbing2", "getdetail"=>function($q){
            $q->with(["getdu"=>function($q){
                $q->with("getmhsdaft")->get();
            }])->get();
        }])->where("no_kelompok", $nokelompok)->first();
        // dd($kelompok);
        return view("pengajar_page.detailpemberangkatan",compact("kelompok"));
    }

    public function updatepemberangkatan($nokelompok){
        if(session("divisi") == "DOSEN"){
            $update = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                      ->where("no_kelompok", $nokelompok)->update([
                          "apr_pemberangkatan21"    => "1",
                          "sts_kelompok"            =>"berangkat OJT"
                        ]);
        }else{
            $update = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                      ->where("no_kelompok", $nokelompok)->update([
                          "apr_pemberangkatan22"    => "1",
                        ]);
        }
        
        return redirect()->back()->with(["sts_pemberangkatan" => "1" ]);
    }

    public function accujian($no_kelompok){

        //update boleh ujian
        try {
            
            $update = kelompokojt::where("no_kelompok", $no_kelompok)->update(["bolehujian"=>1]);
            $status = 1;
            $pesan  = "berhasil";
        } catch (Exception $e) {
            //throw $th;
            $status = 2;
            $pesan  = "gagal . ".$e;
        }

        // dd($no_kelompok);

        return redirect()->back()->with([
            "status"    => $status,
            "pesan"     => $pesan
        ]);
    }

    public function cetakbimbingan($no_kelompok){
         //cek semua syarat OJT
        $kelompok = $this->cekSyaratPemberangkatanByNokelompok($no_kelompok);
        for($j = 0; $j<count($kelompok);$j++){
            if($kelompok[$j]["sts_absensi"] == 0 || $kelompok[$j]["sts_adminis"] == 0 || $kelompok[$j]["sts_nilai"] == 0){
                return view("pengajar_page.cekGagal", ["kelompok" => $kelompok]);        
                break;
            }
        }

        $update = kelompokojt::where("no_kelompok", $no_kelompok)->update(["pulang" => 1]);
        $ojt = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                ->join("tbperusahaan", "tbperusahaan.kode_perusahaan","=","o_kelompokojt.kd_perusahaan")
                ->join("o_detkelompokojt","o_detkelompokojt.no_kelompok","=","o_kelompokojt.no_kelompok")
                ->join("du","du.NIM","=","o_detkelompokojt.nim")
                ->join("mhsdaft","mhsdaft.NPM","=","du.NPM")
                ->select("o_kelompokojt.no_kelompok","tbperusahaan.nama_perusahaan","tbperusahaan.alamat","du.NIM","mhsdaft.NAMA","du.KELAS","mhsdaft.KD_JURUSAN", "mhsdaft.GEL_DAFTAR", "o_kelompokojt.nip", "o_kelompokojt.nip2", "o_kelompokojt.sts_pencarian", "o_kelompokojt.bulan")
                ->where("o_kelompokojt.no_kelompok",$no_kelompok)->get();

        $pembimbing = DB::connection("db".session("th_ajaran"))->table("tbpegawai")
                    ->where("NIP", $ojt[0]["nip"])->orWhere("NIP", $ojt[0]["nip2"])->orderBy("jabatan","DESC")->get();
        $result = [
            "kelompok"      => $ojt,
            "pembimbing"    => $pembimbing
        ];
        $pdf = PDF::loadView("mhs_page.berkas.formbimbingan", $result, [], [
            "format"            => [210, 330],
            "margin_left"       => "20",
            "margin_right"       => "20"
        ]);
        return $pdf->stream("mhs_page.berkas.formbimbingan");
    }

    public function cetakform($no_kelompok){
        
        $kelompok = $this->cekSyaratPemberangkatanByNokelompok($no_kelompok);
        for($j = 0; $j<count($kelompok);$j++){
            if($kelompok[$j]["sts_absensi"] == 0 || $kelompok[$j]["sts_adminis"] == 0 || $kelompok[$j]["sts_nilai"] == 0){
                return view("pengajar_page.cekGagal", ["kelompok" => $kelompok]);        
                break;
            }
        }
        $ojt = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                ->join("tbperusahaan", "tbperusahaan.kode_perusahaan","=","o_kelompokojt.kd_perusahaan")
                ->join("o_detkelompokojt","o_detkelompokojt.no_kelompok","=","o_kelompokojt.no_kelompok")
                ->join("du","du.NIM","=","o_detkelompokojt.nim")
                ->join("mhsdaft","mhsdaft.NPM","=","du.NPM")
                ->select("o_kelompokojt.no_kelompok","tbperusahaan.nama_perusahaan","tbperusahaan.alamat","du.NIM","mhsdaft.NAMA","du.KELAS","mhsdaft.KD_JURUSAN", "mhsdaft.GEL_DAFTAR", "o_kelompokojt.nip", "o_kelompokojt.nip2", "o_kelompokojt.sts_pencarian", "o_kelompokojt.bulan")
                ->where("o_kelompokojt.no_kelompok",$no_kelompok)->get();
        $pembimbing = DB::connection("db".session("th_ajaran"))->table("tbpegawai")
        ->where("NIP", $ojt[0]["nip"])->Orwhere("NIP", $ojt[0]["nip"])->get();
        $pembimbing1 = DB::connection("db".session("th_ajaran"))->table("tbpegawai")
                    ->where("NIP", $ojt[0]["nip"])->get();
        $pembimbing2 = DB::connection("db".session("th_ajaran"))->table("tbpegawai")
        ->where("NIP", $ojt[0]["nip2"])->get();
        $result = [
            "kelompok"      => $ojt,
            "pembimbing"     => $pembimbing,
            "pembimbing1"    => $pembimbing1,
            "pembimbing2"    => $pembimbing2,
        ];
        $pdf = PDF::loadView("mhs_page.berkas.formujian", $result, [], [
            "format"            => [215, 250],
            "margin_left"       => "20",
            "margin_right"       => "20"
        ]);
        return $pdf->stream("mhs_page.berkas.formujian");
    }   

    public function cetaklampiran($no_kelompok){
        $ojt = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                ->join("tbperusahaan", "tbperusahaan.kode_perusahaan","=","o_kelompokojt.kd_perusahaan")
                ->join("o_detkelompokojt","o_detkelompokojt.no_kelompok","=","o_kelompokojt.no_kelompok")
                ->join("du","du.NIM","=","o_detkelompokojt.nim")
                ->join("mhsdaft","mhsdaft.NPM","=","du.NPM")
                ->select("o_kelompokojt.no_kelompok","tbperusahaan.nama_perusahaan","tbperusahaan.alamat","du.NIM","mhsdaft.NAMA","du.KELAS","mhsdaft.KD_JURUSAN", "mhsdaft.GEL_DAFTAR", "o_kelompokojt.nip", "o_kelompokojt.nip2", "o_kelompokojt.sts_pencarian", "o_kelompokojt.bulan")
                ->where("o_kelompokojt.no_kelompok",$no_kelompok)->get();
        $result = [
            "kelompok"      => $ojt,
        ];

        $pdf = PDF::loadView("mhs_page.berkas.lampiranformujian", $result, [], [
            "format"            => "legal",
            "margin_left"       => "12",
            "margin_right"       => "12"
        ]);
        return $pdf->stream("mhs_page.berkas.lampiranformujian");
    }
}
