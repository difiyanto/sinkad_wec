<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Http\Requests;

use App\Models\pengguna;

class AdminController extends Controller
{
    //

    public function login(){
        return view("admin_page.login");
    }

    public function loginPost(Request $req){
        $this->validate($req,[
            "slc_th"        => "required",
            "slc_admin"     => "required",
            "username"      => "required",
            "password"      => "required"
        ],[
            "required"              => "Tidak boleh kosong.",
            "slc_admin.required"    => "Tipe admin belum dipilih.",
            "slc_th.required"    => "Tahun ajaran belum dipilih."
        ]);

        session()->put("th_ajaran", $req->slc_th);
        $password = md5($req->password.md5("fika"));
        // dd($password);
        switch ($req->slc_admin) {
            case 'ecc':
                if($req->username = "ecc" and $req->password == "ecc3030"){
                    session()->put("login_ecc", 1);
                    return redirect()->to("ecc/");
                }else{
                    return redirect()->back()->with(["sts_login" => "0"]);
                }
                break;

            case 'ojt':
                if($req->username = "ojt" and $req->password == "ojt2525"){
                    session()->put("login_ojt", 1);
                    return redirect()->to("ojt");
                }else{
                    return redirect()->back()->with(["sts_login" => "0"]);
                }
                break;

            case 'fo':
                    // $fo = pengguna::where("username", $req->username)->where("password", $password)->where("otoritas", $req->slc_admin)->first();
                    if($req->username = "fo" and $req->password == "fo74"){
                        // dd($fo->Id);
                        // $x = Auth::guard($req->slc_admin)->LoginUsingId($fo->Id);
                        session()->put("login_fo", 1);
                        return redirect("/fo");
                    }else{
                        return redirect()->back()->with(["sts_login" => "0"]);
                    }
                break;
            
            case 'pengajar' :
                $nip        = substr($req->password, 0 , 13);
                $pengajar   = DB::connection("db".session("th_ajaran"))->table("tbpegawai")
                                ->where("st_peg","A")->where("Divisi","like","%DOS%")
                                ->where("NIP", $nip);
                if($pengajar->count() > 0){
                    $Dpengajar = $pengajar->get();
                    session()->put("login_pengajar", 1);
                    session()->put("nama_pengajar", $Dpengajar[0]['nama']);
                    session()->put("jabatan_pengajar", $Dpengajar[0]['jabatan']);
                    session()->put("nip", $Dpengajar[0]['NIP']);
                    session()->put("divisi", $Dpengajar[0]['Divisi']);

                    //cek bendahara wisuda
                    if($nip == "1998090127089"){
                        session()->put("wisuda", 1);
                    }

                    //cek keinstrukturan
                    $Kpengajar = $pengajar->where("jabatan", "like", "%keinstrukturan%")->get();
                    if(count($Kpengajar) > 0 ){
                        session()->put("keinstrukturan", 1);
                    }

                    return redirect()->to("pengajar");
                }else{
                    return redirect()->back()->with([
                        "sts_login" => "0"
                    ]);
                }
                break;
            

            
            default:
                # code...
                break;
        }
    }
}
