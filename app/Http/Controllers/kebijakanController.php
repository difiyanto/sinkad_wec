<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

//models
use App\Models\kebijakan;
use App\Models\du;

class kebijakanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $url1;
    public function __construct(){
       $this->url1 = request()->segment(1);
    } 
    
    public function index()
    {
        //
        $result = [
            "kebijakan" => kebijakan::with("getdu","getmhsdaft")->get()
        ];

        // dd($this->url1);
        return view("{$this->url1}_page.kebijakan.index", $result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $result = [
            "du"    => du::all()
        ];
        return view("{$this->url1}_page.kebijakan.create", $result);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        //
        $this->validate($req, [
            "npm"           => "required",
            "jenis"         => "required",
            "batas"         => "required",
            "keterangan"    => "required"
        ], [
            "required"      => "Tidak boleh kosong"
        ]);

        $tb                     = new kebijakan;
        $tb->NPM                = $req->npm;
        $tb->bts_kebijakan      = $req->batas;
        $tb->jenis_kebijakan    = $req->jenis;
        $tb->keterangan         = $req->keterangan;
        $tb->save();

        return redirect()->to("/{$this->url1}/kebijakan/data.html")->with(["status"=>1]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $kebijakan = kebijakan::find($id);
        $result = [
            "kebijakan" => $kebijakan,
            "du"        => du::all()
        ];
        return view("{$this->url1}_page.kebijakan.edit", $result);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        //
        $this->validate($req, [
            "npm"           => "required",
            "jenis"         => "required",
            "batas"         => "required",
            "keterangan"    => "required"
        ], [
            "required"      => "Tidak boleh kosong"
        ]);

        $tb = kebijakan::find($id);
        $tb->NPM = $req->npm;
        $tb->bts_kebijakan = $req->batas;
        $tb->jenis_kebijakan = $req->jenis;
        $tb->keterangan = $req->keterangan;
        $tb->save();
        return redirect()->to("{$this->url1}/kebijakan/data.html")->with(["status"=>1]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            kebijakan::find($id)->delete();
            $msg = "berhasil";
            $sts = 1;
        } catch (Exception $e) {
            //throw $th;
            $msg = $e;
            $sts = 0;
        }
        return json_encode(["sts"=>$sts, "msg"=>$msg]);
    }
}
