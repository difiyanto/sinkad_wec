<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;

//models
use App\Models\sync\tb_sinkad;
use App\Models\sync\angsuranmaba;
use App\Models\sync\detail_angsuranmaba;
use App\Models\tb_angsuran2;
use App\Models\tb_hasil2;
use App\Models\tb_angsur2;
use App\Models\sync\angsur;

class angsuranController extends Controller
{
    //

    public function sync(){
        $mhs    = tb_sinkad::where("cabang", 1)->where("nim", "2020110063")->get();
        
        foreach ($mhs as $m) {
            $gel            = $m->gel;
            $hasil          = tb_hasil2::where("NIM", $m->nim)->first();
            $sudah_bayar    = $hasil->TOTAL_BYR;
            $lunas          = $hasil->LUNAS;
            $beasiswa       = $hasil->BEASISWA;
            
            $kd_jurusan     = substr($m->nim, 5, 2);
            $kd_jurusan     = substr($kd_jurusan, -1) == "0" ? substr($kd_jurusan, 0, 1) : $kd_jurusan;
            $no_ans         = 1;
            $angsuranMaba   = angsuranmaba::where("NIM", $m->nim)->get();
            
            if(count($angsuranMaba) > 0){
                foreach ($angsuranMaba as $a) {
                    $nokwt          = $a->no_kwitansi;
                    $detailAngsuran = detail_angsuranmaba::where("no_kwitansi", $nokwt)->first();
                    $no_ans         = $detailAngsuran->no_ans;
                    $nominal        = $detailAngsuran->jml_angsur;
                    $sudah_bayar    = $sudah_bayar - $nominal;
                }
                $no_ans++;
            }
            
            if($sudah_bayar > 0){
                // $no_ans = 1;
                
                while ($sudah_bayar > 0) {
                    # code...
                    try {
                        //code...
                        
                        $xSudahBayar    = tb_angsuran2::where("NIM", $m->nim)
                                          ->where("NO_ANS", $no_ans)->sum("JUMLAH");
                    
                        $sudah_bayar    = $sudah_bayar - $xSudahBayar; 
                        // $xSudahBayar = -685000;
                       
                        while ($xSudahBayar > 0) {
                            $angsuran       = tb_angsuran2::where("NIM", $m->nim)
                                              ->where("NO_ANS", $no_ans)->first();   

                            $harusBayar     = angsur::where("id_jurusan", $kd_jurusan)
                                              ->where("gel", $gel)->where("no_ans", $no_ans)
                                              ->where("cabang", "Malang")->first();
                            $NharusBayar    = $beasiswa == "Ya" ? $harusBayar->angsuran_beasiswa : $harusBayar->angsuran_normal;
                            
                            // create data to angsuranmaba and detail_angsuranmaba;
                            $nokwitansi = $this->nokwitansi(1, "Wearnes");
                            $srcCek = DB::connection("db2020")->table("angsuranmabas")
                                        ->join("detail_angsuranmabas", "detail_angsuranmabas.no_kwitansi", "=", "angsuranmabas.no_kwitansi")
                                        ->where("angsuranmabas.nim", $m->nim)->where("detail_angsuranmabas.no_ans", $no_ans)->get();
                            
                            if(count($srcCek) < 1 ){
                                
                                $sv = angsuranmaba::create([
                                    "NIM"                   => $m->nim,
                                    "tgl_angsur"            => $angsuran->TGL_ANS,
                                    "tipe_transaksi"        => $angsuran->TIPE_TRANS == 0 ? "bank" : "kas",
                                    "no_kwitansi"           => $nokwitansi,
                                    "keterangan_kwitansi"   => "-",
                                    "subitem_kwitansi"      => "2",
                                    "foto"                  => "-",
                                    "an"                    => "-",
                                    "statusverifikasi"      => 1,
                                    "operator_id"           => 0
    
                                ]);
                        
                                $sv2 = detail_angsuranmaba::create([
                                    "no_kwitansi"           => $nokwitansi,
                                    "keterangan"            => "-",
                                    "no_ans"                => $no_ans,
                                    "jml_angsur"            => $NharusBayar
                                ]);
                            }
                            $xSudahBayar = $xSudahBayar - $NharusBayar;
                        }
                        
                    } catch (\Throwable $th) {
                        //throw $th;
                        dd($th);
                    }
                    
                    $no_ans++;
                }
            }
            
        }
    }
}
