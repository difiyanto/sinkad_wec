@extends('pengajar_page.layout2')
@section('pemberangkatan','active')
@section('ojt','active')
@section('header')
<h1>Detail Kelompok OJT</h1>
<ol class="breadcrumb">
    <li><a href="{{url('/pengajar')}}"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li><a href="{{url('/pengajar/pemberangkatan.html')}}">Data Pemberangkatan</a></li>
    <li class="active">Detail Pemberangkatan</li>
</ol>
@endsection
@section('body')
<input type="hidden" name="sts_pemberangkatan" id="sts_pemberangkatan" data-value="{{Session::get('sts_pemberangkatan')}}">
  <div class="row">
    <div class="col-lg-8">
        <div class="box box-primary direct-chat direct-chat-warning">
            <div class="box-body" style="padding:10px">
                <form class="form-horizontal" action="{{url('pengajar/updatedetailpemberangkatan/'.$kelompok->no_kelompok)}}">
                   {{ csrf_field() }}
          
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label"><b>No Kelompok</b></label>
                    <div class="col-sm-9">
                      <input id="inputHorizontalSuccess" type="text"  class="form-control form-control-success" value="{{$kelompok->no_kelompok}}" readonly="true" name="no_kelompok">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label"><b>Tanggal Pengajuan</b></label>
                    <div class="col-sm-9">
                      <input id="inputHorizontalWarning" type="text"  class="form-control form-control-warning" value="{{$kelompok->created_at}}" readonly="true">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label"><b>Nama Perusahaan</b></label>
                    <div class="col-sm-9">
                      <input id="inputHorizontalWarning" type="text"  class="form-control form-control-warning" value="{{$kelompok->getperusahaan->nama_perusahaan}}" readonly="true">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label"><b>Alamat Perusahaan</b></label>
                    <div class="col-sm-9">
                      <textarea readonly class="form-control form-control-warning" rows="5" cols="3">{{$kelompok->getperusahaan->alamat}}</textarea>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label"><b>OJT di Bulan</b></label>
                    <div class="col-sm-9">
                      @php
                          $bulan = [ "1" => "Januari", "2"=> "Februari","3"=>"Maret","4"=>"April","5"=>"Mei","6"=>"Juni","7"=>"Juli","8"=>"Agustus","9"=>"September","10"=>"Oktober","11"=>"November","12"=>"Desember" ];
                      @endphp
                      <input id="inputHorizontalWarning" type="text"  class="form-control form-control-warning" value="{{ $bulan[$kelompok->bulan] }}" readonly="true">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label"><b>Lama OJT</b></label>
                    <div class="col-sm-9">
                      <input id="inputHorizontalWarning" type="number" name="lamaojt"  class="form-control form-control-warning" readonly  value="{{ $kelompok->lama }}" style="width:10%;float:left" > &nbsp;&nbsp;Bulan
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label"><b>Pembimbing 1</b></label>
                    <div class="col-sm-9">
                      <input id="inputHorizontalWarning" type="text" name="pembimbing1"  class="form-control form-control-warning" readonly  value="{{ $kelompok->getpembimbing1->nama }}">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label"><b>Pembimbing 2</b></label>
                    <div class="col-sm-9">
                      <input id="inputHorizontalWarning" type="text" name="pembimbing1"  class="form-control form-control-warning" readonly  value="{{ $kelompok["getpembimbing2"]["nama"] }}">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label"><b></b></label>
                    
                    <div class="col-sm-9">

                      @if (session("divisi") == "DOSEN")
                      @if ($kelompok->apr_pemberangkatan21 <> 1)
                          <button class="btn btn-primary " type="submit">Ya, saya akan bimbing kelompok ini</button>
                      @endif
                      @if ($kelompok->bolehujian == 0 and $kelompok->apr_pemberangkatan21 == 1)
                        <div class="row">
                          <div class="col-lg-12">

                            <a id="btnBimbingan" class="btn btn-primary btn-sm" href=" {{ url("/pengajar/cetak berkas bimbingan/".$kelompok->no_kelompok) }}" target="_blank"> <i class="fa fa-print"></i> Form Bimbingan</a>

                          </div>

                          <div class="col-lg-12" style="padding-top:10px;">
                            <hr>
                            
                            <a class="btn btn-primary btn-sm" href=" {{ url("/pengajar/cetak form ujian/".$kelompok->no_kelompok) }} " target="_blank"> <i class="fa fa-print"></i> Form Ujian Komprehensif</a>

                            <a class="btn btn-primary btn-sm" href=" {{ url("/pengajar/cetak lampiran ujian/".$kelompok->no_kelompok) }} " target="_blank"> <i class="fa fa-print"></i> Lampiran Form Ujian</a>

                            <a id="btnKompre" style="margin-top:10px;" class="btn btn-primary" href=" {{ url("/pengajar/bolehujian/".$kelompok->no_kelompok) }} ">Setujui kelompok ini untuk mengikuti ujian komprehensif</a>

                            
                          </div>
                        </div>
                      @endif
                    @else
                      @if ($kelompok->apr_pemberangkatan22 <> 1)
                          <button class="btn btn-primary " type="submit">Ya, saya akan bimbing kelompok ini</button>
                      @endif
                    @endif 
                    
                    </div>
                  </div>

           
            </form>
            </div>
        </div>

    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="box box-primary direct-chat direct-chat-warning">
            <div class="box-body" style="padding:10px">

                <h4>Anggota Kelompok</h4>
                <div class="table-responsive" style="margin-left:15px;margin-right: 15px;width: 90%;">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>NIM</th>
                          <th>Nama Mahasiswa</th>
                          <th>Gelombang</th>
                          <th>Kelas</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $no=1 ?>
                        @foreach($kelompok["getdetail"] as $dt)
                        <tr>
                          <td>{{$no++}}</td>
                          <td>{{$dt['nim']}}</td>
                          <td>{{$dt["getdu"]["getmhsdaft"]['NAMA']}}</td>
                          <td>{{$dt["getdu"]['GEL']}}</td>
                          <td>{{$dt["getdu"]['KELAS']}}</td>
                        </tr>
                        @endforeach
                            
                      </tbody>
                    </table>
                  </div>

            </div>
        </div>
    </div>
</div>
  


@endsection

@section('css')

@endsection

@section('script')
<script>
$(document).ready(function(){

  if($('#sts_pemberangkatan').data("value") == 1){
      Swal.fire({
          title: 'Konfirmasi',
          text: 'Query Berhasil Dijalankan',
          type: 'success',
          confirmButtonText: 'OK'
      });
  }

  $("#btnBimbingan").click(function(e){
    e.preventDefault();
    var url = $(this).attr("href");
    Swal.fire({
        title:"Mencetak form bimbingan artinya kelompok ini sudah menyelesaikan OJT / dalam proses TA. <br> Ingin Melanjutkan ?",
        text:"",
        icon:"warning",
        showCancelButton:true,
        confirmButtonColor:"#3085d6",
        cancelButtonColor:"#d33",
        cancelButtonText:"TIDAK",
        confirmButtonText:"YA"
      }).then((result)=>{
        if(result.value){
          window.open(url,"_blank");      
        }
      });
  }); 

  $("#btnKompre").click(function(e){
    e.preventDefault();
    var url = $(this).attr("href");
    Swal.fire({
        title:"Terima kasih atas bimbingan yang telah diberikan. <br><br> Kelompok ini akan dimasukkan ke daftar kelompok siap Ujian Komprehensif. <br><br> Ingin Melanjutkan ?",
        text:"",
        icon:"warning",
        showCancelButton:true,
        confirmButtonColor:"#3085d6",
        cancelButtonColor:"#d33",
        cancelButtonText:"TIDAK",
        confirmButtonText:"YA"
      }).then((result)=>{
        if(result.value){
          window.open(url,"_blank");
          window.close()      
        }
      });
  }); 


});
</script>
@endsection