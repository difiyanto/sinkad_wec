@extends('ecc_page.layout2')
@section('jadwal','active')
@section('header')
<h1>
Setting Jadwal ECC
</h1>
<ol class="breadcrumb">
    <li><a href="{{url('/ecc')}}"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li class="active">Setting jadwal</li>
</ol>
@endsection
@section('body')
<input type="hidden" id="sts_jadwal" data-value="{{Session::get("sts_jadwal")}}" />

    

<div class="row">
    <div class="col-lg-8">
        <div class="box box-success direct-chat direct-chat-warning">
            <div class="box-body" style="padding:10px">
            
                    <form class="form-horizontal" id="frmJadwal" action="{{url('/ecc/tambahJadwal')}}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="method" value="POST">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="txttanggal" class="col-sm-2 control-label">Tanggal</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="txttanggal" name="txttanggal" placeholder="Tanggal (yyyy-mm-dd ) " value="{{old("txttanggal")}}">
                                    @if ($errors->has("txttanggal"))
                                        <small class="text-danger">{{ $errors->first("txttanggal") }}</small>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="txtjam" class="col-sm-2 control-label">Jam</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="txtjam" name="txtjam" placeholder="Jam ( hh:mm )" value="{{old("txtjam")}}">
                                    @if ($errors->has("txtjam"))
                                        <small class="text-danger">{{ $errors->first("txtjam") }}</small>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="txtlokasi" class="col-sm-2 control-label">Tempat</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="txtlokasi" name="txtlokasi" placeholder="Tempat" value="{{old("txtlokasi")}}">
                                    @if ($errors->has("txtlokasi"))
                                        <small class="text-danger">{{ $errors->first("txtlokasi") }}</small>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button class="btn btn-default" id="btnBatal">Batal</button>
                            <button type="submit" class="btn btn-primary pull-right">SIMPAN</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="box box-success direct-chat direct-chat-warning">
            <div class="box-body" style="padding:10px">
            <h4>Daftar Jadwal Yang Pernah Dibuat</h4>
            <table id="tbjadwal" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th width="5%">#</th>
                        <th width="20%">Tanggal</th>
                        <th width="20%">Jam</th>
                        <th width="20%">Tempat</th>
                        <th width="5%">Status</th>
                        <th width="25%">AKSI</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $no = 1;
                    @endphp
                    @foreach ($eccJadwal as $row)
                        <tr>
                            <td>{{$no}}</td>
                            <td>{{$row['tanggal']}}</td>
                            <td>{{$row['jam']}}</td>
                            <td>{{$row['tempat']}}</td>
                            <td>
                                @if ($row['status'] == 1)
                                    <div class="text-primary"><span class="fa fa-check-square"></span></div>
                                @else
                                <div class="text-danger"><span class="fa fa-close"></span></div>
                                @endif
                            </td>
                            <td>
                                @if ($row['status'] == 1)
                                    <a href="{{ url("ecc/status/nonaktif/".$row["id"]) }}">
                                        <button class="btn btn-warning ">Nonaktifkan Jadwal</button>
                                    </a>
                                @else
                                    <a href="{{ url("ecc/status/aktif/".$row["id"]) }}">
                                        <button class="btn btn-primary ">Aktifkan Jadwal</button>
                                    </a>
                                @endif
                                <a href="{{ url("ecc/hapusJadwal/".$row["id"]) }}">
                                    <button class="btn btn-danger"><span class="fa fa-trash"></span></button>
                                </a>
                                <a href="{{ url("ecc/ubah-jadwal/".$row["id"]) }}">
                                    <button class="btn btn-primary"><span class="fa fa-gear"></span></button>
                                </a>
                            </td>
                        </tr>
                        @php
                            $no++;
                        @endphp
                    @endforeach
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('css')
<link rel="stylesheet" href="{{asset('lte2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('script')
<script src="{{asset('lte2/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.mask.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.mask.test.js')}}"></script>
<script>
$(document).ready(function(){

$('#txttanggal').mask('0000-00-00');
$('#txtjam').mask('00:00');

if($('#sts_jadwal').data("value") == 1){
    Swal.fire({
        title: 'Konfirmasi',
        text: 'Query Berhasil Dijalankan',
        type: 'success',
        confirmButtonText: 'OK'
    });
}


$('#tbjadwal').DataTable();

$('#btnBatal').click(function(e){
  e.preventDefault();
  document.getElementById('frmJadwal').reset();  
});

})
</script>

@endsection