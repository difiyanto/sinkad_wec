@extends('fo_page.layout2')
@section('ojt','active')
@section('penyerahan','active')
@section('header')
<h1>
Penyerahan Laporan
</h1>
<ol class="breadcrumb">
    <li><a href="{{url('fo')}}"><i class="fa fa-home"></i> Beranda</a></li>
    <li class="active">Penyerahan laporan</li>
</ol>
@endsection
@section('body')
<input type="hidden" name="sts_penyerahan" id="sts_penyerahan" data-value="{{Session::get('sts')}}">
<div class="row">
    <div class="col-lg-12">
        <div class="box box-primary direct-chat direct-chat-warning">
            <div class="box-body" style="padding: 10px;overflow: scroll;">
            <h4>Cari Kelompok</h4>
            <table id="datakelompok" class="table table-bordered table-hover" style="width: 120%">
                <thead>
                <tr>			
                  <th>aksi</th>
                  <th style="width: 10%" class="cari">No Kelompok</th>
                  <th style="width: 35%" class="cari">Nama Perusahaan</th>
                  <th style="width: 10%" class="cari">NIM</th>
                  <th style="width: 35%" class="cari">Nama Mhs</th>
                  <th style="width: 10%" class="cari">Kelas</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($kelompok as $kel)
                @foreach ($kel->getdu as $du)
                <tr id="x{{$du->NIM}}">
                    <td> <button href="" class="btn btn-primary" onclick="return update('{{$kel->kode_kelompok}}')">apply</button> </td>
                    <td>{{$kel->kode_kelompok}}</td>
                    <td>{{$kel["getperusahaan"]["nama_perusahaan"]}}</td>
                    <td>{{$du->NIM}}</td>
                    <td>{{$du->getmhsdaft->NAMA}}</td>
                    <td>{{$du->KELAS}}</td>
                </tr>
                @endforeach
                @endforeach
                </tbody>
            </table>
            </div>
        </div>
    </div>

    <div class="col-lg-12">
        <div class="box box-danger direct-chat direct-chat-warning">
            <div class="box-body" style="padding: 10px;overflow: scroll;">
            <h4>Data Kelompok Yang Sudah Mengumpulkan Laporan</h4>
            <table id="datakelompok2" class="table table-bordered table-hover" style="width: 120%">
                <thead>
                <tr>			
                  <th>aksi</th>
                  <th style="width: 10%" class="cari">No Kelompok</th>
                  <th style="width: 35%" class="cari">Nama Perusahaan</th>
                  <th style="width: 10%" class="cari">NIM</th>
                  <th style="width: 35%" class="cari">Nama Mhs</th>
                  <th style="width: 10%" class="cari">Kelas</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($kelompok2 as $kel2)
                @foreach ($kel2->getdu as $du)
                <tr class="y{{$kel2->kode_kelompok}}">
                    <td> <button href="" class="btn btn-danger" onclick="return update2('{{$kel2->kode_kelompok}}')">rollback</button> </td>
                    <td>{{$kel2->kode_kelompok}}</td>
                    <td>{{$kel2["getperusahaan"]["nama_perusahaan"]}}</td>
                    <td>{{$du->NIM}}</td>
                    <td>{{$du->getmhsdaft->NAMA}}</td>
                    <td>{{$du->KELAS}}</td>
                </tr>
                @endforeach
                @endforeach
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('css')
<link rel="stylesheet" href="{{asset('lte2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection


@section('script')
<script src="{{asset('lte2/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

<script>
$(document).ready(function(){
    
    $('#datakelompok thead .cari').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" data-toggle="tooltip" title="'+title+'" style="width:100%" class="form-control txtCari" placeholder="'+title+'" />' );
    } );
    var datakelompok = $('#datakelompok').DataTable();
    datakelompok.columns().every( function () {
        var that = this;
        $( 'input', this.header() ).on( 'keyup change clear', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );

    $('#datakelompok2 thead .cari').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" data-toggle="tooltip" title="'+title+'" style="width:100%" class="form-control txtCari" placeholder="'+title+'" />' );
    } );
    var datakelompok2 = $('#datakelompok2').DataTable();
    datakelompok2.columns().every( function () {
        var that = this;
        $( 'input', this.header() ).on( 'keyup change clear', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );


});

function update(no_kelompok){
    Swal.fire({
        title:"Apakah anda yakin ingin menerima laporan dari kelompok ini ?",
        text:"",
        icon:"warning",
        showCancelButton:true,
        confirmButtonColor:"#3085d6",
        cancelButtonColor:"#d33",
        cancelButtonText:"TIDAK",
        confirmButtonText:"YA"
    }).then((result)=>{
        if(result.value){
            $.ajax({
                url:"{{url('/fo/penyerahan+laporan+')}}"+no_kelompok,
                type:"GET",
                success:function(r){
                    var res = JSON.parse(r);
                    Swal.fire({
                        title: 'Konfirmasi',
                        text: 'Query Berhasil Dijalankan',
                        type: 'success',
                        confirmButtonText: 'OK'
                    });
                    $("#datakelompok2").append(res['rows'])
                    var anggota = res["anggota"];
                    for(var i = 0; i < anggota.length; i++){
                        $("#x"+anggota[i]).hide()
                    }
                    // var rowClass = ".x-"+no_kelompok
                    
                },
                error:function(e){
                    console.log(e.responseText)
                }
            })      
        }
    });
}

function update2(no_kelompok){
    Swal.fire({
        title:"Apakah anda yakin ingin menerima laporan dari kelompok ini ?",
        text:"",
        icon:"warning",
        showCancelButton:true,
        confirmButtonColor:"#3085d6",
        cancelButtonColor:"#d33",
        cancelButtonText:"TIDAK",
        confirmButtonText:"YA"
    }).then((result)=>{
        if(result.value){
            $.ajax({
                url:"{{url('/fo/penyerahan+laporan2+')}}"+no_kelompok,
                type:"GET",
                success:function(r){
                    var res = JSON.parse(r);
                    Swal.fire({
                        title: 'Konfirmasi',
                        text: 'Query Berhasil Dijalankan',
                        type: 'success',
                        confirmButtonText: 'OK'
                    });
                    window.location.reload(1)
                    // $("#datakelompok").append(res['rows'])
                    // var anggota = res["anggota"];
                    // console.log(anggota)
                    // for(var i = 0; i < anggota.length; i++){
                    //     $("#y"+anggota[i]).hide()
                    // }
                },
                error:function(e){
                    console.log(e.responseText)
                }
            })      
        }
    });
}
</script>
@endsection