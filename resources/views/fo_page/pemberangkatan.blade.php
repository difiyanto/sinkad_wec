@extends('fo_page.layout2')
@section('ojt','active')
@section('pemberangkatan','active')
@section('header')
<h1>
Data Kelompok
</h1>
<ol class="breadcrumb">
    <li><a href="{{url('fo')}}"><i class="fa fa-home"></i> Beranda</a></li>
    <li class="active">Data Kelompok</li>
</ol>
@endsection
@section('body')
<div class="row">
<div class="col-lg-12">

<div class="box box-danger direct-chat direct-chat-warning">
    <div class="box-body" style="padding: 10px;">
    <h4>Data Kelompok</h4>
    <table id="datapemberangkatan" class="table table-bordered table-hover">
        <thead>
        <tr>
          <th>#</th>			
          <th class="cari">No Kelompok</th>
          <th class="cari">Nama Perusahaan</th>
          <th class="cari">Tanggal Pengajuan</th>
          <th>aksi</th>
        </tr>
        </thead>
        <tbody>
                <?php $no = 1?>
      @foreach($kelompok as $tp)
          <tr>
              <td>{{$no}}</td>
              <td>{{$tp['no_kelompok']}}</td>
              <td>{{$tp['nama_perusahaan']}}</td>
              <td>{{$tp['created_at']}}</td>
              <td>
                  <a href="{{url('/fo/detailpemberangkatan/'.$tp['no_kelompok'])}}"><button class="btn btn-primary">Detail</button>
              </td>
          </tr>
          @php
              $no++;
          @endphp
           @endforeach
        </tbody>
      </table>
    </div>
</div>

</div>
</div>



@endsection

@section('css')
<link rel="stylesheet" href="{{asset('lte2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection


@section('script')
<script src="{{asset('lte2/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

<script>
$(document).ready(function(){

    $('#datapemberangkatan thead .cari').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" data-toggle="tooltip" title="'+title+'" style="width:100%" class="form-control txtCari" placeholder="'+title+'" />' );
    } );
    var datapemberangkatan = $('#datapemberangkatan').DataTable();
    datapemberangkatan.columns().every( function () {
        var that = this;
        $( 'input', this.header() ).on( 'keyup change clear', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );


});
</script>
@endsection