@extends('fo_page.layout2')
@section('pembayaran','active')
@section('header')
<h1>
Data Pembayaran
</h1>
<ol class="breadcrumb">
    <li><a href="{{url('fo')}}"><i class="fa fa-home"></i> Beranda</a></li>
    <li class="active">Pembayaran</li>
</ol>
@endsection
@section('body')
<div class="row">
   <div class="col-lg-8">
      <div class="box box-primary direct-chat direct-chat-warning">
         <div class="box-body" style="padding:10px;">
            <div class="container">
               <div class="row">
                  <div class="col-sm-2">
                     Pilih Kategori
                  </div>
                  <div class="col-sm-5">
                     <div class="radio">
                        <label>
                          <input type="radio" name="kategori" id="nim" value="nim">
                          Berdasarkan NIM
                        </label>
                     </div>
                     <div class="radio">
                        <label>
                          <input type="radio" name="kategori" id="kelas" value="kelas">
                          Berdasarkan Kelas
                        </label>
                     </div>
                     <div class="radio">
                        <label>
                          <input type="radio" name="kategori" id="kdjurusan" value="kelas">
                          Berdasarkan Jurusan
                        </label>
                     </div>
                  </div>
               </div>
               <div class="row" id="carinim">
                  <div class="col-sm-2">
                     Pilih NIM
                  </div>
                  <div class="col-sm-5">
                     <select name="srcnim" id="srcnim" class="form-control" style="width:100%">
                        <option value="">[ Pilih NIM ]</option>
                        @foreach ($du as $d)
                        <option value="{{$d->NIM}}">{{$d->NIM}} | {{$d->getmhsdaft->NAMA}}</option>
                        @endforeach
                     </select>
                     <br><br>
                     <button class="btn btn-primary btn-block" id="btnCariNim"> <i class="fa fa-search"></i> C A R I</button>
                  </div>
               </div>
               <div class="row" id="carikelas">
                  <div class="col-sm-2">
                     Pilih Kelas
                  </div>
                  <div class="col-sm-5">
                     <select name="srckelas" id="srckelas" class="form-control">
                        <option value="">[ Pilih Kelas ]</option>
                        @foreach ($kelas as $k)
                        <option value="{{$k['KELAS']}}">{{$k['KELAS']}}</option>
                        @endforeach
                     </select>
                     <br><br>
                     <button class="btn btn-primary btn-block" id="btnCariKelas">C A R I</button>
                  </div>
               </div>
               <div class="row" id="carijurusan">
                  <div class="col-sm-2">
                     Pilih Jurusan
                  </div>
                  <div class="col-sm-5">
                     <select name="srckdjurusan" id="srckdjurusan" class="form-control" style="width:100%">
                        <option value="">[ Pilih Jurusan ]</option>
                        @foreach ($jurusan as $j)
                        <option value="{{$j->KD_JURUSAN}}">{{$j->jurusan}}</option>
                        @endforeach
                     </select>
                     <br><br>
                     <button class="btn btn-primary btn-block" id="btnCariKdjurusan">C A R I</button>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="row">
   <div class="col-lg-12">
      <div class="box box-primary direct-chat direct-chat-warning">
         <div class="box-body" style="padding:10px;overflow:scroll">
            <div id="resultcari">
               
            </div>
         </div>
      </div>
   </div>
</div>
@endsection

@section('css')
<link rel="stylesheet" href="{{asset('lte2/bower_components/select2/dist/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('lte2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('script')
<script src="{{asset('lte2/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/select2/dist/js/select2.full.min.js')}}"></script>

<script>
$(document).ready(function(){
   $("#carinim").hide()
   $("#carikelas").hide()
   $("#carijurusan").hide()


   $("#nim").click(function(){
      $("#carinim").show()
      $("#carikelas").hide()
      $("#carijurusan").hide()
   })

   $("#kelas").click(function(){
      $("#carinim").hide()
      $("#carikelas").show()
      $("#carijurusan").hide()
   })

   $("#kdjurusan").click(function(){
      $("#carinim").hide()
      $("#carikelas").hide()
      $("#carijurusan").show()
   })

   $("#srcnim").select2()
   $("#srckelas").select2()
   $("#srckdjurusan").select2()

   $("#btnCariNim").click(function(){
      var nim = $("#srcnim").val()
      $.ajax({
         url:"{{url('/fo/pembayaran/cari/nim/')}}/"+nim,
         type:"GET",
         beforeSend:function(){
            $("#resultcari").html("");
            $("#resultcari").html('<div class="alert alert-info font-weight-bold">SEDANG MENGAMBIL DATA . . .</div>')
         },
         success:function(r){
            var result = JSON.parse(r);
            $("#resultcari").html("");
            $("#resultcari").html(result["data"]);
         },
         error:function(e){
            console.log(e.responseText)
         }
      })
   });

   $("#btnCariKelas").click(function(){
      var kelas = $("#srckelas").val();
      $.ajax({
         url:"{{url('/fo/pembayaran/cari/kelas/')}}/"+kelas,
         type:"GET",
         beforeSend:function(){
            $("#resultcari").html("");
            $("#resultcari").html('<div class="alert alert-info font-weight-bold">SEDANG MENGAMBIL DATA . . .</div>')
         },
         success:function(r){
            var result = JSON.parse(r);
            $("#resultcari").html("");
            $("#resultcari").html(result["data"]);
         },
         error:function(e){
            console.log(e.responseText)
         }
      })
   });

   $("#btnCariKdjurusan").click(function(){
      var kdjurusan = $("#srckdjurusan").val();
      $.ajax({
         url:"{{url('/fo/pembayaran/cari/jurusan/')}}/"+kdjurusan,
         type:"GET",
         beforeSend:function(){
            $("#resultcari").html("");
            $("#resultcari").html('<div class="alert alert-info font-weight-bold">SEDANG MENGAMBIL DATA . . .</div>')
         },
         success:function(r){
            var result = JSON.parse(r);
            $("#resultcari").html("");
            $("#resultcari").html(result["data"]);
            console.log(result["nim"])
         },
         error:function(e){
            console.log(e.responseText)
         }
      })
   });
})
</script>
@endsection