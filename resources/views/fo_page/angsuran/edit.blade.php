@extends('fo_page.layout2')
@section('angsuran','active')
@section('header')
<h1>EDIT DATA</h1>
<br>
@endsection

@section('body')
<div class="row">
        <div class="col-lg-12">
            <div class="box box-primary direct-chat direct-chat-warning">
                <div class="box-body" style="padding:10px">
                    <form action="{{url('/angsuran/edit.html')}}" id="frmangsadd" method="post" enctype="multipart/form-data">
                 {{ csrf_field() }}
                 <div class="form-group row">
                    <label class="col-sm-3 form-control-label">JURUSAN</label>
                    <div class="col-sm-9">
                       <select name="jurusan" id="jurusan" class="form-control form-control-warning">                 
                          <option value="{{$jur->KD_JURUSAN}}" >{{$jur->jurusan}}</option>
                       </select>
                       @if ($errors->has("jurusan"))
                          <small class="text-danger">{{ $errors->first("jurusan") }}</small>
                       @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 form-control-label">GELOMBANG</label>
                    <div class="col-sm-9">
                       <select name="gelombang" id="gelombang" class="form-control form-control-warning">
                       <option value="{{$EDIT->GEL}}" >{{$EDIT->GEL}}</option>

                       </select>
                       @if ($errors->has("gelombang"))
                          <small class="text-danger">{{ $errors->first("gelombang") }}</small>
                       @endif
                    </div>
                </div>
               <div class="form-group row">
                  <label for="an" class="col-sm-3 col-form-label">NOMOR ANGSURAN</label>
                  <div class="col-sm-9">
                     <input type="text" class="form-control" id="no_angsuran" name="no_angsuran" placeholder="Nomor Angsuran" value="{{$EDIT->NO_ANS}}" onkeypress="return isNumber(event)">
                     @if ($errors->has("no_angsuran"))
                     <span class="text-danger">{{$errors->first("no_angsuran")}}</span>
                     @endif
                  </div>
               </div>
               <div class="form-group row">
                  <label for="saldo" class="col-sm-3 col-form-label">ANGSURAN NORMAL</label>
                  <div class="col-sm-9">
                     <input type="text" class="form-control" id="angsuran_normal" name="angsuran_normal" placeholder="ANGSURAN NORMAL" value="{{$EDIT->ANGSURAN}}" onkeypress="return isNumber(event)">
                     @if ($errors->has("angsuran_normal"))
                     <span class="text-danger">{{$errors->first("angsuran_normal")}}</span>
                     @endif
                  </div>
               </div>
               <div class="form-group row">
                <label for="saldo" class="col-sm-3 col-form-label">ANGSURAN BEASISWA</label>
                <div class="col-sm-9">
                   <input type="text" class="form-control" id="angsuran_beasiswa" name="angsuran_beasiswa" placeholder="ANGSURAN BEASISWA" value="{{$EDIT->ANGS_BEASISWA}}" onkeypress="return isNumber(event)">
                   @if ($errors->has("angsuran_beasiswa"))
                   <span class="text-danger">{{$errors->first("angsuran_beasiswa")}}</span>
                   @endif
                </div>
             </div>
               <div class="form-group row">
                  <label for="button" class="col-sm-3 col-form-label"></label>
                  <div class="col-sm-9">
                     <button class="btn btn-primary" type="submit">S I M P A N</button>
                     <a href="{{url("/angsuran")}}" class="btn btn-danger" >B A T A L</a>
                  </div>
               </div>
               </form>
                       
                </div>
            </div>
        </div>
        </div>
@endsection

