@extends('ojt_page.layout2')
@section('pemberangkatan','active')
@section('proses','active')
@section('header')
<h1>Detail Kelompok OJT</h1>
<ol class="breadcrumb">
    <li><a href="{{url('/ojt')}}"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li><a href="{{url('/ojt/pemberangkatan.html')}}">Pemberangkatan</a></li>
    <li class="active">Detail Pemberangkatan</li>
</ol>
@endsection
@section('body')
	
	<div class="row">
    <div class="col-lg-8">
        <div class="box box-primary direct-chat direct-chat-warning">
            <div class="box-body" style="padding:10px">
                <form class="form-horizontal" action="{{url('ojt/updatedetailpemberangkatan/'.$kelompok[0]['no_kelompok'])}}">
                   {{ csrf_field() }}
          
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label"><b>No Kelompok</b></label>
                    <div class="col-sm-9">
                      <input id="no_kelompokxx" type="email"  class="form-control form-control-success" value="{{$kelompok[0]['no_kelompok']}}" readonly="true" name="no_kelompok">
                    </div>
                  </div>
                  
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label"><b>Nama Perusahaan</b></label>
                    <div class="col-sm-9">
                      <input id="inputHorizontalSuccess" type="email"  class="form-control form-control-success" value="{{$kelompok[0]['nama_perusahaan']}}" readonly="true" name="nama_perusahaan">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label"><b>Tanggal Pengajuan</b></label>
                    <div class="col-sm-9">
                      <input id="inputHorizontalWarning" type="text"  class="form-control form-control-warning" value="{{$kelompok[0]['created_at']}}" readonly="true">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Bulan</label>
                    <div class="col-sm-9">
                      <select name="txtbulan" id="txtbulan" class="form-control form-control-warning">
                        @php
                            $no = 1;
                        @endphp
                        @foreach ($bulan as $bulan)
                          <option value="{{ $no }}" {{ $kelompok[0]["bulan"] == $no ? "selected" : "" }} >{{ $bulan }}</option>  
                          @php $no++; @endphp
                        @endforeach
                      </select>
                      @if ($errors->has("txtbulan"))
                      <small class="text-danger">{{ $errors->first("txtbulan") }}</small>
                    @endif
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label"><b>Lama OJT</b></label>
                    <div class="col-sm-9">
                      <input id="inputHorizontalWarning" min="0" type="number" name="lamaojt"  class="form-control form-control-warning" value="{{ $kelompok[0]['lama'] }}" style="width:10%;float:left" {{ $kelompok[0]['apr_pemberangkatan1'] == 1 ? "readonly" : ""  }}> &nbsp;&nbsp;Bulan
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label"><b>Status</b></label>
                    <div class="col-sm-9">
                      <input id="inputHorizontalWarning" type="text"  class="form-control form-control-warning" value="{{ $kelompok[0]['sts_kelompok'] }}" readonly="true">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label"><b>Nama Pembimbing 1</b></label>
                    <div class="col-sm-9">
                      <select id="txtpembimbing1" class="form-control select2" style="width: 100%;" name="pembimbing1"   }}>
                      <option value="">[ PILIH PEMBIMBING 1 ]</option>
                      @foreach($tpdos as $tp)
                      <option value="{{$tp['NIP']}}" {{ $kelompok[0]['nip'] == $tp['NIP'] ? "selected" : ""  }}>{{$tp['nama']}}</option>
                      @endforeach
                    </select>
                    <div id="jumlah1"></div>
                    @if ($errors->has("pembimbing1"))
                        <small class="text-danger">pembimbing belum dipilih.</small>
                    @endif
                  </div> 
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label"><b>Nama Pembimbing 2</b></label>
                    <div class="col-sm-9">
                      <select id="txtpembimbing2" class="form-control select2" style="width: 100%;" name="pembimbing2" >
                      <option value="">[ PILIH PEMBIMBING 2 ]</option>
                      @foreach($tpasdos as $tp3)
                      <option value="{{$tp3['NIP']}}" {{ $kelompok[0]['nip2'] == $tp3['NIP'] ? "selected" : ""  }}>{{$tp3['nama']}}</option>
                      @endforeach
                    </select>
                    <div id="jumlah2"></div>
                  </div> 
                  </div>
                  {{-- @if ($kelompok[0]["sts_kelompok"] <> "semua persyaratan telah selesai di ACC") --}}
                  <button class="btn btn-primary btn-sm" type="submit">Simpan Perubahan</button>
                  <a class="btn btn-danger btn-sm" href="{{ url('ojt/konfirmasi-terima/'.$kelompok[0]['no_kelompok']) }}">Konfirmasi Diterima Saja</a>
                  {{-- @endif --}}
            </form>
            </div>
        </div>

    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="box box-primary direct-chat direct-chat-warning">
            <div class="box-body" style="padding:10px">

                <h4>Anggota Kelompok</h4>

                <button class="btn btn-primary btn-sm" id="btnTambah">Tambah Anggota</button>
                 <a href="{{ url('ojt/download+proposal+'.$kelompok[0]['no_kelompok']) }}" class='btn btn-success btn-sm' id='btncetak' target='_blank'>Download Proposal Baru Untuk Kelompok Ini</a> 

                <div class="table-responsive" style="width: 90%;">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>NIM</th>
                          <th>Nama Mahasiswa</th>
                          <th>Gelombang</th>
                          <th>Kelas</th>
                          <th>Ketidakhadiran (%)</th>
                          <th>aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $no=1 ?>
                        @foreach($kelompok as $dt)
                        <tr>
                          <td>{{$no++}}</td>
                          <td>{{$dt['nim']}}</td>
                          <td>{{$dt['NAMA']}}</td>
                          <td>{{$dt['GEL']}}</td>
                          <td>{{$dt['KELAS']}}</td>
                          <td>{{$dt['absensi']}} %</td>
                          <td>
                            <button class="btn" onclick="lihatnilai({{$dt['nim']}})">Lihat Nilai</button>
                            <a class="btn btn-danger btn-sm" href="{{ url('ojt/hapusanggota/'.$dt['nim'].'/'.$kelompok[0]['no_kelompok']) }}">Hapus</a>
                          </td>
                        </tr>

                        @endforeach
                            
                      </tbody>
                    </table>
                  </div>

            </div>
        </div>
    </div>
</div>
	


<div class="modal fade" id="modal_tambahanggota" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
      <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah Anggota Kelompok</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          </button>
          
      </div>
      <div class="modal-body">
        
          <form class="form-horizontal" method="POST" action="{{url('/ojt/tambahAnggotaPost')}}">

            {{ csrf_field() }}
            <input type="hidden" name="_method" value="POST">
            <input type="hidden" name="no_kelompok" value="{{ $kelompok[0]['no_kelompok'] }}">
              <div class="form-group row">
                <label class="col-sm-4 form-control-label">NIM</label>
                <div class="col-sm-8">
                    <input id="txtnimadd" type="text" placeholder="NIM" class="form-control form-control-success" name="txtnimadd">
                    <div id="load_cek" style="margin-top:8px"></div>
                    <button class="btn btn-secondary btn-sm" id="btnCekNIM" style="margin-top:8px;">CEK</button>
                </div>
              </div>
              <div id="data_mhs">
                <div class="form-group row">
                  <label class="col-sm-4 form-control-label">NAMA</label>
                  <div class="col-sm-8">
                      <input id="txtnamaadd" type="text" placeholder="Nama" class="form-control form-control-success" readonly>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-4 form-control-label">GELOMBANG</label>
                  <div class="col-sm-8">
                      <input id="txtgeladd" type="text" placeholder="Gelombang" class="form-control form-control-success" readonly>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-4 form-control-label">KELAS</label>
                  <div class="col-sm-8">
                      <input id="txtkelasadd" type="text" placeholder="Kelas" class="form-control form-control-success"  readonly>
                  </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-4 form-control-label"></label>
                    <div class="col-sm-8">
                        <div id="feedbackAdd"></div>
                    </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-4 form-control-label"></label>
                  <div class="col-sm-8">
                    <small class="text-primary">
                      * PASTIKAN DATA YANG AKAN DIINPUT SUDAH BENAR <br>
                    <div id="izinkanx">
                      <input type="checkbox" name="izinkan" id="izinkan"> Tetap Masukkan
                    </div>
                    </small><br>
                    <button type="submit" class="btn btn-primary btn-sm" id="btnSimpanAdd" >TAMBAHKAN</button>
                    <a href="{{url('/ojt/detailpemberangkatan/'.$kelompok[0]['no_kelompok'])}}" class="btn btn-danger btn-sm">BATAL</a>
                  </div>
                </div>
              </div>
          </form>

      </div>
      <div class="modal-footer">
          
  </div>
  </div>
  
  

</div>
</div>

{{-- modal lihatnilai --}}
<div class="modal fade" id="Modallihatnilai">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Daftar Nilai`</h4>
        </div>
        <div class="modal-body">
            <div id="tb_lihatnilaiCover">

            </div>
            

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
@endsection

@section('css')
 <link rel="stylesheet" href="{{asset('lte2/bower_components/select2/dist/css/select2.min.css')}}">
 <link rel="stylesheet" href="{{asset('lte2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('script')
<script src="{{asset('lte2/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){
  var base_url = "{{ url('/') }}";

  $('.select2').select2()
  $("#data_mhs").hide();

  

  var sts_pemberangkatan = "{{ session('sts_pemberangkatan') }}";
  if(sts_pemberangkatan == 1){
    Swal.fire({
          title: 'Konfirmasi',
          text: 'Query Berhasil Dijalankan',
          type: 'success',
          confirmButtonText: 'OK'
      });
  }
  
  $("#btnCekNIM").click(function(e){
    e.preventDefault();
    ceknim($("#txtnimadd").val());
  });

  $("#txtnimadd").keyup(function(e){
    if(e.keyCode == 13){
      ceknim($("#txtnimadd").val());
    }
  });

  $("#izinkan").change(function(){
    //   alert($(this).val());
      if($("#izinkan").is(":checked")){
          $('#btnSimpanAdd').show();
      }else{
        $('#btnSimpanAdd').hide();
      }
  });

  $('#btnTambah').click(function(e){
    e.preventDefault();
    $('#modal_tambahanggota').modal();
  });

  $('#txtpembimbing1').change(function(){
    // alert($(this).val());
  $.ajax({
      url : base_url+"/ojt/cekPembimbing/"+$('#txtpembimbing1').val()+"/Dosen",
      type:'GET',
      success:function(r){
        // var result = JSON.parse(r);
        console.log(r);
        $('#jumlah1').html(r["pesan"]);
      },
      error:function(e){
        console.log(e.responseText);
      }
    });
  });

  $('#txtpembimbing2').change(function(){
  $.ajax({
      url : base_url+"/ojt/cekPembimbing/"+$('#txtpembimbing2').val()+"/Asdos",
      type:'GET',
      success:function(r){
        // var result = JSON.parse(r);
        console.log(r);
        $('#jumlah2').html(r["pesan"]);
      },
      error:function(e){
        console.log(e.responseText);
      }
    });
  });



  function ceknim(nim){
    $.ajax({
      url:base_url+"/ojt/tambahanggota/"+nim+"/{{$kelompok[0]['no_kelompok']}}",
      type:'GET',
      beforeSend:function(){
        $('#load_cek').html("<div class='text-primary'>Tunggu Sebentar</div>");
      },
      success:function(r){
        console.log(r);
        var result = JSON.parse(r);
        $("#feedbackAdd").hide();
        $('#btnSimpanAdd').hide();
        $("#data_mhs").hide();
        $('#izinkanx').hide();
        if(result["status"] == "2"){
            $('#load_cek').html("<div class='text-primary'>NIM Sudah Memiliki Kelompok / NIM Salah</div>");
        }else{
            $("#feedbackAdd").show();
            $('#load_cek').html("");
            var r = JSON.parse(r);
            $("#data_mhs").show();

            $("#txtnamaadd").val(r['datamhs'][0]['NAMA']);
            $("#txtgeladd").val(r['datamhs'][0]['GEL_DAFTAR']);
            $("#txtkelasadd").val(r['datamhs'][0]['KELAS']);
            view = "<ul>";
            if(r["gel"] == 0){
            view += "<li class='text-danger'>Tidak pada gelombang yang sama.</li>"; 
            }else{
            view += "<li>Berada pada gelombang yang sama.</li>";
            }
            
            if(r["jurusan"] == 0){
            view += "<li class='text-danger'>Tidak pada jurusan yang sama.</li>";
            }else{
            view += "<li>Berada pada jurusan yang sama.</li>";
            }

            if(r["absensi"] == 0){
            view += "<li class='text-danger'>Prosentase Ketidakhadiran >= 25%.</li>";
            }else{
            view += "<li>Prosentase Ketidakhadiran < 25%.</li>";
            }

            if(r["adminis"] == 0){
            view += "<li class='text-danger'>Belum lunas administrasi dibulan ini.</li>";
            }else{
            view += "<li>Sudah melunasi administrasi bulan ini.</li>";
            }

            if(r["status"] == 0){
            view += "<li class='text-danger'>status : Tidak bisa masuk dalam kelompok</li>";
            $('#btnSimpanAdd').hide();
            $('#izinkanx').show();
            }else{
            view += "<li>status : Memenuhi syarat. (klik tambahkan)</li>";
            $('#btnSimpanAdd').show();
            }
            view += "</ul>";
            $('#feedbackAdd').html("<div class='alert alert-default'>Hasil pengecekan kami mengatakan bahwa calon anggota kelompok ini : <br><br>"+view+"</div>");
        } 
      },
      error:function(e){
        console.log(e.responseText);
        alert("Terjadi Kesalahan !");
      }
    });
  }
});


function lihatnilai(nim){
    $.ajax({
        url:"{{url('')}}/ojt/lihatnilai/"+nim,
        type:"GET",
        success:function(r){
            var r = JSON.parse(r);
            $("#tb_lihatnilaiCover").empty()
            var table = "<table id='tb_nilai' class='table table-responsive table-striped' width='100%'>";
            
            table += "<thead><tr>";

            table += "<th>Mata Kuliah</th>";
            table += "<th>Nilai Akhir</th>";
            table += "</tr></thead>";
            table += "<tbody>";
                for(var i = 0; i < r["dnilai"].length; i++){
                    if(r["dnilai"][i]["nilai_akhir"] < 60){
                      table += "<tr class='text-danger font-weight-bold'><td>"+r["dnilai"][i]["nm_matkul"]+"</td><td>"+r["dnilai"][i]["nilai_akhir"]+"</td></tr>";
                    }else{
                    table += "<tr><td>"+r["dnilai"][i]["nm_matkul"]+"</td><td>"+r["dnilai"][i]["nilai_akhir"]+"</td></tr>";
                    }

                }
            table += "</tbody>";
            table += "</table>";
            $("#tb_lihatnilaiCover").html(table);
            $("#tb_nilai").DataTable();
            $("#Modallihatnilai").modal();
        },
        error:function(e){
            alert("Terjadi Kesalahan !");
            console.log(e.responseText);
        }
    })
}

</script>
@endsection