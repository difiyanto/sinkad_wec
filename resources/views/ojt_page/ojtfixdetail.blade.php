@extends('ojt_page.layout2')
@if ($kelompok[0]["sts_pencarian"] == 2)
@section('ta','active')
@section('rekap','active')
@else    
@section('kelompokfix','active')
@section('rekap','active')
@endif
@section('header')
<h1>Detail Kelompok {{$kelompok[0]["sts_pencarian"] == 2 ? "TA" : "OJT" }}</h1>
<ol class="breadcrumb">
    <li><a href="{{url('/ojt')}}"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li><a href="{{ $kelompok[0]["sts_pencarian"] == 2 ? url('/ojt/Data TA.html') : url('/ojt/kelompok fix.html') }}">Data Kelompok {{$kelompok[0]["sts_pencarian"] == 2 ? "TA" : "OJT" }}</a></li>
    <li class="active">Detail Kelompok</li>
</ol>
@endsection
@section('body')
	
	<div class="row">
    <div class="col-lg-8">
        <div class="box box-primary direct-chat direct-chat-warning">
            <div class="box-body" style="padding:10px">
                <form class="form-horizontal" action="{{url('ojt/updatedetailpemberangkatan/'.$kelompok[0]['no_kelompok'])}}">
                   {{ csrf_field() }}
          
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label"><b>No Kelompok</b></label>
                    <div class="col-sm-9">
                      <input id="inputHorizontalSuccess" type="email"  class="form-control form-control-success" value="{{$kelompok[0]['no_kelompok']}}" readonly="true" name="no_kelompok">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label"><b>Nama Perusahaan</b></label>
                    <div class="col-sm-9">
                      <input id="inputHorizontalSuccess" type="email"  class="form-control form-control-success" value="{{$kelompok[0]['nama_perusahaan']}}" readonly="true" name="nama_perusahaan">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label"><b>Tanggal Pengajuan</b></label>
                    <div class="col-sm-9">
                      <input id="inputHorizontalWarning" type="text"  class="form-control form-control-warning" value="{{$kelompok[0]['created_at']}}" readonly="true">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label"><b>{{$kelompok[0]["sts_pencarian"] == 2 ? "TA" : "OJT" }} di Bulan</b></label>
                    <div class="col-sm-9">
                      @php
                          $bulan = [ "1" => "Januari", "2"=> "Februari","3"=>"Maret","4"=>"April","5"=>"Mei","6"=>"Juni","7"=>"Juli","8"=>"Agustus","9"=>"September","10"=>"Oktober","11"=>"November","12"=>"Desember" ];
                      @endphp
                      <input id="inputHorizontalWarning" type="text"  class="form-control form-control-warning" value="{{ $bulan[$kelompok[0]['bulan']] }}" readonly="true">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label"><b>Lama {{$kelompok[0]["sts_pencarian"] == 2 ? "TA" : "OJT" }}</b></label>
                    <div class="col-sm-9">
                      <input id="inputHorizontalWarning" type="number" name="lamaojt"  class="form-control form-control-warning" value="{{ $kelompok[0]['lama'] }}" style="width:10%;float:left" {{ $kelompok[0]['apr_pemberangkatan1'] == 1 ? "readonly" : ""  }}> &nbsp;&nbsp;Bulan
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label"><b>Nama Pembimbing 1</b></label>
                    <div class="col-sm-9">
                      <select class="form-control select2" style="width: 100%;" name="pembimbing1" {{ $kelompok[0]['apr_pemberangkatan1'] == 1 ? "disabled" : ""  }}>
                      <option value="">[ PILIH PEMBIMBING 1 ]</option>
                      @foreach($tpdos as $tp)
                      <option value="{{$tp['NIP']}}" {{ $kelompok[0]['nip'] == $tp['NIP'] ? "selected" : ""  }}>{{$tp['nama']}}</option>
                      @endforeach
                    </select>
                    @if ($errors->has("pembimbing1"))
                        <small class="text-danger">pembimbing belum dipilih.</small>
                    @endif
                  </div> 
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label"><b>Nama Pembimbing 2</b></label>
                    <div class="col-sm-9">
                      <select class="form-control select2" style="width: 100%;" name="pembimbing2" {{ $kelompok[0]['apr_pemberangkatan1'] == 1 ? "disabled" : ""  }}>
                      <option value="">[ PILIH PEMBIMBING 2 ]</option>
                      @foreach($tpasdos as $tp3)
                      <option value="{{$tp3['NIP']}}" {{ $kelompok[0]['nip2'] == $tp3['NIP'] ? "selected" : ""  }}>{{$tp3['nama']}}</option>
                      @endforeach
                    </select>
                  </div> 
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label"><b></b></label>
                    <div class="col-sm-9">
                      
                   
                  </div> 
                  </div>
                 
           
            </form>
            </div>
        </div>

    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="box box-primary direct-chat direct-chat-warning">
            <div class="box-body" style="padding:10px">

                <h4>Anggota Kelompok</h4>
                <div class="table-responsive table-striped table-bordered" style="width: 90%;">
                    <table class="table table-bordered table-responsive table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>NIM</th>
                          <th>Nama Mahasiswa</th>
                          <th>Gelombang</th>
                          <th>Kelas</th>
                          <th>Ketidakhadiran (%)</th>
                          <th>aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $no=1 ?>
                        @foreach($kelompok as $dt)
                        <tr>
                          <td>{{$no++}}</td>
                          <td>{{$dt['nim']}}</td>
                          <td>{{$dt['NAMA']}}</td>
                          <td>{{$dt['GEL_DAFTAR']}}</td>
                          <td>{{$dt['KELAS']}}</td>
                          <td>{{$dt['absensi']}} %</td>
                          <td>
                            <button class="btn" onclick="lihatnilai({{$dt['nim']}})">Lihat Nilai</button>
                          </td>
                        </tr>

                        @endforeach
                            
                      </tbody>
                    </table>
                  </div>

            </div>
        </div>
    </div>
</div>
	


  
  {{-- modal lihatnilai --}}
<div class="modal fade" id="Modallihatnilai">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Daftar Nilai`</h4>
        </div>
        <div class="modal-body">
            <div id="tb_lihatnilaiCover">

            </div>
            

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>

</div>
</div>

@endsection

@section('css')
 <link rel="stylesheet" href="{{asset('lte2/bower_components/select2/dist/css/select2.min.css')}}">
 <link rel="stylesheet" href="{{asset('lte2/bower_components/select2/dist/css/select2.min.css')}}">
 <link rel="stylesheet" href="{{asset('lte2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('script')
<script src="{{asset('lte2/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){
  var base_url = "{{ url('/') }}";

  $('.select2').select2()
  $("#data_mhs").hide();

  var sts_pemberangkatan = "{{ session('sts_pemberangkatan') }}";
  if(sts_pemberangkatan == 1){
    Swal.fire({
          title: 'Konfirmasi',
          text: 'Query Berhasil Dijalankan',
          type: 'success',
          confirmButtonText: 'OK'
      });
  }
  
  $("#btnCekNIM").click(function(e){
    e.preventDefault();
    ceknim($("#txtnimadd").val());
  });

  $("#txtnimadd").keyup(function(e){
    if(e.keyCode == 13){
      ceknim($("#txtnimadd").val());
    }
  });


  function ceknim(nim){
    $.ajax({
      url:base_url+"/ojt/tambahanggota/"+nim,
      type:'GET',
      beforeSend:function(){
        $('#load_cek').html("<div class='text-primary'>Tunggu Sebentar</div>");
      },
      success:function(r){
        $('#load_cek').html("");
        var datamhs = JSON.parse(r);
        $("#data_mhs").show();
        $("#txtnamaadd").val(datamhs['cek'][0]['NAMA']);
        $("#txtgeladd").val(datamhs['cek'][0]['GEL']);
        $("#txtkelasadd").val(datamhs['cek'][0]['KELAS']);
      },
      error:function(e){
        console.log(e.responseText);
        alert("Terjadi Kesalahan !");
      }
    });
  }


});

function lihatnilai(nim){
    $.ajax({
        url:"{{url('')}}/ojt/lihatnilai/"+nim,
        type:"GET",
        success:function(r){
            var r = JSON.parse(r);
            $("#tb_lihatnilaiCover").empty()
            var table = "<table id='tb_nilai' class='table table-responsive table-striped' width='100%'>";
            table += "<thead><tr>";
            table += "<th>Mata Kuliah</th>";
            table += "<th>Nilai Akhir</th>";
            table += "</tr></thead>";
            table += "<tbody>";
                for(var i = 0; i < r["dnilai"].length; i++){
                    table += "<tr><td>"+r["dnilai"][i]["nm_matkul"]+"</td><td>"+r["dnilai"][i]["nilai_akhir"]+"</td></tr>";
                }
            table += "</tbody>";
            table += "</table>";
            $("#tb_lihatnilaiCover").html(table);
            $("#tb_nilai").DataTable();
            $("#Modallihatnilai").modal();
        },
        error:function(e){
            alert("Terjadi Kesalahan !");
            console.log(e.responseText);
        }
    })
}

</script>
@endsection