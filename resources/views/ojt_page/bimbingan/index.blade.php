@extends('ojt_page.layout2')
@section("bimbingan", "active")
@section("rekap", "active")
@section("body")

<div class="row">
  <div class="col-lg-12">

    <div  class="box box-primary direct-chat direct-chat-warning">
      <div class="box-body" style="padding:10px;height:500px;">
        <h4><strong>Data Bimbingan Dosen</strong></h4>
        <table class="table" id="tbdosen">
          <thead>
            <tr>
              <th class="cari">Nama Dosen</th>
              <th class="cari">No Kelompok</th>
              <th class="cari">Nama Perusahaan</th>
              <th class="cari">Jenis Pengajuan</th>
              <th class="cari">Jumlah mhs</th>
              <th class="cari">GEL</th>
              
            </tr>
          </thead>
          <tbody>
            @foreach ($getDosen as $a)
              @foreach ($a["getkelompok"] as $b)
              <tr>
                <td>{{$a->nama}}</td>
                <td>{{$b->no_kelompok}}</td>
                <td>{{$b["getperusahaan"]["nama_perusahaan"]}}</td>
                <td>
                  @if ($b['sts_pencarian'] == 0)
                    OJT (Tempat Magang Cari Sendiri)
                  @elseif($b['sts_pencarian'] == 1)
                    OJT (Tempat Magang Dicarikan Lembaga)
                  @else
                    TA
                  @endif
                </td>
                <td>{{count($b["getdetail"])}}</td>
                <td>{{substr($b->no_kelompok, 5, 1)}}</td>
              </tr>
              @endforeach
            @endforeach
          </tbody>
        </table>
      </div>
    </div>


  </div>
</div>


<div class="row">
  <div class="col-lg-12" >

    <div  class="box box-primary direct-chat direct-chat-warning">
      <div class="box-body" style="padding:10px;height:500px;">
        <h4><strong>Data Bimbingan Asisten Dosen</strong></h4>
        <table class="table table-striped" id="tbasdos">
          <thead>
            <tr>
              <th class="cari">Nama Asdos</th>
              <th class="cari">No Kelompok</th>
              <th class="cari">Nama Perusahaan</th>
              <th class="cari">Jenis Pengajuan</th>
              <th class="cari">Jumlah mhs</th>
              <th class="cari">GEL</th>
              <th class="cari">Partner Dosen</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($getAsdos as $a)
              @foreach ($a["getkelompok2"] as $b)
              <tr>
                <td>{{$a->nama}}</td>
                <td>{{$b->no_kelompok}}</td>
                <td>{{$b["getperusahaan"]["nama_perusahaan"]}}</td>
                <td>
                  @if ($b['sts_pencarian'] == 0)
                    OJT (Tempat Magang Cari Sendiri)
                  @elseif($b['sts_pencarian'] == 1)
                    OJT (Tempat Magang Dicarikan Lembaga)
                  @else
                    TA
                  @endif
                </td>
                <td>{{count($b["getdetail"])}}</td>
                <td>{{substr($b->no_kelompok, 5, 1)}}</td>
                <td>{{$b["getpembimbing1"]["nama"]}}</td>
              </tr>
              @endforeach
            @endforeach
          </tbody>
        </table>
      </div>
    </div>


  </div>
</div>


@endsection

@section('css')
<link rel="stylesheet" href="{{asset('lte2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('lte2/bower_components/datatables.net-bs/css/buttons.dataTables.min.css')}}">

@endsection

@section('script')
<script src="{{asset('lte2/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/jszip.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/buttons.print.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){
  //untuk table dosen
    var groupColumn = 0;
    $('#tbdosen thead .cari').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" data-toggle="tooltip" title="'+title+'" style="width:100%" class="form-control txtCari" placeholder="'+title+'" />' );
    } );
    var tbdosen = $('#tbdosen').DataTable({
      "columnDefs": [
            { "visible": false, "targets": groupColumn }
        ],
      "dom": 'Bfrtip',
      "buttons": [
            'csv', 'excel', 'pdf'
        ],
      "ordering":false,
      "paging":false,
      "order": [[ groupColumn, 'asc' ]],
      "displayLength": 25,
      "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
            var subTotal = new Array();
            var groupID = -1;
            var aData = new Array();
            var index = 0;

            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {

              var vals = api.row(api.row($(rows).eq(i)).index()).data();
              var jmlmhs = parseInt(vals[4]);

              if (typeof aData[group] == 'undefined') {
                 aData[group] = new Array();
                 aData[group].rows = [];
                 aData[group].jmlmhs = [];
              }

           		aData[group].rows.push(i);
        			aData[group].jmlmhs.push(jmlmhs);

              if ( last !== group ) {
                  $(rows).eq( i ).before(
                      '<tr class="group" style="background-color:#1890db;"><td colspan="5"><strong>'+group+'</strong></td></tr>'
                  );
                  last = group;
              }
            });

            var idx= 0;

          	for(var kelompok in aData){
									 idx =  Math.max.apply(Math,aData[kelompok].rows);

                   var sumjmlmhs = 0;
                   var jmlkelompok = 0;
                   $.each(aData[kelompok].jmlmhs,function(k,v){
                        sumjmlmhs = sumjmlmhs + v;
                        jmlkelompok = jmlkelompok + 1;
                   });
                    $(rows).eq( idx ).after(
                          '<tr class="group" style="background-color:#bfbcb2;"><td colspan="3"><strong>TOTAL</strong></td>'+
                          '<td colspan="2"><strong>'+sumjmlmhs+' Mahasiswa dari '+ jmlkelompok +' Kelompok</strong></td></tr><tr><td></td></tr>'
                    );
            };

        }
    });

    tbdosen.columns().every( function () {
        var that = this;
        $( 'input', this.header() ).on( 'keyup change clear', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );


    //untuk table asdos
      var groupColumn = 0;
      $('#tbasdos thead .cari').each( function () {
          var title = $(this).text();
          $(this).html( '<input type="text" data-toggle="tooltip" title="'+title+'" style="width:100%" class="form-control txtCari" placeholder="'+title+'" />' );
      } );
      var tbasdos = $('#tbasdos').DataTable({
        "columnDefs": [
              { "visible": false, "targets": groupColumn }
          ],
        "dom": 'Bfrtip',
        "buttons": [
              'csv', 'excel', 'pdf'
          ],
        "ordering":false,
        "paging":false,
        "order": [[ groupColumn, 'asc' ]],
        "displayLength": 25,
        "drawCallback": function ( settings ) {
              var api = this.api();
              var rows = api.rows( {page:'current'} ).nodes();
              var last=null;
              var subTotal = new Array();
              var groupID = -1;
              var aData = new Array();
              var index = 0;

              api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {

                var vals = api.row(api.row($(rows).eq(i)).index()).data();
                var jmlmhs = parseInt(vals[4]);

                if (typeof aData[group] == 'undefined') {
                   aData[group] = new Array();
                   aData[group].rows = [];
                   aData[group].jmlmhs = [];
                }

             		aData[group].rows.push(i);
          			aData[group].jmlmhs.push(jmlmhs);

                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group" style="background-color:#1890db;"><td colspan="6"><strong>'+group+'</strong></td></tr>'
                    );
                    last = group;
                }
              });

              var idx= 0;

            	for(var kelompok in aData){
  									 idx =  Math.max.apply(Math,aData[kelompok].rows);

                     var sumjmlmhs = 0;
                     var jmlkelompok = 0;
                     $.each(aData[kelompok].jmlmhs,function(k,v){
                          sumjmlmhs = sumjmlmhs + v;
                          jmlkelompok = jmlkelompok + 1;
                     });
                      $(rows).eq( idx ).after(
                            '<tr class="group" style="background-color:#bfbcb2;"><td colspan="4"><strong>TOTAL</strong></td>'+
                            '<td colspan="3"><strong>'+sumjmlmhs+' Mahasiswa dari '+ jmlkelompok +' Kelompok</strong></td></tr><tr><td></td></tr>'
                      );
              };

          }
      });

      tbasdos.columns().every( function () {
          var that = this;
          $( 'input', this.header() ).on( 'keyup change clear', function () {
              if ( that.search() !== this.value ) {
                  that
                      .search( this.value )
                      .draw();
              }
          } );
      } );
})
</script>
@endsection
