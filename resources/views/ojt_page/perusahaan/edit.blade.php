@extends('ojt_page.layout2')
@section('perusahaan','active')
@section('pengaturan', 'active')
@section('header')
<h1>Perusahaan</h1>
<ol class="breadcrumb">
    <li><a href="{{url('/ojt')}}"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li><a href="{{url('/ojt/perusahaan/data.html')}}"><i class="fa fa-dashboard"></i> Perusahaan</a></li>
    <li class="active">Edit</li>
</ol>
@endsection

@section('body')

<div class="row">
<div class="col-lg-8">

    <div class="box box-primary">
    
    <div class="box-body">
    <p class="text-primary">*Isikan data dengan benar</p>
    <form action="{{ url('/ojt/perusahaan/update/'.$perusahaan->kode_perusahaan) }}" method="POST">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="PUT">
    <div class="form-group row">
        <label class="col-sm-4 col-form-label">Nama Perusahaan</label>
        <div class="col-sm-8">
            <input type="text" name="nm_perusahaan" class="form-control" value="{{ old("nm_perusahaan") == "" ? $perusahaan->nama_perusahaan : old("nm_perusahaan") }}">
            @if ($errors->has("nm_perusahaan"))
                <small class="text-danger">{{ $errors->first("nm_perusahaan") }}</small>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-4 col-form-label">Alamat Perusahaan</label>
        <div class="col-sm-8">
            <input type="text" name="alamat_perusahaan" class="form-control" value="{{ old("alamat_perusahaan") <> "" ? old("alamat_perusahaan") : $perusahaan->alamat }}">
            @if ($errors->has("alamat_perusahaan"))
                <small class="text-danger">{{ $errors->first("alamat_perusahaan") }}</small>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-4 col-form-label">Kota</label>
        <div class="col-sm-8">
            <input type="text" name="kota" class="form-control" value="{{ old("kota") <> "" ? old("kota") : $perusahaan->kota }}">
            @if ($errors->has("kota"))
                <small class="text-danger">{{ $errors->first("kota") }}</small>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-4 col-form-label">Telepon Perusahaan</label>
        <div class="col-sm-8">
            <input type="text" name="telp" class="form-control" value="{{ old("telp") <> "" ? old("telp") : $perusahaan->telp }}">
            @if ($errors->has("telp"))
                <small class="text-danger">{{ $errors->first("telp") }}</small>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-4 col-form-label">Nama Penghubung</label>
        <div class="col-sm-8">
            <input type="text" name="nm_penghubung" class="form-control" value="{{ old("nm_penghubung") <> "" ? old("nm_penghubung") : $perusahaan->nama_Penghubung }}">
            @if ($errors->has("nm_penghubung"))
                <small class="text-danger">{{ $errors->first("nm_penghubung") }}</small>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-4 col-form-label">Telp Penghubung</label>
        <div class="col-sm-8">
            <input type="text" name="telp_penghubung" class="form-control" value="{{ old("telp_penghubung") <> "" ? old("telp_penghubung") : $perusahaan->telp }}" onkeypress="return isNumberKey(event)">
            @if ($errors->has("telp_penghubung"))
                <small class="text-danger">{{ $errors->first("telp_penghubung") }}</small>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label class="col-sm-4 col-form-label"></label>
        <div class="col-sm-8">
           <button type="submit" class="btn btn-primary btn-sm" > <i class="fa fa-save"></i> SIMPAN</button>
           <a href="{{ url("/ojt/perusahaan/data.html") }}" class="btn btn-danger btn-sm"><i class="fa fa-mail-reply"></i> Batal</a>
        </div>
    </div>
    </form>
    </div>
    </div>

</div>
</div>

@endsection

@section('script')
<script>
$(document).ready(function(){
   
})
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>
@endsection