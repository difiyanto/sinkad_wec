@extends('ojt_page.layout2')
@section('perusahaan', 'active')
@section('pengaturan', 'active')
@section('header')
<h1>Perusahaan</h1>
<ol class="breadcrumb">
    <li><a href="{{url('/ojt')}}"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li class="active">Perusahaan</li>
</ol>
@endsection

@section('body')
<div class="row">
<div class="col-lg-12">

    <div class="box box-primary">
        <div class="box-body" style="overflow-x:scroll">
            <a href="{{ url("/ojt/perusahaan/tambah.html") }}" class="btn btn-primary btn-sm"> <i class="fa fa-plus"></i> Tambah Data</a>
            <br>
            <br>
            <table width="110%" id="tbperusahaan" class="table table-responsive table-bordered table-striped">
                <thead>
                    <tr>
                        <th width="5%">#</th>
                        <th width="30%">Nama Perusahaan</th>
                        <th width="25%">Alamat</th>
                        <th width="10%">Kota</th>
                        <th width="15%">Penghubung</th>
                        <th width="20%">aksi</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>

</div>
</div>
@endsection

@section('script')
<script>
$(document).ready(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    getdata()

    var stsPerusahaan = "{{ session()->get('stsPerusahaan') }}";
    if(stsPerusahaan == 1){
        Swal.fire(
        'Konfirmasi',
        'Query berhasil dijalankan',
        'success'
        )
        sessionStorage.removeItem("stsPerusahaan");
    }

});

function deletex(id){  
    Swal.fire({
        title:"apakah anda yakin ?",
        text:"data yang sudah dihapus tidak bisa dikembalikan lagi.",
        icon:"warning",
        showCancelButton:true,
        confirmButtonColor:"#3085d6",
        cancelButtonColor:"#d33",
        confirmButtonText:"Ya, saya yakin"
    }).then((result)=>{
        if(result.value){
            $.ajax({
                url:"{{url('ojt/perusahaan/delete-')}}"+id,
                type:"DELETE",
                success:function(r){
                    var result = JSON.parse(r);
                    // console.log(result);
                    if(result["sts"] == 1){
                        Swal.fire(
                            "Berhasil !","Query berhasil dijalankan","success"
                        );
                        $("#tbperusahaan").DataTable().ajax.reload();
                    }
                },
                error:function(e){
                    console.log(e.responseText);
                }
            })
        }
    });
}

function softdeletex(id){  
    Swal.fire({
        title:"apakah anda yakin ?",
        text:"data yang sudah dihapus tidak bisa dikembalikan lagi.",
        icon:"warning",
        showCancelButton:true,
        confirmButtonColor:"#3085d6",
        cancelButtonColor:"#d33",
        confirmButtonText:"Ya, saya yakin"
    }).then((result)=>{
        if(result.value){
            $.ajax({
                url:"{{url('ojt/perusahaan/softdelete-')}}"+id,
                type:"DELETE",
                success:function(r){
                    var result = JSON.parse(r);
                    // console.log(result);
                    if(result["sts"] == 1){
                        $("#"+id).css("color","#ed0000");
                        Swal.fire(
                            "Berhasil !","Query berhasil dijalankan","success"
                        );
                    }
                },
                error:function(e){
                    console.log(e.responseText);
                }
            })
        }
    });
}

function getdata(){

    var tbperusahaan = $("#tbperusahaan").DataTable({
        "processing":true,
        "serverSide":true,
        "ajax":{
            "url"       :"{{url('ojt/perusahaan/getjson')}}",
            "dataType"  :"JSON",
            "type:"     :"POST"
        },
        "columns": [
            {"data":"no"},
            {"data":"nama_perusahaan"},
            {"data":"alamat"},
            {"data":"kota"},
            {"data":"nama_Penghubung"},
            {"data":"aksi"},
        ]
    });
}
</script>

@endsection
