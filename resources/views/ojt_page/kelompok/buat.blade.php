@extends('ojt_page.layout2')
@section('buat','active')
@section('kelompok','active')
@section('header')
<h1>Buat Kelompok</h1>
<ol class="breadcrumb">
    <li><a href="{{url('/ojt')}}"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li class="active">Buat Kelompok</li>
</ol>
@endsection

@section('body')

<div class="row">
<div class="col-lg-4">
 
   <div class="box box-success direct-chat direct-chat-warning">
      <div class="box-body" style="padding:10px">
        <p>Buat <strong>Kelompok</strong> <br><br>
         <form action=" {{url("/ojt/new/kelompok/buat-2")}} " method="post">
            {{  csrf_field() }}
            <input type="hidden" name="_method" value="POST">
            <input type="text" name="nim" id="nim" class="form-control form-primary" placeholder="Masukkan NIM" onkeypress="return isNumber(event)"> <br>
            <button type="submit" class="btn btn-primary btn-block btn-lg">Buat Sekarang</button>
         </form>
        </p>
      </div>
   </div>
   
</div>
</div>
@endsection

@section('script')
<script>
$(document).ready(function(){

});

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>
@endsection