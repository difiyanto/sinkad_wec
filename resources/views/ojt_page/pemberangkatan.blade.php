@extends('ojt_page.layout2')
@section('pemberangkatan','active')
@section('proses','active')
@section('header')
<h1>Data Pemberangkatan Kelompok</h1>
<ol class="breadcrumb">
    <li><a href="{{url('/ojt')}}"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li class="active">Pemberangkatan</li>
</ol>
@endsection

@section('body')
<div class="box box-primary direct-chat direct-chat-warning">
	 <div class="box-body" style="padding: 10px;">
      <table id="example2" class="table table-bordered table-hover">
        <thead>
        <tr>
          <th style="width:5%">#</th>			
          <th style="width:12%" class="cari">No Kelompok</th>
          <th style="width:30%" class="cari">Nama Perusahaan</th>
          <th style="width:15%" class="cari">Jumlah MHS</th>
          <th style="width:20%" class="cari">Jenis Pengajuan</th>
          <th style="width:15%" class="cari">Tanggal Pengajuan</th>
          <th>aksi</th>
        </tr>
        </thead>
        <tbody>
            <?php $no = 1;$jmlmhs = 0;?>
      @foreach($tampilpemberangkatan as $tp)
          <tr>
              <td>{{$no}}</td>
              <td>{{$tp['no_kelompok']}}</td>
              <td>{{$tp["getperusahaan"]['nama_perusahaan']}}</td>
              <td>{{count($tp['getdetail'])}} orang</td>
              <td>
                <strong>
                @if ($tp['sts_pencarian'] == 0)
                  OJT (Tempat Magang Cari Sendiri)
                @elseif($tp['sts_pencarian'] == 1)
                  OJT (Tempat Magang Dicarikan Lembaga)
                @else
                  TA
                @endif
            </strong>
            </td>
              <td>{{$tp['created_at']}}</td>
              <td>
                  <a href="{{url('/ojt/detailpemberangkatan/'.$tp['no_kelompok'])}}"><button class="btn btn-primary btn-sm">Detail</button>
              </td>
          </tr>
          @php
              $no++;
              $jmlmhs = $jmlmhs + count($tp['getdetail']);
          @endphp
            @endforeach
        </tbody>
      </table>

    <div class="row text-danger">
        <div class="col-md-3">
            <h4><strong>Total Jumlah Kelompok</strong></h4>
        </div>
        <div class="col-md-6">
            <h4><strong>: {{$tampilpemberangkatan->count()}}</strong></h4>
        </div>
    </div>
    <div class="row text-danger">
        <div class="col-md-3">
           <h4><strong>Total Jumlah Mahasiswa</strong></h4>
        </div>
        <div class="col-md-6">
            <h4><strong>: {{$jmlmhs}}</strong></h4>
        </div>
    </div>

    </div>
</div>

<div class="box box-warning">
    <div class="box-body" style="padding: 10px;">
      <h4><strong>Data Kelompok Sudah Diterima Perusahaan (Belum mendapat pembimbing)</strong></h4>
       <table id="example3" class="table table-bordered table-hover">
         <thead>
         <tr>
           <th>#</th>     
           <th class="cari">No Kelompok</th>
           <th class="cari">Nama Perusahaan</th>
           <th class="cari">Jumlah MHS</th>
           <th class="cari">Tanggal Pengajuan</th>
           <th>aksi</th>
         </tr>
         </thead>
         <tbody>
             <?php $no = 1;$jmlmhs = 0;?>
       @foreach($tampilpemberangkatan3 as $tp)
           <tr>
               <td>{{$no}}</td>
               <td>{{$tp['no_kelompok']}}</td>
               <td>{{$tp["getperusahaan"]['nama_perusahaan']}}</td>
               <td>{{count($tp["getdetail"])}} orang</td>
               <td>{{$tp['created_at']}}</td>
               <td>
                   <a href="{{url('/ojt/detailpemberangkatan/'.$tp['no_kelompok'])}}"><button class="btn btn-primary">Detail</button>
               </td>
           </tr>
           @php
               $no++;
               $jmlmhs = $jmlmhs + count($tp["getdetail"]);
           @endphp
             @endforeach
         </tbody>
       </table>

       <div class="row text-danger">
            <div class="col-md-3">
                <h4><strong>Total Jumlah Kelompok</strong></h4>
            </div>
            <div class="col-md-6">
                <h4><strong>: {{$tampilpemberangkatan3->count()}}</strong></h4>
            </div>
        </div>
        <div class="row text-danger">
            <div class="col-md-3">
            <h4><strong>Total Jumlah Mahasiswa</strong></h4>
            </div>
            <div class="col-md-6">
                <h4><strong>: {{$jmlmhs}}</strong></h4>
            </div>
        </div>

     </div>
 </div>

<div class="box box-danger">
    <div class="box-body" style="padding: 10px;">
      <h4><strong>Data Kelompok Yang Sudah Mendapat Pembimbing</strong></h4>
       <table id="example4" class="table table-bordered table-hover">
         <thead>
         <tr>
           <th>#</th>			
           <th class="cari">No Kelompok</th>
           <th class="cari">Nama Perusahaan</th>
           <th class="cari">Jumlah MHS</th>
           <th class="cari">Pembimbing</th>
           <th>aksi</th>
         </tr>
         </thead>
         <tbody>
             <?php $no = 1;$jmlmhs = 0;?>
       @foreach($tampilpemberangkatan2 as $tp)
           <tr>
               <td>{{$no}}</td>
               <td>{{$tp['no_kelompok']}}</td>
               <td>{{$tp["getperusahaan"]['nama_perusahaan']}}</td>
               <td>{{count($tp['getdetail'])}} orang</td>
               <td>{{$tp['getpembimbing1']["nama"]}}</td>
               <td>
                   <a href="{{url('/ojt/detailpemberangkatan/'.$tp['no_kelompok'])}}"><button class="btn btn-primary">Detail</button>
               </td>
           </tr>
           @php
               $no++;
               $jmlmhs = $jmlmhs + count($tp['getdetail']);
           @endphp
             @endforeach
         </tbody>
       </table>

       <div class="row text-danger">
            <div class="col-md-3">
                <h4><strong>Total Jumlah Kelompok</strong></h4>
            </div>
            <div class="col-md-6">
                <h4><strong>: {{$tampilpemberangkatan2->count()}}</strong></h4>
            </div>
        </div>
        <div class="row text-danger">
            <div class="col-md-3">
            <h4><strong>Total Jumlah Mahasiswa</strong></h4>
            </div>
            <div class="col-md-6">
                <h4><strong>: {{$jmlmhs}}</strong></h4>
            </div>
        </div>

     </div>
 </div>
	
@endsection
@section('css')
<link rel="stylesheet" href="{{asset('lte2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('script')
<script src="{{asset('lte2/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){
  //untuk table pertama
  $('#example2 thead .cari').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" data-toggle="tooltip" title="'+title+'" style="width:100%" class="form-control txtCari" placeholder="'+title+'" />' );
    } );
    var example2 = $('#example2').DataTable();
    example2.columns().every( function () {
        var that = this;
        $( 'input', this.header() ).on( 'keyup change clear', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );

  //untuk table kedua
  $('#example3 thead .cari').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" data-toggle="tooltip" title="'+title+'" style="width:100%" class="form-control txtCari" placeholder="'+title+'" />' );
    } );
    var example3 = $('#example3').DataTable();
    example3.columns().every( function () {
        var that = this;
        $( 'input', this.header() ).on( 'keyup change clear', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );

    //untuk table kedua
  $('#example4 thead .cari').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" data-toggle="tooltip" title="'+title+'" style="width:100%" class="form-control txtCari" placeholder="'+title+'" />' );
    } );
    var example4 = $('#example4').DataTable();
    example4.columns().every( function () {
        var that = this;
        $( 'input', this.header() ).on( 'keyup change clear', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );


  var sts_pemberangkatan = "{{ session('sts_pemberangkatan') }}";
  if(sts_pemberangkatan == 1){
    Swal.fire({
          title: 'Konfirmasi',
          text: 'Query Berhasil Dijalankan',
          type: 'success',
          confirmButtonText: 'OK'
      });
  }

});
</script>
@endsection