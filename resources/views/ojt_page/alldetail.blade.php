@extends('ojt_page.layout2')
@section('header')
<h1>Detail Kelompok</h1>
<ol class="breadcrumb">
    <li><a href="{{url('/ojt')}}"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li class="active">Detail</li>
</ol>
@endsection

@section('body')
<div class="row">
   <div class="col-lg-12">
      <div class="box box-primary direct-chat direct-chat-warning">
         <div class="box-body" style="padding: 10px;">
            <div class="form-group row">
               <label class="col-sm-3 form-control-label"><b>No Kelompok</b></label>
               <div class="col-sm-9">
                 <input id="no_kelompokxx" type="text"  class="form-control form-control-success" value="{{$datakelompok->no_kelompok}}" readonly="true" name="no_kelompok">
               </div>
             </div>
             <div class="form-group row">
               <label class="col-sm-3 form-control-label"><b>Nama Perusahaan</b></label>
               <div class="col-sm-9">
                 <input type="text" class="form-control form-control-success" value="{{$datakelompok->getperusahaan == null ? "" : $datakelompok->getperusahaan->nama_perusahaan}}" readonly="true">
               </div>
             </div>
             <div class="form-group row">
               <label class="col-sm-3 form-control-label"><b>Tanggal Pengajuan</b></label>
               <div class="col-sm-9">
                 <input type="text"  class="form-control form-control-success" value="{{$datakelompok->created_at}}" readonly="true">
               </div>
             </div>
             <div class="form-group row">
               <label class="col-sm-3 form-control-label"><b>Bulan</b></label>
               <div class="col-sm-9">
                  @php
                      $no = 1;
                      $bln = "";
                      foreach ($bulan as $bulan) {
                         if($no == $datakelompok->bulan){
                            $bln = $bulan;
                           break;
                         }
                         $no++;
                      }
                  @endphp
                 <input type="text"  class="form-control form-control-success" value="{{$bln}}" readonly="true">
               </div>
             </div>
             <div class="form-group row">
               <label class="col-sm-3 form-control-label"><b>Lama</b></label>
               <div class="col-sm-9">
                 <input type="text"  class="form-control form-control-success" value="{{$datakelompok->lama}}" readonly="true">
               </div>
             </div>
         </div>
      </div>
   </div>
</div>

<div class="row">
   <div class="col-lg-12">
      <div class="box box-primary direct-chat direct-chat-warning">
         <div class="box-body" style="padding: 10px;">
            <div class="table-responsive" style="width: 90%;">
               <table class="table table-bordered">
                 <thead>
                   <tr>
                     <th>#</th>
                     <th>NIM</th>
                     <th>Nama Mahasiswa</th>
                     <th>Gelombang</th>
                     <th>Kelas</th>
                   </tr>
                 </thead>
                 <tbody>
                   <?php $no=1 ?>
                   @foreach($datakelompok->getdetail as $d)
                   <tr>
                     <td>{{$no++}}</td>
                     <td>{{$d->nim}}</td>
                     <td>{{$d->getdu->getmhsdaft->NAMA}}</td>
                     <td>{{$d->getdu->GEL}}</td>
                     <td>{{$d->getdu->KELAS}}</td>
                   </tr>
                   @endforeach
                       
                 </tbody>
               </table>
             </div>
         </div>
      </div>
   </div>
</div>
@endsection