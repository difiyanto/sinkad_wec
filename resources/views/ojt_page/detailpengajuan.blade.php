@extends('ojt_page.layout2')
@section('pengajuan','active')
@section('proses','active')
@section('header')
<h1>Detail Kelompok OJT</h1>
<ol class="breadcrumb">
    <li><a href="{{url('/ojt')}}"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li><a href="{{url('/ojt/pengajuan.html')}}">Pengajuan</a></li>
    <li class="active">Detail Pengajuan</li>
</ol>
@endsection

@section('body')
<div class="row">
    <div class="col-lg-8">
        <div class="box box-primary direct-chat direct-chat-warning">
            <div class="box-body" style="padding:10px">
            
                <form class="form-horizontal">
                   {{ csrf_field() }}
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label"><b>No Kelompok</b></label>
                    <div class="col-sm-9">
                      <input id="txtnokelompok" type="email"  class="form-control form-control-success" value="{{$datakelompok->no_kelompok}}" readonly="true" name="no_kelompok">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label"><b>Tanggal Pengajuan</b></label>
                    <div class="col-sm-9">
                      <input type="text"  class="form-control form-control-warning" value="{{$datakelompok->created_at}}" readonly="true">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label"><b>Jenis Pengajuan </b></label>
                    <div class="col-sm-9">
                      @php

                        if($datakelompok->sts_pencarian == 0 ){
                          $jns = "OJT ( Cari tempat magang sendiri )";
                        }
                        elseif($datakelompok->sts_pencarian == 1){
                          $jns = "OJT (Tempat magang dicarikan lembaga)";
                        }else{
                          $jns = "TA";
                        }
                      @endphp
                      <input  type="text"  class="form-control form-control-warning" value="{{$jns}}" readonly="true">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label"><b>Lama {{$jns == "TA" ? "TA" : "OJT"}}</b></label>
                    <div class="col-sm-9">
                      <input id="txtlama" type="number" name="lamaojt"  class="form-control form-control-warning" value="{{ $datakelompok->lama }}" style="width:10%;float:left" min="1"> &nbsp;&nbsp;Bulan
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label"><b>Mulai Magang</b></label>
                    <div class="col-sm-9">
                      <select name="txtbulan" id="txtbulan" class="form-control form-control-warning">
                        @php
                            $no = 1;
                        @endphp
                        @foreach ($bulan as $bulan)
                          <option value="{{ $no }}" {{ $datakelompok->bulan == $no ? "selected" : "" }} >{{ $bulan }}</option>  
                          @php $no++; @endphp
                        @endforeach
                      </select>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Nama Perusahaan</label>
                    <div class="col-sm-9">
                      
                      <select name="txtkdperusahaan" id="txtkdperusahaan" class="form-control form-control-warning">
                          <option value="">[ PILIH SATU ]</option>
                        @php
                            $alamatPerusahaan = "";
                            $up               = "";
                        @endphp
                        {{-- @if ($jns != "TA") --}}
                        @foreach ($tbperusahaan as $tbperusahaan)
                          <option value="{{$tbperusahaan['kode_perusahaan']}}" {{ $datakelompok->kd_perusahaan == $tbperusahaan['kode_perusahaan'] ? "selected" : "" }}>{{$tbperusahaan['nama_perusahaan']}}</option>
                          @php
                              if($datakelompok->kd_perusahaan  == $tbperusahaan["kode_perusahaan"]){
                                $alamatPerusahaan = $tbperusahaan["alamat"]." ".$tbperusahaan["kota"];
                                $up               = $tbperusahaan["up"];
                              }
                          @endphp
                          @endforeach
                        {{-- @endif --}}
                      </select>
                      <b><div class="text-danger" id="jumlah"></div></b>
                    </div>
                  </div>
        
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Alamat Perusahaan</label>
                    <div class="col-sm-9">
                      <textarea id="txtalamatperusahaan" name="" class="form-control form-control-warning" readonly id="" cols="30" rows="2"> {{ $alamatPerusahaan }} </textarea>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">u.p.</label>
                    <div class="col-sm-9">
                      <input name="txtupperusahaan" id="txtupperusahaan" type="text" placeholder="up" class="form-control form-control-warning" value="{{$up}}" {{ $datakelompok->sts_pencarian == 2 ? "disabled" : "" }}>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label"><b>Status</b></label>
                    <div class="col-sm-9">
                      <input  type="text"  class="form-control form-control-warning" value="{{ $datakelompok->sts_kelompok }}" readonly="true">
                    </div>
                  </div>
                 
                </form>
                   
                @if ($datakelompok->sts_kelompok == "proses pengajuan")
                <a href="{{url('ojt/updatedetail/'.$datakelompok->no_kelompok)}}"><button class="btn btn-primary">Terima</button></a>
                <a href="{{url('ojt/tolakdetail/'.$datakelompok->no_kelompok)}}"><button class="btn btn-danger">Tolak</button></a>                
                @endif

                <a class="btn btn-primary" id="btnSimpan"> Simpan Perubahan</a>

            </div>
        </div>

    </div>

    <div class="col-lg-4">
      @if ($cekJumlah > 1)
      <div class="box box-primary direct-chat direct-chat-warning">
        <div class="box-body" style="padding:10px">
        <strong> <h3 class="text-danger">Harap diperhatikan !</h3></strong>
        <strong> <h4> Ada {{ $cekJumlah-1 }} kelompok lain yang mengajukan di perusahaan terpilih </h4></strong>
        </div>
      </div>
      @endif
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="box box-primary direct-chat direct-chat-warning">
            <div class="box-body" style="padding:10px">

                <h4>Anggota Kelompok</h4>
                <div class="table-responsive table-bordered " style="width: 100%;">
                    <table class="table table-bordered table-stripped">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>NIM</th>
                          <th>Nama Mahasiswa</th>
                          <th>Gelombang</th>
                          <th>Kelas</th>
                          <th>% Ketidakhadiran</th>
                          <th>sudah dibayar</th>
                          <th>harus dibayar</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $no=1 ?>
                        @foreach($detail as $dt)
                        <tr>
                          <td>{{$no++}}</td>
                          <td>{{$dt['nim']}}</td>
                          <td>{{$dt['NAMA']}}</td>
                          <td>{{$dt['GEL_DAFTAR']}}</td>
                          <td>{{$dt['KELAS']}}</td>
                          <td>{{$dt['persen']}} %</td>
                          <td>{{ number_format($dt['totalbayar'])}} </td>
                          <td>{{number_format($dt['totalangsuran'])}}</td>
                        </tr>

                        @endforeach
                            
                      </tbody>
                    </table>
                  </div>

            </div>
        </div>
    </div>
</div>

@endsection
@section('css')
<link rel="stylesheet" href="{{asset('select2/css/select2.css')}}">
@endsection
@section('script')
<script src="{{asset('select2/js/select2.js')}}"></script>
<script>
$(document).ready(function(){
  
  var base_url2 = "{{ url('/') }}";

  $('#txtkdperusahaan').select2();

  $('#txtkdperusahaan').change(function(){
  $.ajax({
      url : base_url2+"/ojt/cekPerusahaan/"+$('#txtkdperusahaan').val()+"/"+$('#txtnokelompok').val(),
      type:'GET',
      success:function(r){
        // var result = JSON.parse(r);
        console.log(r);
        $('#jumlah').html(r["pesan"]);
        $('#txtalamatperusahaan').val(r["dataperusahaan"][0]["alamat"]+" "+r["dataperusahaan"][0]["kota"]);
        $("#txtupperusahaan").val(r["dataperusahaan"][0]["up"]);
      },
      error:function(e){
        console.log(e.responseText);
      }
    });
  });

  $("#btnSimpan").click(function(e){
   e.preventDefault();
    url = "{{ url('/')}}";
    var up = "empty";
    if($("#txtupperusahaan").val() != ""){
      up = $("#txtupperusahaan").val();
    }
    window.location = url+"/ojt/ubahPerusahaan/"+$("#txtkdperusahaan").val()+"/"+up+"/"+$("#txtnokelompok").val()+"/"+$("#txtlama").val()+"/"+$("#txtbulan").val();
  });




});
</script>
@endsection