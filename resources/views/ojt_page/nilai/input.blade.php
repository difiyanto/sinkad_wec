@extends('ojt_page.layout2')
@section('nilai', 'active')
@section('input-nilai', 'active')
@section('header')
<h1>Input Nilai OJT/TA</h1>
<ol class="breadcrumb">
    <li><a href="{{url('/ojt')}}"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li class="active">Input Nilai OJT/TA</li>
</ol>
@endsection
@section('body')
<div class="row">
<div class="col-lg-8">
    <div class="box box-primary" >
        <div class="box-header">
          <h5 class="text-bold">Cari berdasarkan nomor kelompok</h5>
        </div>
        <div class="box-body chat" id="chat-box">
            <select name="nomorKelompok" id="nomorKelompok" class="form-control">
                <option value="">[ PILIH SATU ]</option>
                @foreach ($kelompok as $i)
                <option value="{{$i->no_kelompok}}">{{$i->no_kelompok}}</option>
                @endforeach
            </select>
            <br><br>
            <button class="btn btn-primary" id="btnTampilkan">TAMPILKAN</button>
            <br>
        </div>
    </div>
</div>
</div>

<div class="row">
<div class="col-lg-12">
    <div class="box box-primary" id="datakelompok">
        <div class="box-body chat" >
            <div id="cover">
            </div>
            
        </div>
    </div>
</div>
</div>



<div class="modal fade" id="modal_ceknim" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title" id="exampleModalLabel">KONFIRMASI</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div id="data_mhs"></div>
        </div>
        </div>
    </div>
</div>
@endsection

@section('css')
<link rel="stylesheet" href="{{asset('lte2/bower_components/select2/dist/css/select2.min.css')}}">
@endsection

@section('script')
<script src="{{asset('lte2/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
<script>
$(document).ready(function(){
    $("#nomorKelompok").select2()
    $("#datakelompok").hide()

    $("#btnTampilkan").click(function(){
        var nokelompok = $("#nomorKelompok").val()
        $.ajax({
            url:"{{url('/ojt/input-nilai/cari')}}/"+nokelompok,
            type:'GET',
            success:function(r){
                $("#cover").html(r);
                $("#datakelompok").show()
            },
            error:function(e){
                console.log(e.responseText)
                alert("Terjadi Kesalahan !")
            }
        })
    })

   
})
</script>
@endsection