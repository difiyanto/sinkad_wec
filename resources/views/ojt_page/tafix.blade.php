@extends('ojt_page.layout2')
@section('ta','active')
@section('rekap','active')
@section('header')
<h1>Kelompok TA</h1>
<ol class="breadcrumb">
    <li><a href="{{url('/ojt')}}"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li class="active">Kelompok TA</li>
</ol>
@endsection
@section('body')

<div class="box box-primary direct-chat direct-chat-warning">
    <h4 class="box-header"><strong>Data Kelompok TA</strong></h4>
    <div class="box-body" style="padding: 10px;">
        <table id="kelompok2" class="table table-bordered table-hover">
        <thead>
        <tr>			
            <th>No Kelompok</th>
            <th>Pembimbing</th>
            <th>NIM</th>
            <th>Nama MHS</th>
            <th>Kelas</th>
        </tr>
        </thead>
        <tbody>
        @foreach($kelompok2 as $itm)
           @foreach ($itm->getdetail as $d)
           <tr>
               <td>{{$itm['no_kelompok']}}</td>
               <td>{{$itm['getpembimbing1']["nama"]}}</td>
               <td>{{$d->nim}}</td>
               <td>{{$d->getdu->getmhsdaft->NAMA}}</td>
               <td>{{$d->getdu->KELAS}}</td>
           </tr>
           @endforeach
        @endforeach
        </tbody>
        </table>

    </div>
</div>

@endsection
@section('css')
<link rel="stylesheet" href="{{asset('lte2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('script')
<script src="{{asset('lte2/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/jszip.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/buttons.print.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){
	

    // $('#kelompok2').DataTable();

    var groupColumn = 0;
    var kelompok2 = $('#kelompok2').DataTable({
      "columnDefs": [
            { "visible": false, "targets": groupColumn }
        ],
      "dom": 'Bfrtip',
      "buttons": [
            'csv', 'excel', 'pdf'
        ],
      "ordering":false,
      "paging":true,
      "order": [[ groupColumn, 'asc' ]],
      "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
            var subTotal = new Array();
            var groupID = -1;
            var aData = new Array();
            var index = 0;

            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {

              var vals = api.row(api.row($(rows).eq(i)).index()).data();
              var jmlmhs = parseInt(vals[4]);

              if (typeof aData[group] == 'undefined') {
                 aData[group] = new Array();
                 aData[group].rows = [];
                 aData[group].jmlmhs = [];
              }

           		aData[group].rows.push(i);
        			aData[group].jmlmhs.push(jmlmhs);

              if ( last !== group ) {
                  $(rows).eq( i ).before(
                      '<tr class="group" style="background-color:#1890db;"><td colspan="5"><strong>'+group+'</strong></td></tr>'
                  );
                  last = group;
              }
            });

            // var idx= 0;

          	// for(var kelompok in aData){
			// 						 idx =  Math.max.apply(Math,aData[kelompok].rows);

            //        var sumjmlmhs = 0;
            //        var jmlkelompok = 0;
            //        $.each(aData[kelompok].jmlmhs,function(k,v){
            //             sumjmlmhs = sumjmlmhs + v;
            //             jmlkelompok = jmlkelompok + 1;
            //        });
            //         $(rows).eq( idx ).after(
            //               '<tr class="group" style="background-color:#bfbcb2;"><td colspan="3"><strong>TOTAL</strong></td>'+
            //               '<td colspan="2"><strong>'+sumjmlmhs+' Mahasiswa dari '+ jmlkelompok +' Kelompok</strong></td></tr><tr><td></td></tr>'
            //         );
            // };

        }
    });

    var sts_kelompok = "{{ session('sts_kelompok') }}";
    if(sts_kelompok == 1){
        Swal.fire({
            title: 'Konfirmasi',
            text: 'Query Berhasil Dijalankan',
            type: 'success',
            confirmButtonText: 'OK'
        });
    }

});
</script>
@endsection