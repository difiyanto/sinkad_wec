@extends('ojt_page.layout2')
@section('pengajuan','active')
@section('proses','active')
@section('header')
<h1>Data Pengajuan Kelompok OJT / TA</h1>
<ol class="breadcrumb">
    <li><a href="{{url('/ojt')}}"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li class="active">Pengajuan</li>
</ol>
@endsection

@section('body')
<input type="hidden" name="sts_pengajuan" id="sts_pengajuan" data-value="{{Session::get('sts_pengajuan')}}">

<div class="row">
<div class="col-lg-12">

    <div class="box box-primary direct-chat direct-chat-warning">
        <div class="box-body" style="padding:10px">

<!-- <a href="{{ url("download/+berkas+PROPOSAL OJT ROI.pdf") }}" class="btn btn-warning btn-sm" id="btncetak" target="_blank">Download Proposal Pengajuan</a>
<br>
<br> -->

                <table class="table" id="tbkelompok">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th class="cari">No Kelompok</th>
                      <th class="cari">Nama Perusahaan</th>
                      <th class="cari">Jumlah MHS</th>
                      <th class="cari">Jenis Pengajuan</th>
                      <th class="cari">Tanggal Pengajuan</th>
                      <th>aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                      <?php $no = 1;$jmlmhs = 0;?>
                  @foreach($tampilpengajuan as $tp)
                      <tr>
                          <td>{{$no}}</td>
                          <td>{{$tp['no_kelompok']}}</td>
                          <td>{{$tp["getperusahaan"]['nama_perusahaan'] == null ? "" : $tp["getperusahaan"]['nama_perusahaan']}}</td>
                          <td>{{count($tp["getdetail"])}} orang</td>
                          <td>
                              @if ($tp['sts_pencarian'] == 0)
                                OJT (Tempat Magang Cari Sendiri)
                              @elseif($tp['sts_pencarian'] == 1)
                                OJT (Tempat Magang Dicarikan Lembaga)
                              @else
                                TA
                              @endif
                          </td>
                          <td>{{$tp['created_at']}}</td>
                          <td>
                              <a href="{{url('/ojt/detailpengajuan/'.$tp['no_kelompok'])}}"><button class="btn btn-primary btn-sm">Detail</button>
                          </td>
                      </tr>
                      @php
                          $no++;
                          $jmlmhs = $jmlmhs + count($tp["getdetail"]);
                      @endphp
                       @endforeach
                  </tbody>
                </table>

                <div class="row text-danger">
                    <div class="col-md-3">
                        <h4><strong>Total Jumlah Kelompok</strong></h4>
                    </div>
                    <div class="col-md-6">
                        <h4><strong>: {{$tampilpengajuan->count()}}</strong></h4>
                    </div>
                </div>
                <div class="row text-danger">
                    <div class="col-md-3">
                       <h4><strong>Total Jumlah Mahasiswa</strong></h4>
                    </div>
                    <div class="col-md-6">
                        <h4><strong>: {{$jmlmhs}}</strong></h4>
                    </div>
                </div>
        </div>
    </div>

</div>
</div>

<div class="row">
        <div class="col-lg-12">

            <div class="box box-primary direct-chat direct-chat-warning">
                <div class="box-body" style="padding:10px">
                    <h4><strong>Data Kelompok Yang Sudah Diterima</strong></h4>
                        <table class="table" id="tbkelompok_diterima">
                          <thead>
                            <tr>
                              <th>#</th>
                              <th style="width:8%" class="cari">No Kelompok</th>
                              <th style="width:27%" class="cari">Nama Perusahaan</th>
                              <th style="width:10%" class="cari">Jumlah MHS</th>
                              <th style="width:20%" class="cari">Jenis Pengajuan</th>
                              <th style="width:8%" class="cari">Tanggal Pengajuan</th>
                              <th>aksi</th>
                            </tr>
                          </thead>
                          <tbody>
                              <?php $no = 1;$jmlmhs = 0;?>
                          @foreach($diterima as $tpt)
                              <tr>
                                  <td>{{$no}}</td>
                                  <td>{{$tpt['no_kelompok']}}</td>
                                  <td>{{$tpt["getperusahaan"]['nama_perusahaan']}}</td>
                                  <td>{{count($tpt["getdetail"])}} orang</td>
                                  <td>
                                      @if ($tpt['sts_pencarian'] == 0)
                                        OJT (Tempat Magang Cari Sendiri)
                                      @elseif($tpt['sts_pencarian'] == 1)
                                        OJT (Tempat Magang Dicarikan Lembaga)
                                      @else
                                        TA
                                      @endif
                                  </td>
                                  <td>{{$tpt['created_at']}}</td>
                                  <td>
                                    <a href="{{url('/ojt/detailpengajuan/'.$tpt['no_kelompok'])}}"><button class="btn btn-primary btn-sm">Detail</button>

                                    <a href="{{url('/ojt/tolak/'.$tpt['no_kelompok'])}}"><button class="btn btn-danger btn-sm">Buka pengajuan baru</button>
                                  </td>
                              </tr>
                              @php
                                  $no++;
                                  $jmlmhs = $jmlmhs + count($tpt["getdetail"]);
                              @endphp
                               @endforeach
                          </tbody>
                        </table>

                        <div class="row text-danger">
                            <div class="col-md-3">
                                <h4><strong>Total Jumlah Kelompok</strong></h4>
                            </div>
                            <div class="col-md-6">
                                <h4><strong>: {{$diterima->count()}}</strong></h4>
                            </div>
                        </div>
                        <div class="row text-danger">
                            <div class="col-md-3">
                               <h4><strong>Total Jumlah Mahasiswa</strong></h4>
                            </div>
                            <div class="col-md-6">
                                <h4><strong>: {{$jmlmhs}}</strong></h4>
                            </div>
                        </div>
                </div>
            </div>

        </div>
        </div>

<div class="row">
<div class="col-lg-6">
    <div class="box box-danger direct-chat direct-chat-warning">
        <div class="box-body" style="padding:10px">
            <h4><strong>Data Kelompok Yang Ditolak oleh TIM OJT</strong></h4>
                <table class="table" id="tbkelompok_ditolaktim">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>No Kelompok</th>
                        <th>Nama Perusahaan</th>
                        <th>aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1?>
                    @foreach($ditolak as $tp)
                        <tr>
                            <td>{{$no}}</td>
                            <td>{{$tp['no_kelompok']}}</td>
                            <td>{{$tp["getperusahaan"]['nama_perusahaan']}}</td>
                            <td>
                                <a href="{{url('/ojt/detailpengajuan/'.$tp['no_kelompok'])}}"><button class="btn btn-primary btn-sm">Detail</button>
                            </td>
                        </tr>
                        @php
                            $no++;
                        @endphp
                        @endforeach
                    </tbody>
                </table>
        </div>
    </div>
</div>
<div class="col-lg-6">
    <div class="box box-danger direct-chat direct-chat-warning">
        <div class="box-body" style="padding:10px">
            <h4><strong>Data Kelompok Yang Ditolak oleh Perusahaan</strong></h4>
                <table class="table" id="tbkelompok_ditolakperusahaan">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>No Kelompok</th>
                        <th>Nama Perusahaan</th>
                        <th>aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1?>
                    @foreach($ditolak2 as $tp)
                        <tr>
                            <td>{{$no}}</td>
                            <td>{{$tp['no_kelompok']}}</td>
                            <td>{{$tp["getperusahaan"]['nama_perusahaan']}}</td>
                            <td>
                                <a href="{{url('/ojt/detailpengajuan/'.$tp['no_kelompok'])}}"><button class="btn btn-primary btn-sm">Detail</button>
                            </td>
                        </tr>
                        @php
                            $no++;
                        @endphp
                        @endforeach
                    </tbody>
                </table>
        </div>
    </div>
</div>
</div>
@endsection
@section('css')
<link rel="stylesheet" href="{{asset('lte2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('script')
<script src="{{asset('lte2/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

<script>
$(document).ready(function(){

  //untuk table pertama
    $('#tbkelompok thead .cari').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" data-toggle="tooltip" title="'+title+'" style="width:100%" class="form-control txtCari" placeholder="'+title+'" />' );
    } );
    var tbkelompok = $('#tbkelompok').DataTable();
    tbkelompok.columns().every( function () {
        var that = this;
        $( 'input', this.header() ).on( 'keyup change clear', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );

    //untuk table kedua
    $('#tbkelompok_diterima thead .cari').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" data-toggle="tooltip" title="'+title+'" style="width:100%" class="form-control txtCari" placeholder="'+title+'" />' );
    } );
    var tbkelompok_diterima = $('#tbkelompok_diterima').DataTable();
    tbkelompok_diterima.columns().every( function () {
        var that = this;
        $( 'input', this.header() ).on( 'keyup change clear', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );

  $('#tbkelompok_ditolaktim').DataTable();
  $('#tbkelompok_ditolakperusahaan').DataTable();

  if($('#sts_pengajuan').data("value") == 1){
      Swal.fire({
          title: 'Konfirmasi',
          text: 'Query Berhasil Dijalankan',
          type: 'success',
          confirmButtonText: 'OK'
      });
  }

});
</script>
@endsection
