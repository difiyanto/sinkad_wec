<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kartu Kelompok OJT</title>
     <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{asset('lte2/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">

    <style type="text/css" media="print">
        @page 
        {
            size: auto;   /* auto is the initial value */
            margin: 0mm;  /* this affects the margin in the printer settings */
            font-size: 12px;
        }

        #box{
            width: 100%;
            /* height: 300px; */
            padding:10px;
            /* border-style: solid; */
            /* border: 1em; */
        }
    </style>
</head>
<body>

<div id="box">

<div class="container-fluid">
<div class="row">

<div class="col-xs-12">
        <div class="container-fluid" style="border-style:solid">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 text-center">
                    <h5>FORMULIR PEMBERANGKATAN OJT</h5>
                    </div>
                </div>
                <div class="row" style="margin-top:20px;">
                    <div class="col-lg-8 col-sm-8 col-md-8 col-xs-8">
                        Dengan ini, memberitahukan bahwa kami dari kelompok OJT :
                    </div>
                    <div class="col-lg-4 col-sm-4 col-md-4 col-xs-4" style="text-align:right;">
                        Malang, {{ Date("d F Y") }}
                    </div>
                </div>
                <div class="row" >
                    <div class="col-xs-3 col-sm-3 col-lg-3 col-md-3">
                        NO. Kelompok
                    </div>
                    <div class="col-xs-7 col-sm-7 col-lg-7 col-md-7">
                        : {{$ojt[0]["no_kelompok"]}}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-3 col-sm-3 col-lg-3 col-md-3">
                        Nama Perusahaan
                    </div>
                    <div class="col-xs-7 col-sm-7 col-lg-7 col-md-7">
                        : {{$ojt[0]["nama_perusahaan"]}}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-3 col-sm-3 col-lg-3 col-md-3">
                        Alamat Perusahaan
                    </div>
                    <div class="col-xs-7 col-sm-7 col-lg-7 col-md-7">
                        : {{$ojt[0]["alamat"]}}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-3 col-sm-3 col-lg-3 col-md-3">
                        Penghubung
                    </div>
                    <div class="col-xs-7 col-sm-7 col-lg-7 col-md-7">
                        : 
                    </div>
                </div>
                <div class="row" style="margin-top:15px;">
                    <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
                        <strong>Data Anggota Kelompok</strong><br>
                        <table style="width:100%;" border="1">
                            <thead>
                            <tr class="text-center">
                                <th style="width:5%;" class="text-center">NO</th>
                                <th style="width:10%;" class="text-center">NIM</th>
                                <th style="width:40%;" class="text-center">NAMA</th>
                                <th style="width:10%;" class="text-center">KELAS</th>
                                <th style="width:8%;" class="text-center">% Absensi</th>
                            </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                @endphp
                            @foreach ($ojt as $ojt)
                            <tr>
                                <td style="padding-left:8px;">{{ $no++ }}</td>
                                <td style="padding-left:8px;">{{$ojt["NIM"]}}</td>
                                <td style="padding-left:5px;">{{$ojt["NAMA"]}}</td>
                                <td style="padding-left:8px;">{{$ojt["KELAS"]}}</td>
                                <td style="padding-left:8px;">{{$ojt["persen"]}} %</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row" style="margin-top:5px;">
                    <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
                    Selanjutnya kami bersedia mentaati segala peraturan yang berkaitan dengan OJT sebagai berikut : 
                    <ol>
                        <li>Akan kembali ke kampus langsung pada tanggal _______________ tanpa alasan apapun, karena pada tgl tsb dan seterusnya kami akan mengikuti kuliah susulan sesuai penjadwalan di mading. dan jika kami tidak mengikuti jadwal kuliah susulan karena alasan (a) tidak mengetahui pengumuman di mading dan (b) mengikuti kuliah susulan tetapi tidak sesuai jadwal yang ditetapkan maka kami tidak berhak untuk meminta ganti mata kuliah yang ditinggalkan pada jam atau kelas lain.</li>
                        <li>Akan menyelesaikan laporan OJT selambat-lambatnya 1 bulan setelah point(1) yaitu pada tanggal _________________</li>
                        <li>Mengumpulkan laporan OJT/TA disekretariat kampus pusat selambat-lambatnya 15 hari setelah tanggal komprehensif dan jika terlambat atau tidak mengumpulkan maka kami tidak berhak atas transkrip</li>
                        <li>Setiap anggota dalam kelompoknya masing-masing wajib (1) mengetahui alamat/telepon masing-masing anggotanya, (2) nomor kelompoknya dan (3) mempunyai file laporan OJT baik dalam bentuk print atau flashdisk</li>
                    </ol>
                    </div>
                </div>
                <div class="row" style="margin-top:10px;">
                    <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
                       Mengetahui, 
                    </div>
                </div>
                <div class="row" style="margin-bottom:10px;">
                    <div class="col-xs-3 col-sm-3 col-lg-3 col-md-3">
                        TIM OJT
                        <br><br><br><br>
                        _______________ 
                    </div>
                    <div class="col-xs-3 col-sm-3 col-lg-3 col-md-3">
                        Sekretariat
                        <br><br><br><br>
                        _______________ 
                    </div>
                    <div class="col-xs-3 col-sm-3 col-lg-3 col-md-3">
                        Pembimbing 1
                        <br><br><br><br>
                        _______________ 
                    </div>
                    <div class="col-xs-3 col-sm-3 col-lg-3 col-md-3">
                        Pembimbing 2
                        <br><br><br><br>
                        _______________ 
                    </div>
                    {{-- <div class="col-xs-3 col-sm-3 col-lg-3 col-md-3">
                        <br><br><br>
                        Sekretariat / FO
                    </div> --}}
                </div>
            
            
                
            </div>
</div>

</div>
</div>

</div>

<br>
<center> --------------------------- potong disini --------------------------- </center>

<!-- jQuery 3 -->
<script src="{{asset('lte2/bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('lte2/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script>
window.print();
</script>
</body>
</html>