@extends('mhs_page.layout2')
@section('nilaikelas','active')
@section('header')
<h1>
Rata-rata Nilai Tiap Kelas
</h1>
<ol class="breadcrumb">
    <li><a href="{{url('/mahasiswa/index.html')}}"><i class="fa fa-home"></i> Beranda</a></li>
    <li class="active">Rata-rata nilai</li>
</ol>
@endsection
@section('body')
<div class="row">
    <div class="col-lg-8 col-sm-8 col-xs-12">
        <div class="box box-success direct-chat direct-chat-warning">
        <div class="box-body" style="padding:10px">
            <div class="row">
                <div class="col-lg-4">PILIH JURUSAN</div>
                <div class="col-lg-8">
                    <select name="slcJurusan" id="slcJurusan" class="form-control">
                        <option value="" disabled selected>[ pilih ]</option>
                        @foreach ($jurusan->get() as $dJurusan)
                            <option value="{{$dJurusan['kd_jurusan']}}" >{{$dJurusan['nama_jurusan']}}</option>    
                        @endforeach
                        
                    </select>
                </div>
            </div>
            <div class="row" id="pilkelas" style="padding-top:1em;">
                <div class="col-lg-4">PILIH KELAS</div>
                <div class="col-lg-8">
                    <div class="opt_kelas"></div>
                </div>
            </div>
            
            <div class="row" id="pilmatkul" style="padding-top:1em;">
                <div class="col-lg-4">PILIH MATA KULIAH</div>
                <div class="col-lg-8">
                <div class="opt_matkul"></div>
                </div>
            </div>
            <div class="row" id="submit" style="padding-top:1em;">
                <div class="col-lg-4"></div>
                <div class="col-lg-8">
                <button class="btn btn-primary" id="btnSubmit">SUBMIT</button>
                </div>            
            </div>
            <div class="fbnilaikelas" style="padding-top:1em;"></div>
        </div>
        </div>
    
    </div>
    <div class="col-lg-4 col-sm-4 col-xs-12">
        <div class="box box-success direct-chat direct-chat-warning" id="cover_rata">
        <div class="box-body" style="padding:10px">
            <div id="datarata"></div>
        </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">

        <div class="box box-success direct-chat direct-chat-warning" id="cover_nilai">
        <div class="box-body" style="padding:10px">
            
            <div id="datanilai"></div>
        
        </div>
        </div>

    </div>
</div>
@endsection
@section('css')
@endsection
@section('script')
<script>
$(document).ready(function(){

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
var base_url2 = $('#base_url2').val();


$('#pilkelas').hide();
$('#pilmatkul').hide();
$('#submit').hide();
$('#cover_nilai').hide();
$('#cover_rata').hide();

$('#slcJurusan').on('change', function(){
var jurusan = $(this).val();
$.ajax({
    url:base_url2+'/mahasiswa/cariKelasByJurusan/'+jurusan,
    type:'GET',
    success:function(result){
    var Arrresult =  JSON.parse(result);
    $('.opt_kelas').html(Arrresult.kelas);
    $('.opt_matkul').html(Arrresult.matkul);
    $('#pilkelas').show();
    $('#pilmatkul').show();
    $('#submit').show();
    },
    error:function(result){
    alert("Terjadi Kesalahan !");
    console.log(result.responseText);
    }
});
});

$('#btnSubmit').click(function(){
var kelas = $('#slcKelas').val();
var jurusan = $('#slcJurusan').val();
var matkul = $('#slcMatkul').val();
$.ajax({
    url : base_url2+"/mahasiswa/nilaiperkelas",
    type:'POST',
    data:{kelas:kelas, jurusan:jurusan, matkul:matkul},
    beforeSend:function(){
    $('.fbnilaikelas').html("<p class='text-primary'>Tunggu Sebentar ... </p>")
    },
    success:function(result){
    var res = JSON.parse(result);
    $('.fbnilaikelas').html("");
    $('#datanilai').html(res['tbnilai']);
    $('#cover_nilai').show();
    $('#datarata').html(res['ratarata']);
    $('#cover_rata').show();
    },
    error:function(result){
    alert('Terjadi Kesalahan !');
    console.log(result.responseText);
    }
});
});

});
</script>
@endsection