@extends('mhs_page.layout2')
@section('pengaturan','active')
@section('header')
<h1>
Profil
</h1>
<ol class="breadcrumb">
    <li><a href="{{url('/mahasiswa/index.html')}}"><i class="fa fa-home"></i> Beranda</a></li>
    <li class="active">Profil</li>
</ol>
@endsection
@section('body')
<div class="row">
<div class="col-lg-4">
    <div class="box box-success">
    <div class="box-body" style="padding:10px;">
      <center>
      <img class="profile-user-img img-responsive img-circle" src="{{asset('images/user.jpg')}}" alt="User profile picture">
      <h4>{{ session("nama_mhs") }}</h4>
      ( {{session("kelas")}} )<br>
      <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#pwdModal">UBAH PASSWORD</button>
      </center>
    </div>
    </div>
</div>
<div class="col-lg-8">

  <div class="box box-success">
  <div class="box-body" style="padding:10px;">
    
    <div class="row" style="padding-top:2em;">
      <div class="col-lg-3" style="text-align:left;">NIM</div>
      <div class="col-lg-6" style="text-align:left;">: {{session('nim')}}</div>
  </div>
   <div class="row" style="padding-top:1em;">
      <div class="col-lg-3" style="text-align:left;">Jurusan</div>
      <div class="col-lg-6" style="text-align:left;">: {{$profil[0]['jurusan']}} ( {{$profil[0]['KELAS']}} )</div>
  </div>
   <div class="row" style="padding-top:1em;">
      <div class="col-lg-3" style="text-align:left;">Gelombang</div>
      <div class="col-lg-6" style="text-align:left;">: {{$profil[0]['GEL_DAFTAR']}}</div>
  </div>
   <div class="row" style="padding-top:2em;">
      <div class="col-lg-3" style="text-align:left;">Tempat, Tgl Lahir</div>
      <div class="col-lg-6" style="text-align:left;">: {{$profil[0]['TMP_LAHIR']}}, {{ date("d M Y", strtotime($profil[0]['TGL_LAHIR']))}}</div>
  </div>
   <div class="row" style="padding-top:2em;">
      <div class="col-lg-3" style="text-align:left;">Alamat Lengkap</div>
      <div class="col-lg-6" style="text-align:left;">: {{$profil[0]['ALAMAT1']}}{{$profil[0]['ALAMAT2']}}</div>
  </div>
   <div class="row" style="padding-top:2em;">
      <div class="col-lg-3" style="text-align:left;">Telp</div>
      <div class="col-lg-6" style="text-align:left;">: {{$profil[0]['TELEPON']}}</div>
  </div>

  </div>
  </div>

</div>
</div>


<!-- Modal -->
<div class="modal fade" id="pwdModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ubah Password</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
                <div class="col-lg-12">
                    <input type="password" name="txtpwdLama" id="txtpwdLama" class="form-control" placeholder="Masukkan Password Lama">
                    <div id="f" class="text-primary"><b> * Tekan Enter </b></div>
                </div>

                <div class="col-lg-12" style="padding-top:1em;">
                    <input type="password" name="" id="txtpwdBaru" class="form-control" placeholder="Masukkan Password Baru">
                </div>

                <div class="col-lg-12" style="padding-top:1em;">
                    <input type="password" name="" id="txtpwdBaruKonfir" class="form-control" placeholder="Konfirmasi Password Baru">
                    <div id="e" class="text-primary"><b>* Tekan Enter </b></div>
                </div>

                <div class="col-lg-12">
                    <div id="feedback-gantipwd" style="padding-top:1em;">
                        <div class="loader"></div>
                    </div>
                </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="simpanpwdbaru">SIMPAN</button>
      </div>
    </div>
  </div>
</div>
@endsection

{{-- section css --}}
@section('css')
@endsection
{{-- end of section css --}}

{{-- section javascript --}}
@section('script')

<script>
$(document).ready(function(){
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  var base_url2 = $('#base_url2').val();
  $('#txtpwdBaru').hide();
  $('#txtpwdBaruKonfir').hide();
  $('#e').hide()
  $('#simpanpwdbaru').hide();
  $('#txtpwdLama').keyup(function(e){
        if(e.keyCode == 13){
          $.ajax({
          url : base_url2+"/mahasiswa/gantipwd",
          type:'POST',
          data:{pwdlama : $(this).val()},
          success:function(result){
            // alert(result);
            if(result == 1){
              $('#txtpwdBaru').show();
              $('#txtpwdBaruKonfir').show();
              $('#e').show();
              $('#f').hide();
              $('#txtpwdBaru').focus();
            }else{
              $('#feedback-gantipwd').html("<div class='alert alert-danger'>* Password Salah !</div>");
              setInterval(function(){ 
                $('#feedback-gantipwd').html("");
               }, 1000);
            }
          },
          error:function(result){
            alert('Terjadi Kesalahan !');
            console.log(result.responseText);
          }
        });
        }
      });

      $('#txtpwdBaruKonfir').keyup(function(e){
        if(e.keyCode == 13){

        var pwdbaru = $('#txtpwdBaru').val();
        var pwdbarukonfir = $('#txtpwdBaruKonfir').val();
        if(pwdbaru != pwdbarukonfir){
          $('#feedback-gantipwd').html("<div class='alert alert-danger'>* Password Tidak Sama !</div>");
           setInterval(function(){ 
                $('#feedback-gantipwd').html("");
               }, 3000);
        }else{
          $.ajax({
            url:base_url2+'/mahasiswa/gantipwd2',
            type:'POST',
            data:{pwdBaru:$('#txtpwdBaru').val()},
            beforeSend:function(){
              $('#feedback-gantipwd').html("<div class='alert alert-primary'>* Tunggu Sebentar ...</div>");
            },
            success:function(result){
              if(result == 1){
                $('#feedback-gantipwd').html("<div class='alert alert-primary'>* Password berhasil diubah</div>");
                setInterval(function(){ 
                window.location.href = base_url2+"/mahasiswa/pengaturan.html";
               }, 3000);
              }else{
                $('#feedback-gantipwd').html("<div class='alert alert-danger'>* Error !</div>");
              }
            },
            error:function(result){
              alert('Terjadi Kesalahan !');
              console.log(result.responseText);
            }
          });
        }
        }
      })

      $('#simpanpwdbaru').click(function(){
        var pwdbaru = $('#txtpwdBaru').val();
        var pwdbarukonfir = $('#txtpwdBaruKonfir').val();
        if(pwdbaru != pwdbarukonfir){
          $('#feedback-gantipwd').html("<div class='alert alert-danger'>* Konfirmasi Password Tidak Sama !</div>");
           setInterval(function(){ 
                $('#feedback-gantipwd').html("");
               }, 1000);
        }else{
          $.ajax({
            url:base_url2+'/mahasiswa/gantipwd2',
            type:'POST',
            data:{pwdBaru:$('#txtpwdBaru').val()},
            beforeSend:function(){
              $('#feedback-gantipwd').html("<div class='alert alert-primary'>* Tunggu Sebentar ...</div>");
            },
            success:function(result){
              if(result == 1){
                $('#feedback-gantipwd').html("<div class='alert alert-primary'>* Password berhasil diubah</div>");
                setInterval(function(){ 
                window.location.href = base_url2+"/mahasiswa/pengaturan.html";
               }, 4000);
              }else{
                $('#feedback-gantipwd').html("<div class='alert alert-danger'>* Error !</div>");
              }
            },
            error:function(result){
              alert('Terjadi Kesalahan !');
              console.log(result.responseText);
            }
          })
        }
      });

});
</script>
@endsection
{{-- end of section javascript --}}
    