@extends('mhs_page.layout2')
@section('pengajuan','active')
@section('ojt','active')
@section('header')
<h1>
Pengajuan Kelompok OJT / TA
</h1>
<ol class="breadcrumb">
    <li><a href="{{url('/mahasiswa/index.html')}}"><i class="fa fa-home"></i> Beranda</a></li>
    <li class="active">Pengajuan</li>
</ol>
@endsection
@section('body')
@php
    $alamatPerusahaan = "";
@endphp
@if ($jmlkelompokojt == 0)
<div class="row">
<div class="col-lg-6">

    <div class="box box-success direct-chat direct-chat-warning">
    <div class="box-body" style="padding:10px">
      <p>Anda belum pernah melakukan pengajuan kelompok OJT ?<br>
      <a href="{{url('/mahasiswa/pengajuan/buatkelompok')}}" class="btn btn-primary btn-sm">Ajukan Sekarang</a> <br>
      <a href="#" id="termOJT"><small class="text-danger">* 
          Pelajari syarat pengajuan kelompok OJT
      </small></a>
      </p>
    </div>
    </div>

</div>

<div class="col-lg-6">
  
    <div class="box box-success direct-chat direct-chat-warning">
      <div class="box-body" style="padding:10px">
        <p>Pengajuan <strong>TA</strong> <br>
        <a href="{{url('/mahasiswa/pengajuan ta/buatkelompok')}}" class="btn btn-primary btn-sm">Ajukan Sekarang</a> <br> <small class="text-danger">* Sebelum mengajukan TA, konsultasikan terlebih dahulu dengan tim OJT</small><br>
        </p>
      </div>
      </div>
   
  </div>
  </div>


<div class="modal fade" id="modal_termOJT">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Persyaratan Pengajuan OJT</h4>
      </div>
      <div class="modal-body">
        <ol>
          <li class="text-uppercase">Semua anggota kelompok harus berasal dari jurusan yang sama.</li>
          <br>
          <li class="text-uppercase">Semua anggota kelompok harus berasal dari gelombang pendaftaran yang sama.</li>
          <br>
          <li class="text-uppercase">Semua anggota kelompok harus sudah melunasi biaya administrasi di bulan berjalan.</li>
          <br>
          <li class="text-uppercase">Semua anggota kelompok memiliki presentase ketidakhadiran < 30%.</li>
        </ol>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Baik, Saya Mengerti</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

@else
{{-- area ketua kelompok --}}
@if ($kelompokojt[0]["jabatan"] == "KETUA")
<div class="row">
<div class="col-lg-12">
    
    <div class="box box-success direct-chat direct-chat-warning">
    <div class="box-body" style="padding:10px">
      <form class="form-horizontal" method="POST" action="{{url('mahasiswa/ubahPengajuan')}}">
      {{csrf_field()}}
      <input type="hidden" name="_method" value="POST">
      <div class="row">
        <div class="col-lg-7">

          <div class="form-group row">
            <label class="col-sm-4 form-control-label">No Kelompok</label>
            <div class="col-sm-8">
                <input id="txtnokelompok" name="txtnokelompok" type="text" placeholder="Nomor Kelompok" class="form-control form-control-success" readonly value="{{ $master->no_kelompok }}">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-sm-4 form-control-label">Tanggal Pengajuan</label>
            <div class="col-sm-8">
              <input id="txttglpengajuan" name="txttglpengajuan" type="text" placeholder="Tanggal Pengajuan" class="form-control form-control-warning" readonly value="{{$master->created_at}}">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-sm-4 form-control-label">Jenis Pengajuan</label>
            <div class="col-sm-8">
              <select name="jenis_pengajuan" id="jenis_pengajuan" class="form-control form-control-warning">
                <option value="">[ PILIH SATU ]</option>
                <option value="0" {{$master->sts_pencarian == 0 ? "selected" : ""}}>Cari Tempat OJT Sendiri</option>
                <option value="1" {{$master->sts_pencarian == 1 ? "selected" : ""}}>Tempat OJT Dicarikan Lembaga</option>
                <option value="2" {{$master->sts_pencarian == 2 ? "selected" : ""}}>TA</option> 
              </select>
              @if ($errors->has("jenis_pengajuan"))
                <small class="text-danger">{{ $errors->first("jenis_pengajuan") }}</small>
              @endif
            </div>
          </div>

          <div class="form-group row">
            <label class="col-sm-4 form-control-label"><b>Lama {{ $master->sts_pencarian == 2 ? "TA" : "OJT" }}</b></label>
            <div class="col-sm-8">
              <input id="inputHorizontalWarning" type="number" name="lamaojt"  class="form-control" style="width:15%;float:left" value="{{ $kelompokojt[0]['lama'] }}" {{ $master->sts_pencarian == 2 ? "readonly" : "" }}> &nbsp;&nbsp;Bulan <br>
              @if ($errors->has("lamaojt"))
                <small class="text-danger">{{ $errors->first("lamaojt") }}</small>
              @endif
            </div>
          </div>

          <div class="form-group row">
            <label class="col-sm-4 form-control-label">Nama Perusahaan</label>
            <div class="col-sm-8">
              <select {{ $kelompokojt[0]["sts_pencarian"] == 2 ? "disabled" : "" }} name="txtkdperusahaan" id="txtkdperusahaan" class="form-control form-control-warning">
                  <option value="">[ PILIH SATU ]</option>
                @foreach ($tbperusahaan as $tbperusahaan)
                  <option value="{{$tbperusahaan['kode_perusahaan']}}" {{ $master->kd_perusahaan == $tbperusahaan['kode_perusahaan'] ? "selected" : "" }}>{{$tbperusahaan['nama_perusahaan']}}</option>
                  @php
                      if($master->kd_perusahaan == $tbperusahaan["kode_perusahaan"]){
                        $alamatPerusahaan = $tbperusahaan["alamat"]." ".$tbperusahaan["kota"];
                      }
                  @endphp
                  @endforeach
              </select>
              @if ($kelompokojt[0]["sts_pencarian"] <> 2)
              <a href="{{url('/mahasiswa/tambah perusahaan.html')}}" class="text-info" id="btnTambahPerusahaan">+ Klik disini jika nama perusahaan yang anda inginkan tidak ada</a><br>
              @endif
              <b><div class="text-danger" id="jumlah"></div></b>
              @if ($errors->has("txtkdperusahaan"))
                <small class="text-danger">{{ $errors->first("txtkdperusahaan") }}</small>
              @endif
            </div>
          </div>

          <div class="form-group row">
            <label class="col-sm-4 form-control-label">Alamat Perusahaan</label>
            <div class="col-sm-8">
              <textarea id="txtalamatperusahaan" name="" class="form-control form-control-warning" readonly id="" cols="30" rows="2"> {{ $alamatPerusahaan }} </textarea>
              {{-- <input  name="txtalamatperusahaan" type="text" placeholder="Alamat Perusahaan" class="form-control form-control-warning" readonly value="kosong"> --}}
            </div>
          </div>

          <div class="form-group row">
            <label class="col-sm-4 form-control-label">{{ $master->sts_pencarian == 2 ? "TA" : "OJT" }} di Bulan</label>
            <div class="col-sm-8">
              <select name="txtbulan" id="txtbulan" class="form-control form-control-warning">
                  <option value="">[ PILIH SATU ]</option>
                @php
                    $no = 1;
                @endphp
                @foreach ($bulan as $bulan)
                  <option value="{{ $no }}" {{ $master->bulan == $no ? "selected" : "" }} >{{ $bulan }}</option>  
                  @php $no++; @endphp
                @endforeach
              </select>
              @if ($errors->has("txtbulan"))
              <small class="text-danger">{{ $errors->first("txtbulan") }}</small>
            @endif
            </div>
          </div>

          <div class="form-group row">
            <label class="col-sm-4 form-control-label">Status Kelompok</label>
            <div class="col-sm-8">
              <input id="txtstatus" type="text" readonly class="form-control form-control-warning text-info" value="{{$master->sts_kelompok}}">
            </div>
          </div>

        </div>
        <div class="col-lg-5">
          <br>
          <br>
          <div class="text-center text-primary">
            <strong><h1 id="nokelompokmu">{{ $master->no_kelompok }}</h1></strong>
          </div>
          <strong><div style="margin-left:70px;" class="text-uppercase"> adalah nomor kelompok anda. Harap "dicatat" dan diingat baik-baik.</div></strong>
          <br><br>
          <div class="box box-danger direct-chat direct-chat-warning pull-right" style="margin:10px;width:90%">
            <div class="box-body" style="padding:5px">
              <h4>Untuk Diperhatikan</h4>
              <b style="margin-bottom:10px;">Untuk MENCARI nama perusahaan dengan benar, Tulis nama perusahaan tanpa menyertakan jenis badan usahanya. Contoh "PT. Sejahtera Raya", tulis "Sejahtera Raya".</b> <br><br>
              <b style="margin-bottom:5px;">Pastikan alamat perusahaan yang muncul sudah sesuai dengan alamat tempat anda akan melaksanakan OJT/TA.</b> <br><br>
              <b style="margin-bottom:5px;">Pastikan semua isian telah diisi dengan benar.</b>
            </div>
          </div>
          

        </div>
      </div>
          <br>
          {{-- <hr> --}}
          {{-- <p><h4>Daftar Anggota Kelompok :</h4></p> --}}
          <button class="btn btn-primary btn-sm" id="btntambah">TAMBAH ANGGOTA</button><br><br>
          <div class="table-responsive">                       
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th width="5%">#</th>
                    <th width="15%">NIM</th>
                    <th width="40%">NAMA</th>
                    <th width="10%">KELAS</th>
                    <th width="10%">GEL</th>
                    <th width="10%"><i>AKSI</i></th>
                </tr>
                </thead>
                <tbody>
                  @php
                      $no=1;
                  @endphp
                @foreach ($detkelompokojt as $item)
                    <tr>
                      <td>{{$no}}</td>
                      <td>{{$item["NIM"]}}</td>
                      <td>{{$item["NAMA"]}}</td>
                      <td>{{$item["KELAS"]}}</td>
                      <td>{{$item["GEL_DAFTAR"]}}</td>
                      <td>
                        @if ($item["jabatan"] == "KETUA")
                        <button class="btn btn-danger btn-sm" disabled>hapus</button>
                        @else
                        <a href="{{url("/mahasiswa/hapusAnggota/".$item['NIM']."/".$kelompokojt[0]["no_kelompok"])}}" class="btn btn-danger btn-sm">hapus
                        </a>
                        @endif
                      </td>
                    </tr>
                    @php
                        $no++;
                    @endphp
                @endforeach  
                </tbody>
            </table>
          </div>
          <a href="{{ url("mahasiswa/pengajuanBatal/{$kelompokojt[0]['no_kelompok']}") }}" class="btn btn-danger btn-sm" id="btnbatal">Batalkan Pengajuan</a>
          <button type="submit" class="btn btn-primary btn-sm" id="btnsimpan">Simpan Perubahan</button>
          @if (!empty($kelompokojt[0]["kd_perusahaan"]) OR $kelompokojt[0]["sts_pencarian"] == 2)

          {{-- <a href="{{ url("mahasiswa/pengajuanCetak/{$kelompokojt[0]['no_kelompok']}") }}" class="btn btn-warning btn-sm" id="btncetak" target="_blank">Cetak Formulir Pengajuan</a>
          
          <a href="{{ url("download/+berkas+PROPOSAL OJT WEC.pdf") }}" class="btn btn-warning btn-sm" id="btncetak" target="_blank">Download Proposal Pengajuan</a> --}}

          @endif
          
        </form>
      

    </div>
    </div>
</div>
</div>

{{-- modal tambah perusahaan --}}
<div class="modal fade" id="modal_tambahperusahaan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title" id="exampleModalLabel">Tambah Data Perusahaan</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
          
            <form class="form-horizontal" method="POST" action="{{url('/mahasiswa/tambahPerusahaan')}}">
  
              {{ csrf_field() }}
              <input type="hidden" name="_method" value="POST">
              <input type="hidden" name="no_kelompok" value="{{ $kelompokojt[0]['no_kelompok'] }}">
                  <div class="form-group row">
                    <label class="col-sm-4 form-control-label">Nama Perusahaan</label>
                    <div class="col-sm-8">
                        <input id="txtnmperusahaan" name="txtnmperusahaan" type="text" placeholder="Nama Perusahaan" class="form-control form-control-success" >
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-4 form-control-label">Alamat Perusahaan</label>
                    <div class="col-sm-8">
                    <textarea name="txtalamatperusahaan" id="txtalamatperusahaan" cols="30" rows="5" class="form-control form-control-success"></textarea>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-4 form-control-label">Kota</label>
                    <div class="col-sm-8">
                        <input id="txtkota" name="txtkota" type="text" placeholder="Kota" class="form-control form-control-success" >
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-4 form-control-label">Telepon Perusahaan</label>
                    <div class="col-sm-8">
                        <input id="txttelp" name="txttelp" type="text" placeholder="Telepon Perusahaan" class="form-control form-control-success" >
                    </div>
                  </div>

                  <div class="form-group row">
                      <label class="col-sm-4 form-control-label"></label>
                      <div class="col-sm-8">
                        <button class="btn btn-primary btn-sm" type="submit">SIMPAN</button>
                      </div>
                    </div>
                  
            </form>
  
        </div>
        <div class="modal-footer">
            
    </div>
    </div>
</div>
</div>

{{-- modal tambah anggota kelompok --}}
<div class="modal fade" id="modal_tambahanggota" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Tambah Anggota Kelompok</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
          
            <form class="form-horizontal" method="POST" action="{{url('/mahasiswa/tambahAnggotaPost')}}">
  
              {{ csrf_field() }}
              <input type="hidden" name="_method" value="POST">
              <input type="hidden" name="no_kelompok" value="{{ $kelompokojt[0]['no_kelompok'] }}">
                <div class="form-group row">
                  <label class="col-sm-4 form-control-label">NIM</label>
                  <div class="col-sm-8">
                      <input id="txtnimadd" type="text" placeholder="NIM" class="form-control form-control-success" name="txtnimadd" onkeypress="return isNumber(event)">
                      <div id="load_cek" style="margin-top:8px"></div>
                      <button class="btn btn-secondary btn-sm" id="btnCekNIM" style="margin-top:8px;">CEK</button>
                  </div>
                </div>
                <div id="data_mhs">
                  <div class="form-group row">
                    <label class="col-sm-4 form-control-label">NAMA</label>
                    <div class="col-sm-8">
                        <input id="txtnamaadd" type="text" placeholder="Nama" class="form-control form-control-success" >
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-4 form-control-label">GELOMBANG</label>
                    <div class="col-sm-8">
                        <input id="txtgeladd" type="text" placeholder="Gelombang" class="form-control form-control-success" >
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-4 form-control-label">KELAS</label>
                    <div class="col-sm-8">
                        <input id="txtkelasadd" type="text" placeholder="Kelas" class="form-control form-control-success"  >
                    </div>
                  </div>
                  <div class="form-group row">
                      <label class="col-sm-4 form-control-label"></label>
                      <div class="col-sm-8">
                        <div id="feedbackAdd">
                        </div>
                      </div>
                    </div>
                  <div class="form-group row">
                    <label class="col-sm-4 form-control-label"></label>
                    <div class="col-sm-8">
                      <button type="submit" class="btn btn-primary btn-sm" id="btnSimpanAdd" >TAMBAHKAN</button>
                    </div>
                  </div>
                </div>
            </form>
  
        </div>
        <div class="modal-footer">
            
    </div>
    </div>
</div>
</div>
@else
{{-- area anggota kelompok lain --}}
<div class="row">
<div class="col-lg-12">

    <div class="box box-success direct-chat direct-chat-warning">
    <div class="box-body" style="padding:10px">
      <form class="form-horizontal">
      <div class="row">
        <div class="col-lg-7">

          <div class="form-group row">
            <label class="col-sm-4 form-control-label">No Kelompok</label>
            <div class="col-sm-8">
                <input id="txtnokelompok" type="text" placeholder="Nomor Kelompok" class="form-control form-control-success" readonly value="{{ $kelompokojt[0]['no_kelompok'] }}">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-sm-4 form-control-label">Tanggal Pengajuan</label>
            <div class="col-sm-8">
              <input id="txttglpengajuan" type="text" placeholder="Tanggal Pengajuan" class="form-control form-control-warning" readonly value="{{$kelompokojt[0]['created_at']}}">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-sm-4 form-control-label">Jenis Pengajuan</label>
            <div class="col-sm-8">
              <select name="jenis_pengajuan" id="jenis_pengajuan" class="form-control form-control-warning" readonly>
                <option value="">[ PILIH SATU ]</option>
                <option value="0" {{$kelompokojt[0]['sts_pencarian'] == 0 ? "selected" : ""}}>Cari Tempat OJT Sendiri</option>
                <option value="1" {{$kelompokojt[0]['sts_pencarian'] == 1 ? "selected" : ""}}>Tempat OJT Dicarikan Lembaga</option>
                <option value="2" {{$kelompokojt[0]['sts_pencarian'] == 2 ? "selected" : ""}}>TA</option>
              </select>
              @if ($errors->has("jenis_pengajuan"))
                <small class="text-danger">{{ $errors->first("jenis_pengajuan") }}</small>
              @endif
            </div>
          </div>

          <div class="form-group row">
            <label class="col-sm-4 form-control-label"><b>Lama OJT / TA</b></label>
            <div class="col-sm-8">
              <input id="inputHorizontalWarning" type="number" name="lamaojt"  class="form-control form-control-warning" style="width:10%;float:left" value="{{ $kelompokojt[0]['lama'] }}" readonly> &nbsp;&nbsp;Bulan <br>
              @if ($errors->has("lamaojt"))
                <small class="text-danger">{{ $errors->first("lamaojt") }}</small>
              @endif
            </div>
          </div>

          <div class="form-group row">
            <label class="col-sm-4 form-control-label">Nama Perusahaan</label>
            <div class="col-sm-8">
              <select name="txtkdperusahaan" disabled id="txtkdperusahaan" class="form-control form-control-warning">
                  <option value="">[ PILIH SATU ]</option>
                @foreach ($tbperusahaan as $tbperusahaan)

                  <option value="{{$tbperusahaan['kode_perusahaan']}}" {{ $kelompokojt[0]['kd_perusahaan'] == $tbperusahaan['kode_perusahaan'] ? "selected" : "" }}>{{$tbperusahaan['nama_perusahaan']}}</option>
                
                  @endforeach
              </select>
              {{-- <small><a href="{{url('/mahasiswa/tambah perusahaan.html')}}" class="text-info" id="btnTambahPerusahaan">Tambah Perusahaan</a></small> --}}
              @if ($errors->has("txtkdperusahaan"))
                <small class="text-danger">{{ $errors->first("txtkdperusahaan") }}</small>
              @endif
            </div>
          </div>

          <div class="form-group row">
            <label class="col-sm-4 form-control-label">OJT/TA di Bulan</label>
            <div class="col-sm-8">
              <select name="txtbulan" readonly id="txtbulan" class="form-control form-control-warning">
                  <option value="">[ PILIH SATU ]</option>
                @php
                    $no = 1;
                @endphp
                @foreach ($bulan as $bulan)
                  <option value="{{ $no }}" {{ $kelompokojt[0]["bulan"] == $no ? "selected" : "" }} >{{ $bulan }}</option>  
                  @php $no++; @endphp
                @endforeach
              </select>
              @if (Session::has("errors1"))
                  <small class="text-danger">Inputan salah</small>
              @endif
            </div>
          </div>

          <div class="form-group row">
            <label class="col-sm-4 form-control-label">Status Kelompok</label>
            <div class="col-sm-8">
              <input id="txtstatus" type="text" readonly class="form-control form-control-warning text-info" value="{{$kelompokojt[0]['sts_kelompok']}}">
            </div>
          </div>

        </div>
        <div class="col-lg-5">

        </div>
      </div>
            <br>
            <p>Daftar anggota kelompok :</p>
            <div class="table-responsive">                       
              <table class="table table-striped table-hover">
                  <thead>
                  <tr>
                      <th width="5%">#</th>
                      <th width="15%">NIM</th>
                      <th width="40%">NAMA</th>
                      <th width="10%">KELAS</th>
                      <th width="10%">GEL</th>
                  </tr>
                  </thead>
                  <tbody>
                    @php
                        $no=1;
                    @endphp
                  @foreach ($detkelompokojt as $item)
                      <tr>
                        <td>{{$no}}</td>
                        <td>{{$item["NIM"]}}</td>
                        <td>{{$item["NAMA"]}}</td>
                        <td>{{$item["KELAS"]}}</td>
                        <td>{{$item["GEL_DAFTAR"]}}</td>
                      </tr>
                      @php
                          $no++;
                      @endphp
                  @endforeach  
                  </tbody>
              </table>
            </div>

          </form>

    </div>
    </div>

</div>
</div>
@endif 
@endif
@endsection


@section('css')
<link rel="stylesheet" href="{{asset('select2/css/select2.css')}}">
@endsection

@section('script')

<script src="{{asset('select2/js/select2.js')}}"></script>
<script>
$(document).ready(function(){

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('#txtkdperusahaan').select2();

var base_url2 = $('#base_url2').val();

var sts_jadwal = "{{ session('sts_pengajuan') }}";
if(sts_jadwal == 1){
    Swal.fire({
        title: 'Konfirmasi',
        text: 'Query Berhasil Dijalankan',
        type: 'success',
        confirmButtonText: 'OK'
    });
}

$('#data_mhs').hide();

$('#termOJT').click(function(e){
    e.preventDefault();
    // alert();
    $('#modal_termOJT').modal();
});

$('#btntambah').click(function(e){
  e.preventDefault();
  $('#modal_tambahanggota').modal();
});

$('#btnCekNIM').click(function(e){
  e.preventDefault();
  var nokelompok = $("#txtnokelompok").val();
  $('#data_mhs').hide();
  $nim = $('#txtnimadd').val();
  $.ajax({
    url:base_url2+"/mahasiswa/anggota/"+$nim+"/"+nokelompok,
    type:'GET',
    beforeSend:function(){
      $('#load_cek').html("<div class='alert alert-info'>Tunggu Sebentar ...</div>");
    },
    success:function(r){
      console.log(r);
      $('#load_cek').html("");
      var view = "<ul>";
      if(r["datamhs"] == 0){
        $('#load_cek').html("<div class='alert alert-danger'>NIM ini tidak ditemukan</div>")
      }else if(r["datamhs"] == 1){
        $('#load_cek').html("<div class='alert alert-danger'>NIM ini sudah memiliki kelompok</div>")
      } if(r["datamhs"] == 2){
        $('#load_cek').html("<div class='alert alert-danger'>Tidak bisa menambah anggota lagi.</div>")
      }else{
        $('#txtnamaadd').val(r["datamhs"][0]["NAMA"]);
        $('#txtgeladd').val(r["datamhs"][0]["GEL"]);
        $('#txtkelasadd').val(r["datamhs"][0]["KELAS"]);
        
        if(r["gel"] == 0){
          view += "<li class='text-danger'><b>Tidak pada gelombang yang sama.</b></li>"; 
        }else{
          view += "<li>Berada pada gelombang yang sama.</li>";
        }
        
        if(r["jurusan"] == 0){
          view += "<li class='text-danger'><b>Tidak pada jurusan yang sama.</b></li>";
        }else{
          view += "<li>Berada pada jurusan yang sama.</li>";
        }

        if(r["absensi"] == 0){
          view += "<li class='text-danger'><b>Prosentase Ketidakhadiran >= 25%.</b></li>";
        }else{
          view += "<li>Prosentase Ketidakhadiran < 25%.</li>";
        }

        if(r["adminis"] == 0){
          view += "<li class='text-danger'><b>Belum lunas administrasi dibulan ini.</b></li>";
        }else{
          view += "<li>Sudah melunasi administrasi bulan ini.</li>";
        }

        if(r["status"] == 0){
          view += "<li class='text-danger'><b>status : Tidak bisa masuk dalam kelompok</b></li>";
          $('#btnSimpanAdd').hide();
        }else{
          view += "<li>status : Memenuhi syarat. (klik tambahkan)</li>";
          $('#btnSimpanAdd').show();
        }
        
        view += "</ul>";
        $('#feedbackAdd').html("<div class='alert alert-default'>Hasil pengecekan kami mengatakan bahwa calon anggota kelompok ini : <br><br>"+view+"</div>");
        $('#data_mhs').show();
      }
    },
    error:function(e){
      console.log(e.responseText);
    }
  });  
});

$('#btnTambahPerusahaan').click(function(e){
  e.preventDefault();
  $('#modal_tambahperusahaan').modal();
});

$('#txtkdperusahaan').change(function(){
  $.ajax({
    url : base_url2+"/mahasiswa/cekPerusahaan/"+$('#txtkdperusahaan').val()+"/"+$('#txtnokelompok').val(),
    type:'GET',
    success:function(r){
      // var result = JSON.parse(r);
      console.log(r);
      $('#jumlah').html(r["pesan"]);
      $('#txtalamatperusahaan').val(r["dataperusahaan"][0]["alamat"]+" "+r["dataperusahaan"][0]["kota"]);
    },
    error:function(e){
      console.log(e.responseText);
    }
  });
});



});

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>
@endsection