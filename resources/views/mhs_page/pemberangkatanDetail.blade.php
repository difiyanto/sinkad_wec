@extends('mhs_page.layout2')
@section('ojt', 'active')
@section('statusKelompok','active')
@section('header')
<h1>
Detail Kelompok
</h1>
<ol class="breadcrumb">
    <li><a href="{{url('/mahasiswa/index.html')}}"><i class="fa fa-home"></i> Beranda</a></li>
    <li><a href="{{url('/mahasiswa/status kelompok.html')}}"><i class="fa fa-users"></i> Status Kelompok / Pengajuan</a></li>
    <li class="active">Detail Kelompok</li>
</ol>
@endsection
@section('body')
<div class="row">
<div class="col-lg-12">

    <div class="box box-success direct-chat direct-chat-warning">
    <div class="box-body" style="padding:10px">
            <form class="form-horizontal">
                    <div class="form-group row">
                      <label class="col-sm-3 form-control-label">No Kelompok</label>
                      <div class="col-sm-6">
                          <input id="txtnokelompok" type="text" placeholder="Nomor Kelompok" class="form-control form-control-success" readonly value="{{ $kelompokojt[0]['no_kelompok'] }}">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-3 form-control-label">Tanggal Pengajuan</label>
                      <div class="col-sm-6">
                        <input id="txttglpengajuan" type="text" placeholder="Tanggal Pengajuan" class="form-control form-control-warning" readonly value="{{$kelompokojt[0]['created_at']}}">
                      </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Jenis Pengajuan</label>
                        <div class="col-sm-6">
                          <select name="jenis_pengajuan" id="jenis_pengajuan" class="form-control form-control-warning" readonly>
                            <option value="">[ PILIH SATU ]</option>
                            <option value="0" {{$kelompokojt[0]['sts_pencarian'] == 0 ? "selected" : ""}}>Cari Tempat OJT Sendiri</option>
                            <option value="1" {{$kelompokojt[0]['sts_pencarian'] == 1 ? "selected" : ""}}>Tempat OJT Dicarikan Lembaga</option>
                            <option value="2" {{$kelompokojt[0]['sts_pencarian'] == 2 ? "selected" : ""}}>TA</option>
                          </select>
                          @if ($errors->has("jenis_pengajuan"))
                            <small class="text-danger">{{ $errors->first("jenis_pengajuan") }}</small>
                          @endif
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-sm-3 form-control-label"><b>Lama OJT / TA</b></label>
                        <div class="col-sm-9">
                          <input id="inputHorizontalWarning" type="number" name="lamaojt"  class="form-control form-control-warning" style="width:10%;float:left" value="{{ $kelompokojt[0]['lama'] }}" readonly> &nbsp;&nbsp;Bulan <br>
                          @if ($errors->has("lamaojt"))
                            <small class="text-danger">{{ $errors->first("lamaojt") }}</small>
                          @endif
                        </div>
                      </div>
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Nama Perusahaan</label>
                        <div class="col-sm-6">
                          <select name="txtkdperusahaan" disabled id="txtkdperusahaan" class="form-control form-control-warning">
                              <option value="">[ PILIH SATU ]</option>
                              @php
                              $alamatperusahaan = "test";

                              @endphp
                            @foreach ($tbperusahaan as $tbperusahaan)
            
                              <option value="{{$tbperusahaan['kode_perusahaan']}}" {{ $kelompokojt[0]['kd_perusahaan'] == $tbperusahaan['kode_perusahaan'] ? "selected" : "" }}>{{$tbperusahaan['nama_perusahaan']}}</option>
                               
                              @php
                                  if($kelompokojt[0]["kd_perusahaan"] == $tbperusahaan["kode_perusahaan"]){
                                      $alamatperusahaan = $tbperusahaan["alamat"]." ".$tbperusahaan["kota"];
                                    }
                              @endphp

                              @endforeach
                          </select>
                          {{-- <small><a href="{{url('/mahasiswa/tambah perusahaan.html')}}" class="text-info" id="btnTambahPerusahaan">Tambah Perusahaan</a></small> --}}
                          @if ($errors->has("txtkdperusahaan"))
                            <small class="text-danger">{{ $errors->first("txtkdperusahaan") }}</small>
                          @endif
                        </div>
                      </div>

                      <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Alamat Perusahaan</label>
                        <div class="col-sm-6">
                          <textarea readonly class="form-control form-control-warning" cols="10" rows="5"> {{ $alamatperusahaan }}</textarea>
                          <!-- <input id="txtalamatperusahaanx"  type="text" placeholder="Tanggal Pengajuan" class="form-control form-control-warning" readonly value="{{$alamatperusahaan}}"> -->
                        </div>
                      </div>
                      
                      <div class="form-group row">
                        <label class="col-sm-3 form-control-label">OJT di Bulan</label>
                        <div class="col-sm-6">
                          <select name="txtbulan" readonly id="txtbulan" class="form-control form-control-warning">
                              <option value="">[ PILIH SATU ]</option>
                            @php
                                $no = 1;
                            @endphp
                            @foreach ($bulan as $bulan)
                              <option value="{{ $no }}" {{ $kelompokojt[0]["bulan"] == $no ? "selected" : "" }} >{{ $bulan }}</option>  
                              @php $no++; @endphp
                            @endforeach
                          </select>
                          @if (Session::has("errors1"))
                              <small class="text-danger">Inputan salah</small>
                          @endif
                        </div>
                      </div>
            
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Pembimbing 1</label>
                        <div class="col-sm-6">
                          @foreach ($tbpegawai as $itm)
                              @if ($itm->NIP == $kelompokojt[0]["nip"])
                                <input type="text" readonly class="form-control" value="{{ $itm->nama }}">
                              @endif
                          @endforeach
                        </div>
                      </div>

                      <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Pembimbing 2</label>
                        <div class="col-sm-6">
                          @foreach ($tbpegawai as $itm)
                              @if ($itm->NIP == $kelompokojt[0]["nip2"])
                                <input type="text" readonly class="form-control" value="{{ $itm->nama }}">
                              @endif
                          @endforeach
                        </div>
                      </div>


                    <br>
                    <p>Daftar anggota kelompok :</p>
                    <div class="table-responsive">                       
                      <table class="table table-striped table-hover">
                          <thead>
                          <tr>
                              <th width="5%">#</th>
                              <th width="15%">NIM</th>
                              <th width="40%">NAMA</th>
                              <th width="10%">KELAS</th>
                              <th width="10%">GEL</th>
                          </tr>
                          </thead>
                          <tbody>
                            @php
                                $no=1;
                            @endphp
                          @foreach ($detkelompokojt as $item)
                              <tr>
                                <td>{{$no}}</td>
                                <td>{{$item["NIM"]}}</td>
                                <td>{{$item["NAMA"]}}</td>
                                <td>{{$item["KELAS"]}}</td>
                                <td>{{$item["GEL"]}}</td>
                              </tr>
                              @php
                                  $no++;
                              @endphp
                          @endforeach  
                          </tbody>
                      </table>
                    </div>
        
                  </form>
        
        
    </div>
    </div>

</div>
</div>
@endsection