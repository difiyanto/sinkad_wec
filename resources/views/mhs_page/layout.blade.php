<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sistem Informasi Akademik</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="{{asset('admin_temp/vendor/bootstrap/css/bootstrap.min.css')}}">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="{{asset('admin_temp/vendor/font-awesome/css/font-awesome.min.css')}}">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="{{asset('admin_temp/css/fontastic.css')}}">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="{{asset('admin_temp/css/style.default.css')}}" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="{{asset('admin_temp/css/custom.css')}}">
    <!-- Favicon-->
    <link rel="shortcut icon" href="{{asset('admin_temp/img/head logo.png')}}">
    @yield('css')
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    {{-- loader --}}
    
  </head>
  <body>
  <input type="hidden" name="" id="base_url2" value="{{url('/')}}">
    <div class="page">
      <!-- Main Navbar-->
      <header class="header">
        <nav class="navbar">
          <!-- Search Box-->
          <div class="search-box">
            <button class="dismiss"><i class="icon-close"></i></button>
            <form id="searchForm" action="#" role="search">
              <input type="search" placeholder="What are you looking for..." class="form-control">
            </form>
          </div>
          <div class="container-fluid">
            <div class="navbar-holder d-flex align-items-center justify-content-between">
              <!-- Navbar Header-->
              <div class="navbar-header">
                <!-- Navbar Brand --><a href="index.html" class="navbar-brand d-none d-sm-inline-block">
                  <div class="brand-text d-none d-lg-inline-block"><span>SINKAD</span><strong> - {{session('mhs')}}</strong></div>
                  <div class="brand-text d-none d-sm-inline-block d-lg-none"><strong>SINKAD</strong></div></a>
                <!-- Toggle Button--><a id="toggle-btn" href="#" class="menu-btn active"><span></span><span></span><span></span></a>
              </div>
              <!-- Navbar Menu -->
              <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
              
                <!-- Logout    -->
                <li class="nav-item"><a href="{{url('/logout')}}" class="nav-link logout"> <span class="d-none d-sm-inline">Logout</span><i class="fa fa-sign-out"></i></a></li>
              </ul>
            </div>
          </div>
        </nav>
      </header>
      <div class="page-content d-flex align-items-stretch"> 
        <!-- Side Navbar -->
        <nav class="side-navbar">
          <!-- Sidebar Header-->
          <div class="sidebar-header d-flex align-items-center">
            <div class="avatar"><img src="{{asset('storage/app/img_akun/user.png')}}" alt="..." class="img-fluid rounded-circle"></div>
            <div class="title">
              <h1 class="h4">{{ session("nama_mhs") }}</h1>
              <p>{{ session("kelas") }}</p>
            </div>
          </div>
          <!-- Sidebar Navidation Menus--><span class="heading">Main</span>
          <ul class="list-unstyled">
                    <li class="@yield('index')"><a href="{{url('mahasiswa/index.html')}}"> <i class="icon-home"></i>Beranda</a></li>
                    <li class="@yield('nilaikelas')"><a href="{{url('mahasiswa/nilaikelas.html')}}"> <i class="fa fa-tasks"></i>Rata-rata Nilai </a></li>
                    <li class="@yield('absensi')"><a href="{{url('mahasiswa/absensi.html')}}"> <i class="icon-grid"></i>Absensi </a></li>
                    <li class="@yield('angsuran')"><a href="{{url('mahasiswa/angsuran.html')}}"> <i class="fa fa-money"></i>Angsuran </a></li>
                    <li class="@yield('uploadKeg')"><a href="{{url('mahasiswa/kegiatan.html')}}"> <i class="fa fa-users"></i>Upload Kegiatan Kelas </a></li>
                    @if (Session::get("ecc") == 1)
                    <li class="@yield('ecc')"><a href="{{url('mahasiswa/ecc.html')}}"> <i class="fa fa-users"></i>E C C</a></li>
                    @endif
                    {{-- <li class="@yield('devcom')"><a href="{{url('mahasiswa/devcom.html')}}"> <i class="fa fa-code"></i>Pendaftaran Devcom </a></li> --}}
                    <li class="@yield('pengaturan')"><a href="{{url('mahasiswa/pengaturan.html')}}"> <i class="fa fa-cogs"></i>Pengaturan </a></li>
                    <li><a href="#exampledropdownDropdown" aria-expanded="false" data-toggle="collapse"> <i class="icon-interface-windows"></i>OJT </a>
                      <ul id="exampledropdownDropdown" class="collapse list-unstyled ">
                        <li class="@yield('pengajuan')"><a href="{{url('mahasiswa/pengajuan.html')}}">Pengajuan Kelompok</a></li>
                        <li class="@yield('statusKelompok')"><a href="{{url('mahasiswa/status kelompok.html')}}">Status Kelompok</a></li>
                      </ul>
                    </li>
                    <li><a href="{{url('/')}}"> <i class="fa fa-reply"></i>Landing Page </a></li>
          </ul>
        </nav>
        <div class="content-inner">
          <!-- Page Header-->
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">@yield('halaman')</h2>
            </div>
          </header>
          
          {{-- kontent --}}
          @yield('konten')
          
          <!-- Page Footer-->
          <footer class="main-footer">
            <div class="container-fluid">
              <div class="row">
                <div class="col-sm-6">
                  <p>Wearnes Education Center | 2019</p>
                </div>
                <div class="col-sm-6 text-right">
                  <p> Tim Asdos WECMLG - <a href="https://bootstrapious.com/admin-templates" class="external">Bootstrapious</a></p>
                  <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
                </div>
              </div>
            </div>
          </footer>
        </div>
      </div>
    </div>

<!-- Modal -->
<div class="modal fade" id="confirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Konfirmasi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h2 class="text-danger">Under Maintanance</h2>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>




    <!-- JavaScript files-->
    <script src="{{asset('admin_temp/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('admin_temp/vendor/popper.js/umd/popper.min.js')}}"> </script>
    <script src="{{asset('admin_temp/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('admin_temp/vendor/jquery.cookie/jquery.cookie.js')}}"> </script>
    <script src="{{asset('admin_temp/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
    
    <!-- Main File-->
    <script src="{{asset('admin_temp/js/front.js')}}"></script>
    <script>
    $(document).ready(function(){
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });


      var base_url2 = $('#base_url2').val();
      $('#txtpwdBaru').hide();
      $('#txtpwdBaruKonfir').hide();
      $('#e').hide()
      $('#simpanpwdbaru').hide();
      $('#pilkelas').hide();
      $('#pilmatkul').hide();
      $('#submit').hide();
      $('#cover_nilai').hide();
      $('#cover_rata').hide();
      $('#txtpwdLama').keyup(function(e){
        if(e.keyCode == 13){
          $.ajax({
          url : base_url2+"/mahasiswa/gantipwd",
          type:'POST',
          data:{pwdlama : $(this).val()},
          success:function(result){
            // alert(result);
            if(result == 1){
              $('#txtpwdBaru').show();
              $('#txtpwdBaruKonfir').show();
              $('#e').show();
              $('#f').hide();
              $('#txtpwdBaru').focus();
            }else{
              $('#feedback-gantipwd').html("<div class='alert alert-danger'>* Password Salah !</div>");
              setInterval(function(){ 
                $('#feedback-gantipwd').html("");
               }, 1000);
            }
          },
          error:function(result){
            alert('Terjadi Kesalahan !');
            console.log(result.responseText);
          }
        });
        }
      });

      $('#txtpwdBaruKonfir').keyup(function(e){
        if(e.keyCode == 13){

        var pwdbaru = $('#txtpwdBaru').val();
        var pwdbarukonfir = $('#txtpwdBaruKonfir').val();
        if(pwdbaru != pwdbarukonfir){
          $('#feedback-gantipwd').html("<div class='alert alert-danger'>* Password Tidak Sama !</div>");
           setInterval(function(){ 
                $('#feedback-gantipwd').html("");
               }, 3000);
        }else{
          $.ajax({
            url:base_url2+'/mahasiswa/gantipwd2',
            type:'POST',
            data:{pwdBaru:$('#txtpwdBaru').val()},
            beforeSend:function(){
              $('#feedback-gantipwd').html("<div class='alert alert-primary'>* Tunggu Sebentar ...</div>");
            },
            success:function(result){
              if(result == 1){
                $('#feedback-gantipwd').html("<div class='alert alert-primary'>* Password berhasil diubah</div>");
                setInterval(function(){ 
                window.location.href = base_url2+"/mahasiswa/pengaturan.html";
               }, 3000);
              }else{
                $('#feedback-gantipwd').html("<div class='alert alert-danger'>* Error !</div>");
              }
            },
            error:function(result){
              alert('Terjadi Kesalahan !');
              console.log(result.responseText);
            }
          });
        }
        }
      })

      $('#simpanpwdbaru').click(function(){
        var pwdbaru = $('#txtpwdBaru').val();
        var pwdbarukonfir = $('#txtpwdBaruKonfir').val();
        if(pwdbaru != pwdbarukonfir){
          $('#feedback-gantipwd').html("<div class='alert alert-danger'>* Konfirmasi Password Tidak Sama !</div>");
           setInterval(function(){ 
                $('#feedback-gantipwd').html("");
               }, 1000);
        }else{
          $.ajax({
            url:base_url2+'/mahasiswa/gantipwd2',
            type:'POST',
            data:{pwdBaru:$('#txtpwdBaru').val()},
            beforeSend:function(){
              $('#feedback-gantipwd').html("<div class='alert alert-primary'>* Tunggu Sebentar ...</div>");
            },
            success:function(result){
              if(result == 1){
                $('#feedback-gantipwd').html("<div class='alert alert-primary'>* Password berhasil diubah</div>");
                setInterval(function(){ 
                window.location.href = base_url2+"/mahasiswa/pengaturan.html";
               }, 4000);
              }else{
                $('#feedback-gantipwd').html("<div class='alert alert-danger'>* Error !</div>");
              }
            },
            error:function(result){
              alert('Terjadi Kesalahan !');
              console.log(result.responseText);
            }
          })
        }
      });


      $('#slcJurusan').on('change', function(){
        var jurusan = $(this).val();
        $.ajax({
          url:base_url2+'/mahasiswa/cariKelasByJurusan/'+jurusan,
          type:'GET',
          success:function(result){
            var Arrresult =  JSON.parse(result);
            $('.opt_kelas').html(Arrresult.kelas);
            $('.opt_matkul').html(Arrresult.matkul);
            $('#pilkelas').show();
            $('#pilmatkul').show();
            $('#submit').show();
          },
          error:function(result){
            alert("Terjadi Kesalahan !");
            console.log(result.responseText);
          }
        });
      });

      $('#btnSubmit').click(function(){
        var kelas = $('#slcKelas').val();
        var jurusan = $('#slcJurusan').val();
        var matkul = $('#slcMatkul').val();
        $.ajax({
          url : base_url2+"/mahasiswa/nilaiperkelas",
          type:'POST',
          data:{kelas:kelas, jurusan:jurusan, matkul:matkul},
          beforeSend:function(){
            $('.fbnilaikelas').html("<p class='text-primary'>Tunggu Sebentar ... </p>")
          },
          success:function(result){
            var res = JSON.parse(result);
            $('.fbnilaikelas').html("");
            // $('#datanilai').html(res['tbnilai']);
            // $('#cover_nilai').show();
            $('#datarata').html(res['ratarata']);
            $('#cover_rata').show();
          },
          error:function(result){
            alert('Terjadi Kesalahan !');
            console.log(result.responseText);
          }
        });
      });

    
    });
    </script>

    @yield('script')

  </body>
</html>