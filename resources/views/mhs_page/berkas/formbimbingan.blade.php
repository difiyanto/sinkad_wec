<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Form Bimbingan</title>
    <style>
    body{
        font-family: Arial, Helvetica, sans-serif;
        font-size: 14px;
    }

    #anggota td, th {
        border:1px solid black;
        padding-left: 5px;
        height: 18px;
    }

    #bimbingan td, th {
        border:1px solid black;
        padding-left: 5px;
        height: 21px;
    }
    </style>
</head>
<body>
<div style="text-align:center"><h3>KARTU KENDALI BIMBINGAN {{ $kelompok[0]["sts_pencarian"] == 2 ? "TA" : "OJT" }}</h3></div>
* Data Kelompok
<table style="width:100%">
    <tr>
        <td style="width:25%">No Kelompok</td>
        <td style="width:2%">:</td>
        <td style="width:73%">{{$kelompok[0]["no_kelompok"]}}</td>
    </tr>
    <tr>
        <td style="vertical-align: top">Dosen Pembimbing</td>
        <td style="vertical-align: top">:</td>
        <td><ol>
                @for ($i = 0; $i < count($pembimbing); $i++)
                <li style="text-transform:uppercase">{{ $pembimbing[$i]["nama"] }}</li>   
                @endfor
            </ol>
        </td>
    </tr>
    <tr>
        <td style="width:25%">Bulan</td>
        <td style="width:2%">:</td>
        <td>{{date("F", strtotime($kelompok[0]["bulan"]."/01/2019"))}}</td>
    </tr>
    <tr>
        <td style="width:25%">Nama Perusahaan</td>
        <td style="width:2%">:</td>
        <td>{{$kelompok[0]["nama_perusahaan"]}}</td>
    </tr>
    <tr>
        <td style="width:25%;vertical-align:top">Judul Laporan</td>
        <td style="width:2%;vertical-align:top">:</td>
        <td style="width:73%">______________________________________________________________________ <br><br> ______________________________________________________________________</td>
    </tr>
</table>

* Anggota Kelompok :
<table style="width:100%;" id="anggota" class="table table-striped table-bordered" cellspacing="0">
    <tr>
        <th style="width:5%">NO</th>
        <th style="width:15%">NIM</th>
        <th style="width:70%">NAMA MAHASISWA</th>
        <th>KELAS</th>
    </tr>
    @php
        $no = 1;
    @endphp
    @for ($i = 0; $i < count($kelompok); $i++)
    <tr>
        <td>{{$no}}</td>
        <td>{{$kelompok[$i]["NIM"]}}</td>
        <td>{{$kelompok[$i]["NAMA"]}}</td>
        <td>{{$kelompok[$i]["KELAS"]}}</td>
    </tr>
    @php
        $no++;
    @endphp
    @endfor
    @php
        $sisarow = 5 - count($kelompok);
    @endphp
    @if ($sisarow <> 0)
    @for ($i = 0; $i < $sisarow; $i++)
    <tr>
        <td>{{$no}}</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    @php
        $no++;
    @endphp
    @endfor
    @endif
</table>
<br>
*Bimbingan
<table style="width:100%" cellspacing="0" id="bimbingan">
    <tr>
        <th style="width:5%">NO</th>
        <th style="width:10%">TANGGAL</th>
        <th style="width:50%">KETERANGAN</th>
        <th style="width:20%">TANDA TANGAN</th>
    </tr>
    @for ($i = 0; $i < 18; $i++)
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    @endfor
</table>

<p>
    Setelah melalui proses bimbingan, maka terhadap kelompok mahasiswa tersebut diatas diajukan untuk mengikuti ujian komprehensif pada tanggal _____________________
</p>

<table style="width:100%">
    <tr>
        <td style="width:60%">Malang, </td>
        <td></td>
    </tr>
    <tr>
        <td>Dosen Pembimbing I</td>
        <td>{{ $kelompok[0]["nip2"] <> "" ? "Pembimbing II" : ""}}</td>
    </tr>
    <tr>
        <td style="height:60px;"></td>
        <td></td>
    </tr>
    <tr>
        <td style="text-transform:uppercase"> <u>{{ $pembimbing[0]["nama"] }}</u></td>
        <td style="text-transform:uppercase">{!! $kelompok[0]["nip2"] <> "" ? "<u>".$pembimbing[1]["nama"]."</u>" : "" !!}</td>
    </tr>
</table>


</body>
</html>