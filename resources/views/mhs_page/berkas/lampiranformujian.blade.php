<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Form Ujian Komprehensif</title>
    <style>
    body{
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13px;
    }


    .matkul td, th {
        border:1px solid black;
        padding-left: 5px;
        font-size: 13px;
    }

    .anggota{
        border:2px solid black;
        width:48%;
        height:280px;
        float:left;
        margin-right:2px;
        margin-bottom:5px;
        font-size: 14px;
        padding:5px;
    }

    .anggota td, th {
        border:1px solid black;
        padding-left: 5px;
        height: 20px;
    }

    .anggota, table{
        font-size: 13px;
    }

    .ket{
        height: 5px;
        width: 5px;
        border: 1px solid black;
    }


    </style>
</head>
<body>
Lampiran Form Ujian Komprehensif
@for ($i = 0; $i < count($kelompok); $i++)
<div class="anggota">
<p style="text-align:center">
    <strong><u>CEK NILAI PESERTA UJIAN KOMPHRE</u></strong>
</p>
<table id="dtanggota" style="border:0;">
    <tr>
        <td style="border:0;">NIM</td>
        <td style="border:0;">:</td>
        <td style="border:0;">{{$kelompok[$i]["NIM"]}}</td>
    </tr>
    <tr>
        <td style="border:0;">NAMA</td>
        <td style="border:0;">:</td>
        <td style="border:0;">{{$kelompok[$i]["NAMA"]}}</td>
    </tr>
    <tr>
        <td style="border:0;">KELAS</td>
        <td style="border:0;">:</td>
        <td style="border:0;">{{$kelompok[$i]["KELAS"]}}</td>
    </tr>
</table>
<br>
<table style="margin-bottom:3px;">
    <tr>
        <td style="border:0;">Lulus <input type="checkbox" name="" id=""> m.k</td>
        <td style="width:20%;border:0"></td>
        <td style="border:0;">Mengulang <input type="checkbox" name="" id=""> m.k</td>
    </tr>
</table>

<span>Data mata kuliah yang mengulang (nilai < 60):</span>
<table id="matkul" cellspacing="0" style="float:left">
    <tr>
        <td>No</td>
        <td style="width:35%">Mata Kuliah</td>
        <td>Nilai</td>
        <td style="width:5%;border:0;"></td>
        <td>No</td>
        <td style="width:35%">Mata Kuliah</td>
        <td>Nilai</td>
    </tr>
    <tr><td></td><td></td><td></td><td style="border:0"></td><td></td><td></td><td></td></tr>
    <tr><td></td><td></td><td></td><td style="border:0"></td><td></td><td></td><td></td></tr>
    <tr><td></td><td></td><td></td><td style="border:0"></td><td></td><td></td><td></td></tr>
    <tr><td></td><td></td><td></td><td style="border:0"></td><td></td><td></td><td></td></tr>
    <tr><td></td><td></td><td></td><td style="border:0"></td><td></td><td></td><td></td></tr>
    <tr><td></td><td></td><td></td><td style="border:0"></td><td></td><td></td><td></td></tr>
    
</table>
</div>
@endfor
@php
    $sisarow = 5 - count($kelompok);
@endphp
@for ($i = 0; $i < $sisarow; $i++)
<div class="anggota">
<p style="text-align:center">
    <strong><u>CEK NILAI PESERTA UJIAN KOMPHRE</u></strong>
</p>
<table id="dtanggota" style="border:0;">
    <tr>
        <td style="border:0;">NIM</td>
        <td style="border:0;">:</td>
        <td style="border:0;"></td>
    </tr>
    <tr>
        <td style="border:0;">NAMA</td>
        <td style="border:0;">:</td>
        <td style="border:0;"></td>
    </tr>
    <tr>
        <td style="border:0;">KELAS</td>
        <td style="border:0;">:</td>
        <td style="border:0;"></td>
    </tr>
</table>
<br>
<table style="margin-bottom:3px;">
    <tr>
        <td style="border:0;">Lulus <input type="checkbox" name="" id=""> m.k</td>
        <td style="width:20%;border:0"></td>
        <td style="border:0;">Mengulang <input type="checkbox" name="" id=""> m.k</td>
    </tr>
</table>

<span>Data mata kuliah yang mengulang (nilai < 60):</span>
<table id="matkul" cellspacing="0" style="float:left;">
    <tr>
        <td>No</td>
        <td style="width:35%">Mata Kuliah</td>
        <td>Nilai</td>
        <td style="width:5%;border:0;"></td>
        <td>No</td>
        <td style="width:35%">Mata Kuliah</td>
        <td>Nilai</td>
    </tr>
    <tr><td></td><td></td><td></td><td style="border:0"></td><td></td><td></td><td></td></tr>
    <tr><td></td><td></td><td></td><td style="border:0"></td><td></td><td></td><td></td></tr>
    <tr><td></td><td></td><td></td><td style="border:0"></td><td></td><td></td><td></td></tr>
    <tr><td></td><td></td><td></td><td style="border:0"></td><td></td><td></td><td></td></tr>
    <tr><td></td><td></td><td></td><td style="border:0"></td><td></td><td></td><td></td></tr>
    <tr><td></td><td></td><td></td><td style="border:0"></td><td></td><td></td><td></td></tr>
    
</table>
</div>
@endfor
</body>
</html>