<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Form Ujian Komprehensif</title>
    <style>
    body{
        font-family: Arial, Helvetica, sans-serif;
        font-size: 15px;
    }

    #anggota td, th {
        border:1px solid black;
        padding-left: 5px;
    }

    #bimbingan td, th {
        border:1px solid black;
        padding-left: 5px;
        height: 18px;
    }

    #penguji td, th {
        /* border:1px solid black; */
        padding-left: 5px;
        height: 30px;
    }

    #ttdkeins{
        top:100;
        left:580;
        width:150;
        border: 1px solid black;
        position:absolute;
        /* padding-top:80px; */
        overflow:auto; 
        text-align: center;
    }

    #catatan, ul, li{
        font-size: 14px;
    }

    #potong{
        text-align: center;
    }


   

    </style>
</head>
<body>

{{-- <div id="ttdkeins">
<br><br><br>
Paraf Keinstrukturan
</div> --}}

<div style="text-align:center"><h3>FORM UJIAN KOMPREHENSIF</h3></div>
* Data Kelompok
<table style="width:100%">
    <tr>
        <td style="width:25%">Tipe Kelompok</td>
        <td style="width:2%">:</td>
        <td style="width:73%">{{$kelompok[0]["sts_pencarian"] == 2 ? "TA" : "OJT"}}</td>
    </tr>
    <tr>
        <td style="width:25%">No Kelompok</td>
        <td style="width:2%">:</td>
        <td style="width:73%">{{$kelompok[0]["no_kelompok"]}}</td>
    </tr>
    <tr>
        <td style="vertical-align: top">Dosen Pembimbing</td>
        <td style="vertical-align: top">:</td>
        <td><ol>
                <li>{{ $pembimbing1[0]["nama"] }}</li>  
                @if ($kelompok[0]["nip2"] <> "")
                <li> {{ $pembimbing2[0]["nama"] }} </li>
                @endif 
            </ol>
        </td>
    </tr>
    <tr>
        <td style="width:25%">Bulan</td>
        <td style="width:2%">:</td>
        <td>{{date("F", strtotime($kelompok[0]["bulan"]."/01/2019"))}}</td>
    </tr>
    <tr>
        <td style="width:25%">Nama Perusahaan</td>
        <td style="width:2%">:</td>
        <td>{{$kelompok[0]["nama_perusahaan"]}}</td>
    </tr>
    <tr>
        <td style="width:25%;vertical-align:top">Judul Laporan</td>
        <td style="width:2%;vertical-align:top">:</td>
        <td style="width:73%">______________________________________________________________________ <br><br> ______________________________________________________________________</td>
    </tr>
</table>
<br>
* Anggota Kelompok :
<table style="width:100%;" id="anggota" class="table table-striped table-bordered" cellspacing="0">
    <tr>
        <th style="width:5%">NO</th>
        <th style="width:15%">NIM</th>
        <th style="width:60%">NAMA MAHASISWA</th>
        <th>KELAS</th>
        <th>NILAI</th>
    </tr>
    @php
        $no = 1;
    @endphp
    @for ($i = 0; $i < count($kelompok); $i++)
    <tr>
        <td>{{$no}}</td>
        <td>{{$kelompok[$i]["NIM"]}}</td>
        <td>{{$kelompok[$i]["NAMA"]}}</td>
        <td>{{$kelompok[$i]["KELAS"]}}</td>
        <td></td>
    </tr>
    @php
        $no++;
    @endphp
    @endfor
    @php
        $sisarow = 5 - count($kelompok);
    @endphp
    @if ($sisarow <> 0)
    @for ($i = 0; $i < $sisarow; $i++)
    <tr>
        <td>{{$no}}</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    @php
        $no++;
    @endphp
    @endfor
    @endif
</table>
<span>
Ujian komprehensif dilaksanakan pada tanggal ________________________
</span>
<table style="width:100%">
    <tr>
        <td>Dosen Pembimbing I</td>
        <td>{{ $kelompok[0]["nip2"] <> "" ? "Pembimbing II" : ""}}</td>
        
    </tr>
    <tr>
        <td style="height:50px"></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><u>{{$pembimbing1[0]["nama"]}}</u> </td>
        <td><u>{!! $kelompok[0]["nip2"] <> "" ? "<u>".$pembimbing2[0]["nama"]."</u>" : "" !!}</u></td>
        
    </tr>
</table>
<br>
*Dosen Penguji
<table id="penguji">
    <tr>
        <td>01.</td>
        <td>________________________</td>
        <td style="width:20%"></td>
        <td>__________________</td>
    </tr>
    <tr>
        <td>02.</td>
        <td>________________________</td>
        <td style="width:20%"></td>
        <td>__________________</td>
    </tr>
    <tr>
        <td>03.</td>
        <td>________________________</td>
        <td style="width:20%"></td>
        <td>__________________</td>
    </tr>
</table>
<br>
*Catatan
<div class="catatan">
Mahasiswa diperbolehkan mengikuti ujian komprehensif setelah mendapatkan tanda tangan dari Dosen Pembimbing I/II dan form ini diperlihatkan dan diserahkan kepada dosen penguji bersama dengan KTM masing-masing Mahasiswa.
</div>

<pagebreak />
<div style="text-align:center"><h3>FORM UJIAN ULANG KOMPREHENSIF</h3></div>
* Data Kelompok
<table style="width:100%">
    <tr>
        <td style="width:25%">Tipe Kelompok</td>
        <td style="width:2%">:</td>
        <td style="width:73%">{{$kelompok[0]["sts_pencarian"] == 2 ? "TA" : "OJT"}}</td>
    </tr>
    <tr>
        <td style="width:25%">No Kelompok</td>
        <td style="width:2%">:</td>
        <td style="width:73%">{{$kelompok[0]["no_kelompok"]}}</td>
    </tr>
    <tr>
        <td style="vertical-align: top">Dosen Pembimbing</td>
        <td style="vertical-align: top">:</td>
        <td><ol>
                <li>{{ $pembimbing1[0]["nama"] }}</li>  
                @if ($kelompok[0]["nip2"] <> "")
                <li> {{ $pembimbing2[0]["nama"] }} </li>
                @endif 
            </ol>
        </td>
    </tr>
    <tr>
        <td style="width:25%">Bulan</td>
        <td style="width:2%">:</td>
        <td>{{date("F", strtotime($kelompok[0]["bulan"]."/01/2019"))}}</td>
    </tr>
    <tr>
        <td style="width:25%">Nama Perusahaan</td>
        <td style="width:2%">:</td>
        <td>{{$kelompok[0]["nama_perusahaan"]}}</td>
    </tr>
    <tr>
        <td style="width:25%;vertical-align:top">Judul Laporan</td>
        <td style="width:2%;vertical-align:top">:</td>
        <td style="width:73%">______________________________________________________________________ <br><br> ______________________________________________________________________</td>
    </tr>
</table>
<br>
* Anggota Kelompok :
<table style="width:100%;" id="anggota" class="table table-striped table-bordered" cellspacing="0">
    <tr>
        <th style="width:5%">NO</th>
        <th style="width:15%">NIM</th>
        <th style="width:60%">NAMA MAHASISWA</th>
        <th>KELAS</th>
        <th>NILAI</th>
    </tr>
    @php
        $no = 1;
    @endphp
    @for ($i = 0; $i < count($kelompok); $i++)
    <tr>
        <td>{{$no}}</td>
        <td>{{$kelompok[$i]["NIM"]}}</td>
        <td>{{$kelompok[$i]["NAMA"]}}</td>
        <td>{{$kelompok[$i]["KELAS"]}}</td>
        <td></td>
    </tr>
    @php
        $no++;
    @endphp
    @endfor
    @php
        $sisarow = 5 - count($kelompok);
    @endphp
    @if ($sisarow <> 0)
    @for ($i = 0; $i < $sisarow; $i++)
    <tr>
        <td>{{$no}}</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    @php
        $no++;
    @endphp
    @endfor
    @endif
</table>
<span>
Ujian ulang komprehensif dilaksanakan pada tanggal ________________________
</span>
<br>
*Dosen Penguji
<table id="penguji">
    <tr>
        <td>01.</td>
        <td>________________________</td>
        <td style="width:20%"></td>
        <td>__________________</td>
    </tr>
    <tr>
        <td>02.</td>
        <td>________________________</td>
        <td style="width:20%"></td>
        <td>__________________</td>
    </tr>
    <tr>
        <td>03.</td>
        <td>________________________</td>
        <td style="width:20%"></td>
        <td>__________________</td>
    </tr>
</table>
<br>
*Catatan
<div class="catatan" style="text-align: justify">
Form dibawa oleh salah satu dosen penguji, dan dipergunakan sebagai bukti Ujian Ulang. Form harus ditanda tangani oleh dosen penguji setelah diadakan ujian ulang komprehensif serta selanjutnya segera diserahkan ke TIM OJT.
</div>
</body>
</html>