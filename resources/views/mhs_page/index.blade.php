@extends('mhs_page.layout2')
@section('index','active')
@section('header')
<h1>
Beranda
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-home"></i> Beranda</a></li>
    <li class="active">Beranda</li>
</ol>
@endsection 
@section('body')
<div class="row">
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
      <div class="small-box bg-aqua">
        <div class="inner">
          <h3>{{ $jml_matkul }}</h3>
          <p>Total Mata Kuliah</p>
        </div>
        <div class="icon">
          <i class="ion ion-pie-graph"></i>
        </div>
        <a href="#" class="small-box-footer"></a>
      </div>
    </div>

    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
      <div class="small-box bg-green">
        <div class="inner">
          <h3>{{ $sudah }}</h3>
          <p>Sudah Ditempuh</p>
        </div>
        <div class="icon">
          <i class="ion ion-pie-graph"></i>
        </div>
        <a href="#" class="small-box-footer"></a>
      </div>
    </div>

    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3>{{ $lulus }}</h3>
          <p>Ujian Lulus</p>
        </div>
        <div class="icon">
          <i class="ion ion-pie-graph"></i>
        </div>
        <a href="#" class="small-box-footer"></a>
      </div>
    </div>

    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
      <div class="small-box bg-red">
        <div class="inner">
          <h3>{{ $ulang }}</h3>
          <p>Ujian Belum Lulus</p>
        </div>
        <div class="icon">
          <i class="ion ion-pie-graph"></i>
        </div>
        <a href="#" class="small-box-footer"></a>
      </div>
    </div>

</div>

<div class="row">
  <div class="col-lg-12">

    <div class="box box-success direct-chat direct-chat-warning">
        <div class="box-body" style="padding:10px">
        <h4>Hai {{ session("nama_mhs2") }}, Berikut kami tampilkan data nilai anda</h4>

        <div class="table-responsive">                       
          <table class="table table-striped table-hover" id="tbnilai">
            <thead>
              <tr>
                <th>Mata Kuliah</th>
                <th>Kategori</th>
                <th>N. Utama</th>
                <th>N. Ulang</th>
                <th>N. Bersama</th>
                <th>N. Akhir</th>
              </tr>
            </thead>
            <tbody>
              {!!$datanilai!!}
              
            </tbody>
          </table>
          * Keterangan :<br>
          <div class="hijau alert alert-success" style="width:1em;height:1em;float:left"></div><div style="font-size: 12px;"> &nbsp; Nilai Lulus</div> <br><br>
          <div class="hijau alert alert-danger" style="width:1em;height:1em;float:left"></div><div style="font-size: 12px;"> &nbsp; Nilai Belum Lulus</div> <br><br>
          <div class="hijau alert bg-dark" style="width:1em;height:1em;float:left;background-color:black"></div><div style="float:left;font-size: 12px;"> &nbsp; Nilai Belum Diinputkan</div>
        </div>

        </div>
    </div>

  </div>
</div>
@endsection
@section('css')
<link rel="stylesheet" href="{{asset('lte2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection
@section('script')
<script src="{{asset('lte2/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

<script>
$(document).ready(function(){

  // $('#tbnilai').DataTable();

});
</script>
@endsection
